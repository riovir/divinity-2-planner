# Divinity - Original Sin 2: Definitive edition - Build planner

This is a fan made app. Big thanks to `Larian Studios` for the making of `Divinity - Original Sin 2`. Without it there wouldn't be much to build here, would it?

## Try it out

> It should be live here: https://divinity-2-planner.netlify.com

...unless this repo is really old, and nobody remembers what this game was.

## The codebase in no particular order

![Code cloud](/.code-cloud/divinity-2-planner.png)

> See: Kevlin Henney's [Seven Ineffective Coding Habits of Many Programmers](https://vimeo.com/97329157). At some point he's talking about what a word cloud would tell about a project's codebase.