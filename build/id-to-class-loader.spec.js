const idToClassLoader = require('./id-to-class-loader');
const { bind } = require('ramda');

test('by default returns content as is', () => {
	expect(Loader()('initial')).toBe('initial');
});

test('replaces id attributes with class attributes', () => {
	const loader = Loader({ ids: ['test'] });
	expect(loader('<path id="test" />')).toBe('<path class="test" />');
	expect(loader('<path id="test" /><path id="test" />')).toBe('<path class="test" /><path class="test" />');
	expect(loader('<path notid="test" /><path id="test" />')).toBe('<path notid="test" /><path class="test" />');
});

test('ignores non-specified ids', () => {
	const loader = Loader({ ids: ['test'] });
	expect(loader('<path id="ignore" /><path id="test" />')).toBe('<path id="ignore" /><path class="test" />');
	expect(loader('<path id="ignore" /><path id="testbutbotreally" />')).toBe('<path id="ignore" /><path id="testbutbotreally" />');
});

test('replaces multiple id attributes with class attributes', () => {
	const loader = Loader({ ids: ['test', 'ing'] });
	expect(loader('<path id="test" />')).toBe('<path class="test" />');
	expect(loader('<path id="test" /><path id="ing" />')).toBe('<path class="test" /><path class="ing" />');
	expect(loader('<path id="testing" />')).toBe('<path id="testing" />');
});

test('ignores casing', () => {
	const loader = Loader({ ids: ['tESt'] });
	expect(loader('<path ID="tESt" />')).toBe('<path class="tESt" />');
});

function Loader(query = {}) {
	return bind(idToClassLoader, { query });
}
