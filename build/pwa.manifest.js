const path = require('path');

module.exports = {
	name: 'Divinity: Original Sin 2 - Build planner',
	'short_name': 'DOS2-Planner',
	description: 'Prepare your Divinity: Original Sin 2 party with this Build planner',
	'background_color': '#201813',
	icons: [
		{
			src: path.resolve(__dirname, '../src/assets/favicon.png'),
			sizes: [36, 48, 72, 96, 144, 192],
		},
	],
};
