const path = require('path');
const { merge } = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const OfflinePlugin = require('offline-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const baseConfig = require('./webpack.base.config');
const pwaManifest = require('./pwa.manifest');

const distPath = path.resolve(__dirname, '../dist');

module.exports = merge(baseConfig, {
	entry: {
		app: './src/main.js',
	},
	output: {
		path: distPath,
		publicPath: '/',
		filename: '[name]-[hash].js',
	},
	node: false,
	optimization: {
		runtimeChunk: 'single',
		splitChunks: { chunks: 'all' },
	},
	plugins: [
		new CleanWebpackPlugin(),
		new WebpackPwaManifest(pwaManifest),
		new OfflinePlugin({
			appShell: '/',
			autoUpdate: 1000 * 60 * 2,
			excludes: ['_redirects', '**/*.map'],
			responseStrategy: process.env.NODE_ENV === 'production' ?
				'cache-first' :
				'network-first',
			updateStrategy: 'changed',
			ServiceWorker: { events: true, navigateFallbackURL: '/' },
		}),
		...conditionally(process.env.ANALYZE, new BundleAnalyzerPlugin({
			analyzerMode: 'static',
			generateStatsFile: true,
		})),
	],
	devServer: {
		compress: true,
		hot: true,
		port: 3000,
		historyApiFallback: { index: '/' },
	},
});

function conditionally(condition, value) {
	return condition ? [].concat(value) : [];
}
