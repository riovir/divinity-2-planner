const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const prefixedIdLoader = path.resolve(__dirname, './prefixed-id-loader');
const cssLoaders = [CssExtractPlugin.loader, withSourceMap('css-loader'), withSourceMap('postcss-loader')];
const babelInclude = projectPaths('src', 'test');
const theBackgroundSvg = path.resolve(__dirname, '../src/assets/background.svg');
const inlineSvgLoaders = ['raw-loader', prefixedIdLoader, IdToClassLoader({
	ids: ['is-primary', 'is-secondary', 'is-tertiary', 'is-outline'],
}) ];

module.exports = {
	resolve: {
		alias: {
			'src': path.resolve(__dirname, '../src'),
			'test': path.resolve(__dirname, '../test'),
		},
		extensions: ['*', '.js', '.vue', '.json'],
		mainFiles: ['index', 'index.vue'],
	},
	resolveLoader: {
		modules: [path.resolve(__dirname, 'app'), 'node_modules'],
	},
	module: {
		rules: [
			{ test: /\.vue$/, loader: 'vue-loader' },
			{ test: /\.js$/, loader: 'babel-loader', include: babelInclude },
			{ test: /\.css$/, use: cssLoaders },
			{ test: /\.(scss|sass)$/, use: [...cssLoaders, 'resolve-url-loader', withSourceMap('sass-loader')] },
			{ test: /\.(woff|woff2)$/, loader: 'file-loader', options: { name: 'fonts/[hash].[ext]' } },
			{ test: /\.svg$/, include: theBackgroundSvg, use: [
				'file-loader?name=images/[name]-[hash].[ext]',
				SvgoLoader({ floatPrecision: 0.9 }),
			] },
			{ test: /\.svg$/, include: svgsOf(['logo', 'general']), use: [...inlineSvgLoaders, SvgoLoader()] },
			{ test: /\.svg$/, include: svgsOf(['origin']), use: [...inlineSvgLoaders, SvgoLoader({ floatPrecision: 0.9 })] },
			{	test: /\.svg$/, include: svgsOf(['skill-']), use: [...inlineSvgLoaders, SvgoLoader({ floatPrecision: 0.15 })] },
		],
	},
	plugins: [
		new VueLoaderPlugin(),
		new CopyWebpackPlugin({ patterns: [{ from: './static' }] }),
		new CssExtractPlugin({ filename: '[name]-[hash].css', chunkFilename: '[id]-[hash].css' }),
		new HtmlWebpackPlugin({ template: 'src/index.html', favicon: 'src/assets/favicon.png', chunksSortMode: 'none' }),
	],
	devtool: '#source-map',
};

function projectPaths(...rootPaths) {
	const toAbsolute = rootRelative => path.resolve(__dirname, '../' + rootRelative);
	return rootPaths.map(toAbsolute);
}

function IdToClassLoader({ ids }) {
	return {
		loader: path.resolve(__dirname, './id-to-class-loader.js'),
		options: { ids },
	};
}

function svgsOf(paths = []) {
	const knownPaths = paths.map(resource => path.resolve(__dirname, `../src/assets/${resource}`));
	return file => knownPaths.find(resource => file.startsWith(resource));
}

function SvgoLoader(opts = {}) {
	const options = { plugins: [{ cleanupIDs: false  }], ...opts };
	return { loader: 'svgo-loader', options };
}

function withSourceMap(loader) {
	return { loader, options: { sourceMap: true } };
}
