const { always } = require('ramda');
const { PrefixedIdLoader } = require('./prefixed-id-loader');

const load = content => PrefixedIdLoader({ hash: always('test-hash') })(content);

test('prefixes id attributes with hashes', () => {
	expect(load('<path id="test" />')).toBe('<path id="id-test-hash-test" />');
	expect(load('<path id="test" /><path id="test" />')).toBe('<path id="id-test-hash-test" /><path id="id-test-hash-test" />');
	expect(load('<path notid="test" /><path id="test" />')).toBe('<path notid="test" /><path id="id-test-hash-test" />');
});

test('ignores casing', () => {
	expect(load('<path ID="tESt" />')).toBe('<path id="id-test-hash-tESt" />');
});
