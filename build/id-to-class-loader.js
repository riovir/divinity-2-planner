const loaderUtils = require('loader-utils');

module.exports = function idToClass(content) {
	const { ids } = loaderUtils.getOptions(this);
	if (!ids) { return content; }
	const toIdRegex = id => new RegExp(`\\sid="(${id})"`, 'gi');
	return ids.map(toIdRegex).reduce((con, regex) => con.replace(regex, ' class="$1"'), content);
};
