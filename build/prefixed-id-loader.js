const hashString = require('hash-string');

module.exports = PrefixedIdLoader({ hash: hashString });
module.exports.PrefixedIdLoader = PrefixedIdLoader;

function PrefixedIdLoader({ hash }) {
	return content => {
		const contentHash = hash(content);
		return content
				.replace(/\sid="([^"]+)"/gi, ` id="id-${contentHash}-$1"`)
				.replace(/\sxlink:href="#([^"]+)"/gi, ` xlink:href="#id-${contentHash}-$1"`);
	};
}
