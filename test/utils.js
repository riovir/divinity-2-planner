import Build from 'src/store/build';
import { O_FANE, A_FINESSE, A_MEMORY, A_WITS, CV_SNEAKING, CV_LUCKY_CHARM } from 'src/store/constants';
import {
	CS_AEROTHEURGE, CS_HUNTSMAN, CS_SCOUNDREL, CS_POLYMORPH,
	T_LONE_WOLF, T_THE_PAWN, T_SPIDERS_KISS_SD,
	SK_ELECTRIC_DISCHARGE,
} from 'src/store/constants';

export function FaneRogueBuild(overrides) {
	return Build({
		id: 3,
		level: 7,
		origin: O_FANE,
		attributeProgression: [
			A_FINESSE, A_WITS,
			A_MEMORY, A_MEMORY,
			A_FINESSE, A_FINESSE, A_FINESSE,
			A_FINESSE, A_WITS,
			A_MEMORY, A_WITS,
			A_FINESSE, A_WITS,
			A_FINESSE, A_WITS,
		],
		combatProgression: [CS_SCOUNDREL, CS_HUNTSMAN, CS_POLYMORPH, CS_SCOUNDREL, CS_SCOUNDREL, CS_HUNTSMAN, CS_SCOUNDREL, CS_AEROTHEURGE],
		civilProgression: [CV_SNEAKING, CV_SNEAKING, CV_LUCKY_CHARM],
		talents: [T_LONE_WOLF, T_THE_PAWN],
		freeTalents: [T_SPIDERS_KISS_SD],
		skills: [SK_ELECTRIC_DISCHARGE],
		...overrides,
	});
}
