import { curry } from 'ramda';

export const camelConcat = curry((left, right) => {
	if (!right || !right.length) { return left; }
	const [letter, ...rest] = right;
	return [left, letter.toUpperCase(), ...rest].join('');
});
