import './style/theme.scss';
import * as OfflinePluginRuntime from 'offline-plugin/runtime';

import Vue from 'vue';
import vueSetup, { FLAG_SW_UPDATE } from './vue-setup';

OfflinePluginRuntime.install({
	onUpdated: () => { window[FLAG_SW_UPDATE] = true; },
	onUpdateReady: () => { OfflinePluginRuntime.applyUpdate(); },
});

const app = { el: '#app', render: h => h('router-view') };
new Vue(Object.assign(app, vueSetup));
