import { always, cond, contains, curry, flip, prop, sum } from 'ramda';

export const TYPE_ATTRIBUTE = 'attribute';
export const TYPE_COMBAT_ABILITY = 'combatAbility';
export const TYPE_CIVIL_ABILITY = 'civilAbility';
export const TYPE_RESIST = 'resist';
export const TYPE_DERIVE = 'derivedStat';

export const LEVEL_CAP = 21;
export const ATTRIBUTE_CAP = 40;
export const COMBAT_ABILITY_CAP = 10;
export const CIVIL_ABILITY_CAP = 5;
export const RESIST_CAP = 100;

const TYPE_CAPS = {
	[TYPE_ATTRIBUTE]: ATTRIBUTE_CAP,
	[TYPE_COMBAT_ABILITY]: COMBAT_ABILITY_CAP,
	[TYPE_CIVIL_ABILITY]: CIVIL_ABILITY_CAP,
	[TYPE_RESIST]: RESIST_CAP,
	[TYPE_DERIVE]: Infinity,
};

// Character
export const C_COMPANIONS_IN_PARTY = 'companionsInParty';
export const C_COMPANIONS = 'companions';
export const C_SKILLS_OVER_LIMIT = 'skillsOverLimit';

export const O_BEAST = 'beast';
export const O_FANE = 'fane';
export const O_IFAN = 'ifan';
export const O_LOHSE = 'lohse';
export const O_RED_PRINCE = 'redPrince';
export const O_SEBILLE = 'sebille';
export const O_CUSTOM_ELF_FEMALE = 'customElfFemale';

export const R_DWARF = 'dwarf';
export const R_ELF = 'elf';
export const R_HUMAN = 'human';
export const R_LIZARD = 'lizard';

export const G_FEMALE = 'female';
export const G_MALE = 'male';

export const A_STRENGTH = 'strength';
export const A_FINESSE = 'finesse';
export const A_INTELLIGENCE = 'intelligence';
export const A_CONSTITUTION = 'constitution';
export const A_MEMORY = 'memory';
export const A_WITS = 'wits';
export const ATTRIBUTES = [A_STRENGTH, A_FINESSE, A_INTELLIGENCE, A_CONSTITUTION, A_MEMORY, A_WITS];

export const AT_PHYSICAL = 'armorPhysical';
export const AT_MAGICAL = 'armorMagical';

export const CS_AEROTHEURGE = 'aerotheurge';
export const CS_DUAL_WIELDING = 'dualWielding';
export const CS_GEOMANCER = 'geomancer';
export const CS_HUNTSMAN = 'huntsman';
export const CS_HYDROSOPHIST = 'hydrosophist';
export const CS_LEADERSHIP = 'leadership';
export const CS_NECROMANCER = 'necromancer';
export const CS_SCOUNDREL = 'scoundrel';
export const CS_POLYMORPH = 'polymorph';
export const CS_PYROKINETIC = 'pyrokinetic';
export const CS_RANGED = 'ranged';
export const CS_SINGLE_HANDED = 'singleHanded';
export const CS_SUMMONING = 'summoning';
export const CS_TWO_HANDED = 'twoHanded';
export const CS_WARFARE = 'warfare';

// Skill subtypes
export const ST_EQUIPMENT = 'equipment';
export const ST_INNATE = 'innate';
export const ST_GEOMANCER_POISON = 'geomancer-poison';
export const ST_SCOUNDREL_BLOOD = 'scoundrel-blood';
export const ST_SCOUNDREL_LIGHT = 'scoundrel-light';
export const ST_SOURCE = 'source';
export const ST_STORY = 'story';

// Effects types
export const EF_BURNING = 'burning';
export const EF_POISONED = 'poisoned';
export const EF_BLEEDING = 'bleeding';
export const EF_NECROFIRE = 'necrofire';
export const EF_ACID = 'acid';
export const EF_SUFFOCATING = 'suffocating';
export const EF_ENTANGLED = 'entangled';
export const EF_DEATH_WISH = 'deathWish';
export const EF_RUPTURED_TENDONS = 'rupturedTendons';

export const SKILLS = [
	CS_AEROTHEURGE, CS_GEOMANCER, CS_HUNTSMAN, CS_HYDROSOPHIST, CS_NECROMANCER,
	CS_POLYMORPH, CS_PYROKINETIC, CS_SCOUNDREL, CS_SUMMONING, CS_WARFARE,
];
export const SKILL_SCHOOLS = [...SKILLS, ST_STORY, ST_SOURCE];
export const WEAPONS = ['dualWielding', 'ranged', 'singleHanded', 'twoHanded'];
export const DEFENCE = ['leadership', 'perserverance', 'retribution'];

export const COMBAT_ABILITIES = SKILLS.concat(WEAPONS).concat(DEFENCE);

export const CV_BARTERING = 'bartering';
export const CV_LOREMASTER = 'loremaster';
export const CV_LUCKY_CHARM = 'luckyCharm';
export const CV_PERSUASION = 'persuasion';
export const CV_SNEAKING = 'sneaking';
export const CIVIL_ABILITIES = [
	'telekinesis', CV_LOREMASTER,
	CV_SNEAKING, 'thievery',
	CV_BARTERING, CV_PERSUASION, CV_LUCKY_CHARM,
];

// Racial talents
export const T_ANCESTRAL_KNOWLEDGE = 'ancestralKnowledge';
export const T_CORPSE_EATER = 'corpseEater';
export const T_DWARVEN_GUILE = 'dwarvenGuile';
export const T_STURDY = 'sturdy';
export const T_THRIFTY = 'thrifty';
export const T_INGENIOUS = 'ingenious';
export const T_SPELLSONG = 'spellsong';
export const T_SOPHISTICATED = 'sophisticated';

// Selectable talents
export const T_ALL_SKILLED_UP = 'allSkilledUp';
export const T_BIGGER_AND_BETTER = 'biggerAndBetter';
export const T_DEMON = 'demon';
export const T_EXECUTIONER = 'executioner';
export const T_GLASS_CANNON = 'glassCannon';
export const T_FAR_OUT_MAN = 'farOutMan';
export const T_HOTHEAD = 'hothead';
export const T_ICE_KING = 'iceKing';
export const T_LONE_WOLF = 'lonewolf';
export const T_MNEMONIC = 'mnemonic';
export const T_PARRY_MASTER = 'parryMaster';
export const T_PETPAL = 'petPal';
export const T_PICTURE_OF_HEALTH = 'pictureOfHealth';
export const T_SPIDERS_KISS_TO = 'spidersKissTO';
export const T_SPIDERS_KISS_CQ = 'spidersKissCQ';
export const T_SPIDERS_KISS_GC = 'spidersKissGC';
export const T_SPIDERS_KISS_SD = 'spidersKissSD';
export const T_SPIDERS_KISS_TC = 'spidersKissTC';
export const T_THE_PAWN = 'thePawn';
export const T_TORTURER = 'torturer';
export const T_TRADERS_SECRETS = 'tradeSecrets';
export const T_TRAINING_AUTHORITY = 'trainingAuthority';
export const T_TRAINING_INTELLIGENCE = 'trainingIntelligence';
export const T_TRAINING_WITS = 'trainingWits';
export const T_TRAINING_FINESSE = 'trainingFinesse';
export const T_TRAINING_CONSTITUTION = 'trainingConstitution';
export const T_TRAINING_STRENGTH = 'trainingStrength';

// Story talents
export const T_ROOTED = 'rooted';
export const T_UNDEAD = 'undead';

export const RS_FIRE = 'resistFire';
export const RS_WATER = 'resistWater';
export const RS_AIR = 'resistAir';
export const RS_EARTH = 'resistEarth';
export const RS_POISON = 'resistPoison';
export const RESISTS = [RS_FIRE, RS_WATER, RS_AIR, RS_EARTH, RS_POISON];

// Derived stats
export const DS_ACCURACY = 'accuracy';
export const DS_AP_MAX = 'apMax';
export const DS_AP_REGEN = 'apRegen';
export const DS_AP_START = 'apStart';
export const DS_CARRY_WEIGHT = 'carryWeight';
export const DS_CRIT_CHANCE = 'critChance';
export const DS_CRIT_MULTIPLIER = 'critMultiplier';

export const DS_DAMAGE_STRENGTH = 'damageStrength';
export const DS_DAMAGE_FINESSE = 'damageFinesse';
export const DS_DAMAGE_INTELLIGENCE = 'damageIntelligence';
export const DS_DAMAGE_AIR = 'damageAir';
export const DS_DAMAGE_EARTH = 'damageEarth';
export const DS_DAMAGE_FIRE = 'damageFire';
export const DS_DAMAGE_PHYSICAL = 'damagePhysical';
export const DS_DAMAGE_WATER = 'damageWater';

export const DS_DODGING = 'dodging';
export const DS_EFFECT_DURATION_BONUSES = 'effectDurationBonuses';
export const DS_INITIATIVE = 'initiative';
export const DS_MEMORY_SLOT = 'memorySlot';
export const DS_MOVEMENT = 'movement';
export const DS_OBJECT_WEIGHT = 'objectWeight';
export const DS_RANGE_BONUS = 'rangeBonus';
export const DS_VITALITY = 'vitality';
export const DS_VITALITY_MULTIPLIER = 'vitalityMultiplier';

export const DERIVED_STATS = [
	DS_ACCURACY,
	DS_AP_MAX, DS_AP_REGEN, DS_AP_START,
	DS_CRIT_CHANCE, DS_CRIT_MULTIPLIER,
	DS_DODGING, DS_INITIATIVE, DS_MOVEMENT,
	DS_VITALITY, DS_VITALITY_MULTIPLIER,
	DS_DAMAGE_STRENGTH, DS_DAMAGE_FINESSE, DS_DAMAGE_INTELLIGENCE,
	DS_DAMAGE_AIR, DS_DAMAGE_EARTH, DS_DAMAGE_FIRE, DS_DAMAGE_PHYSICAL, DS_DAMAGE_WATER,
	DS_MEMORY_SLOT, DS_CARRY_WEIGHT, DS_OBJECT_WEIGHT,
	DS_RANGE_BONUS,
	...RESISTS,
];

// Equipment in hands
export const EH_BOW = 'bow';
export const EH_CROSSBOW = 'crossbow';
export const EH_AXE = 'axe';
export const EH_MACE = 'mace';
export const EH_DAGGER = 'dagger';
export const EH_SWORD = 'sword';
export const EH_WAND = 'wand';
export const EH_SHIELD = 'shield';
export const EH_2H_AXE = '2hAxe';
export const EH_2H_MACE = '2hMace';
export const EH_2H_SWORD = '2hSword';
export const EH_SPEAR = 'spear';
export const EH_STAFF = 'staff';

// Equipment types
export const ET_MELEE = 'melee';
export const ET_MISSILE = 'missile';
export const ET_RANGED = 'ranged';
export const ET_SHIELD = 'shield';

// Equipment slots
export const ES_MAIN_HAND = 'mainHand';
export const ES_OFF_HAND = 'offHand';

// Skills
export const SK_BLINDING_RADIANCE = 'blindingRadiance';
export const SK_ELECTRIC_DISCHARGE = 'electricDischarge';
export const SK_FAVOURABLE_WIND = 'favourableWind';
export const SK_SHOCKING_TOUCH = 'shockingTouch';

export function isAttribute(stat) { return statType(stat) === TYPE_ATTRIBUTE; }
export function isCombatAbility(stat) { return statType(stat) ==TYPE_COMBAT_ABILITY; }
export function isCivilAbility(stat) { return statType(stat) == TYPE_CIVIL_ABILITY; }

export const statCapFor = curry(({ bonuses }, stat) => {
	if (stat === 'level') { return LEVEL_CAP; }
	const type = statType(stat);
	if (!type) { return; }
	const statBonuses = bonuses[stat] || [];
	const capBonuses = sum(statBonuses.map(prop('capRaise')));
	return TYPE_CAPS[type] + capBonuses;
});

export function statType(stat) {
	const containedIn = flip(contains);
	return cond([
		[containedIn(ATTRIBUTES), always(TYPE_ATTRIBUTE)],
		[containedIn(COMBAT_ABILITIES), always(TYPE_COMBAT_ABILITY)],
		[containedIn(CIVIL_ABILITIES), always(TYPE_CIVIL_ABILITY)],
		[containedIn(RESISTS), always(TYPE_RESIST)],
		[containedIn(DERIVED_STATS), always(TYPE_DERIVE)],
	])(stat);
}
