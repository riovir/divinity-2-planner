import { toSkillProp } from './index';
import { EH_2H_SWORD, EH_STAFF } from '../constants';

test('allIn does not apply to a staff', () => {
	expect(toSkillProp('availableFor', 'allIn')({ mainHand: EH_2H_SWORD })).toBe(true);
	expect(toSkillProp('availableFor', 'allIn')({ mainHand: EH_STAFF })).toBe(false);
});
