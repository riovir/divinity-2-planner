import { AT_PHYSICAL, CS_AEROTHEURGE, CS_GEOMANCER, CS_HYDROSOPHIST, CS_POLYMORPH, CS_PYROKINETIC } from '../constants';
import { Basic, Advanced, Master, Crafted } from './skills';

export const SKILLS = [
	Basic({ ability: CS_POLYMORPH, name: 'bullHorns', cooldown: 6, apCost: 0 }),
	Basic({ ability: CS_POLYMORPH, name: 'chameleonCloak', cooldown: 6, apCost: 1 }),
	Basic({ ability: CS_POLYMORPH, name: 'chickenClaw', cooldown: 6, range: 2, resistedBy: AT_PHYSICAL }),
	Basic({ ability: CS_POLYMORPH, name: 'tentacleLash', range: 8, resistedBy: AT_PHYSICAL }),
	Advanced({ ability: CS_POLYMORPH, name: 'heartOfSteel', cooldown: 5 }),
	Advanced({ ability: CS_POLYMORPH, name: 'medusaHead', cooldown: 5 }),
	Advanced({ ability: CS_POLYMORPH, name: 'spiderLegs', cooldown: 5, apCost: 0 }),
	Advanced({ ability: CS_POLYMORPH, name: 'spreadYourWings', cooldown: 6, apCost: 1 }),
	Advanced({ ability: CS_POLYMORPH, name: 'summonOilyBlob', cooldown: 6, range: 13, radius: 3 }),
	Advanced({ ability: CS_POLYMORPH, name: 'terrainTransmutation', cooldown: 2, range: 13, apCost: 1 }),
	Master({ ability: CS_POLYMORPH, name: 'apotheosis', cooldown: 0, spCost: 3 }),
	Master({ ability: CS_POLYMORPH, name: 'equalise', cooldown: 5 }),
	Master({ ability: CS_POLYMORPH, name: 'flaySkin', cooldown: 5, range: 13, resistedBy: AT_PHYSICAL }),
	Master({ ability: CS_POLYMORPH, name: 'forcedExchange', cooldown: 1, range: 2, resistedBy: AT_PHYSICAL, apCost: 1, spCost: 2 }),
	Master({ ability: CS_POLYMORPH, name: 'skinGraft', cooldown: 0, apCost: 1, spCost: 1 }),
	Crafted({ ability: CS_POLYMORPH, otherAbility: CS_PYROKINETIC, name: 'flamingSkin', cooldown: 5, apCost: 1, spCost: 1, subtype: CS_PYROKINETIC }),
	Crafted({ ability: CS_POLYMORPH, otherAbility: CS_HYDROSOPHIST, name: 'icySkin', cooldown: 5, apCost: 1, spCost: 1, subtype: CS_HYDROSOPHIST }),
	Crafted({ ability: CS_POLYMORPH, otherAbility: CS_AEROTHEURGE, name: 'jellyfishSkin', cooldown: 5, apCost: 1, spCost: 1, subtype: CS_AEROTHEURGE }),
	Crafted({ ability: CS_POLYMORPH, otherAbility: CS_GEOMANCER, name: 'poisonousSkin', cooldown: 5, apCost: 1, spCost: 1, subtype: CS_GEOMANCER }),
];
