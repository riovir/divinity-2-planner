import { AT_PHYSICAL, CS_HUNTSMAN, ET_RANGED } from '../constants';
import { Basic, Advanced, Master, requiringA } from './skills';

export const SKILLS = [
	Basic({ ability: CS_HUNTSMAN, name: 'elementalArrowheads', cooldown: 4, range: 3, apCost: 1 }),
	Basic({ ability: CS_HUNTSMAN, name: 'firstAid', cooldown: 4, range: 8, apCost: 1 }),
	Advanced({ ability: CS_HUNTSMAN, name: 'tacticalRetreat', cooldown: 4, range: 13, apCost: 1 }),
	Master({ ability: CS_HUNTSMAN, name: 'farsight', range: 13, apCost: 1, spCost: 1 }),
	Master({ ability: CS_HUNTSMAN, name: 'glitterDust', range: 13, apCost: 1 }),
	requiringA(ET_RANGED, Basic({ ability: CS_HUNTSMAN, name: 'pinDown', range: 13, resistedBy: AT_PHYSICAL, apCost: 3 })),
	requiringA(ET_RANGED, Basic({ ability: CS_HUNTSMAN, name: 'ricochet', cooldown: 4, range: 13 })),
	requiringA(ET_RANGED, Advanced({ ability: CS_HUNTSMAN, name: 'ballisticShot', cooldown: 4, range: 13 })),
	requiringA(ET_RANGED, Advanced({ ability: CS_HUNTSMAN, name: 'barrage', range: 13, apCost: 3 })),
	requiringA(ET_RANGED, Advanced({ ability: CS_HUNTSMAN, name: 'marksmansFang', cooldown: 2, range: 13 })),
	requiringA(ET_RANGED, Advanced({ ability: CS_HUNTSMAN, name: 'reactiveShot', cooldown: 2, range: 13 })),
	requiringA(ET_RANGED, Advanced({ ability: CS_HUNTSMAN, name: 'skyShot', range: 20 })),
	requiringA(ET_RANGED, Master({ ability: CS_HUNTSMAN, name: 'arrowSpray', cooldown: 5, range: 13, apCost: 3, spCost: 1 })),
	requiringA(ET_RANGED, Master({ ability: CS_HUNTSMAN, name: 'arrowStorm', cooldown: 4, range: 13, apCost: 3, spCost: 3, radius: 2 })),
	requiringA(ET_RANGED, Master({ ability: CS_HUNTSMAN, name: 'assassinate', cooldown: 5, range: 13, apCost: 3 })),
];
