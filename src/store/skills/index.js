import { both, propEq } from 'ramda';
import { SKILLS as AEROTHEURGE_SKILLS } from './aerotheurge';
import { SKILLS as GEOMANCER_SKILLS } from './geomancer';
import { SKILLS as HUNTSMAN_SKILLS } from './huntsman';
import { SKILLS as HYDROSOPHIST_SKILLS } from './hydrosophist';
import { SKILLS as NECROMANCER_SKILLS } from './necromancer';
import { SKILLS as POLYMORPH_SKILLS } from './polymorph';
import { SKILLS as PYROKINETIC_SKILLS } from './pyrokinetic';
import { SKILLS as SCOUNDREL_SKILLS } from './scoundrel';
import { SKILLS as SUMMONING_SKILLS } from './summoning';
import { SKILLS as WARFARE_SKILLS } from './warfare';
import { SKILLS as SPECIAL_SKILLS } from './special';
import { sortSkills, toDetails, toProp } from './skills';

const ALL_SKILLS = [
	...AEROTHEURGE_SKILLS,
	...GEOMANCER_SKILLS,
	...HUNTSMAN_SKILLS,
	...HYDROSOPHIST_SKILLS,
	...NECROMANCER_SKILLS,
	...POLYMORPH_SKILLS,
	...PYROKINETIC_SKILLS,
	...SCOUNDREL_SKILLS,
	...SUMMONING_SKILLS,
	...WARFARE_SKILLS,
	...SPECIAL_SKILLS,
];
const toSkillDetails = toDetails(ALL_SKILLS);
const toSkillProp = toProp(ALL_SKILLS);

export function freeSkillsOf(character) {
	const canHave = ({ availableFor }) => availableFor(character);
	return ALL_SKILLS.filter(both(propEq('memoryCost', 0), canHave));
}

export { ALL_SKILLS, sortSkills, toSkillDetails, toSkillProp };
