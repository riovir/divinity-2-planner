import { pipe, prop, propEq } from 'ramda';
import {
	AT_MAGICAL,	AT_PHYSICAL, EF_BURNING, ET_MELEE,
	ST_EQUIPMENT, ST_INNATE, ST_SOURCE, ST_STORY, ET_SHIELD, EH_STAFF,
	O_BEAST, O_FANE, O_IFAN, O_LOHSE, O_RED_PRINCE, O_SEBILLE, R_DWARF, R_ELF, R_HUMAN, R_LIZARD,
	CS_AEROTHEURGE, CS_GEOMANCER, CS_NECROMANCER, CS_PYROKINETIC, CS_SUMMONING,
} from '../constants';
import { toEquipmentProp, toEquipmentDetails } from '../equipment';
import { Skill, Effect, requiringA } from './skills';
import { toOriginDetails } from '../origins';

const effect = Effect({ name: EF_BURNING, duration: 2 });

export const SKILLS = [
	// Innate skills
	Free({
		category: ST_INNATE, name: 'blindingSquall', availableFor: originIs(O_BEAST),
		cooldown: 5, range: 13, resistedBy: AT_MAGICAL, spCost: 1, subtype: CS_AEROTHEURGE }),
	Free({
		category: ST_INNATE, name: 'timeWarp', availableFor: originIs(O_FANE),
		cooldown: 0, range: 6, spCost: 1, subtype: ST_STORY }),
	Free({
		category: ST_INNATE, name: 'summonIfansSoulWolf', availableFor: originIs(O_IFAN),
		cooldown: 6, range: 13, apCost: 3, spCost: 1, duration: 10, subtype: CS_SUMMONING }),
	Free({
		category: ST_INNATE, name: 'maddeningSong', availableFor: originIs(O_LOHSE),
		cooldown: 5, range: 15, resistedBy: AT_MAGICAL, spCost: 1, subtype: ST_STORY }),
	Free({
		category: ST_INNATE, name: 'demonicStare', availableFor: originIs(O_RED_PRINCE),
		range: 13, resistedBy: AT_MAGICAL, spCost: 1, subtype: ST_STORY }),
	Free({
		category: ST_INNATE, name: 'breakTheShackles', availableFor: originIs(O_SEBILLE),
		cooldown: 2, spCost: 1, subtype: ST_STORY }),
	Free({
		category: ST_INNATE, name: 'dragonsBlaze', availableFor: isLiveRace(R_LIZARD),
		range: 7, resistedBy: AT_MAGICAL, apCost: 1, effect, subtype: CS_PYROKINETIC }),
	Free({
		category: ST_INNATE, name: 'encourage', availableFor: isLiveRace(R_HUMAN),
		cooldown: 6, apCost: 1, subtype: CS_SUMMONING }),
	Free({
		category: ST_INNATE, name: 'fleshSacrifice', availableFor: isLiveRace(R_ELF),
		cooldown: 5, subtype: CS_NECROMANCER }),
	Free({
		category: ST_INNATE, name: 'petrifyingTouch', availableFor: isLiveRace(R_DWARF),
		cooldown: 4, range: 2, resistedBy: AT_MAGICAL, apCost: 1, subtype: CS_GEOMANCER }),
	Free({
		category: ST_INNATE, name: 'domeOfProtection', availableFor: originPropEq('unique', false),
		cooldown: 6, range: 13, duration: 5, apCost: 1, spCost: 1, subtype: ST_STORY }),
	Free({
		category: ST_INNATE, name: 'playDead', availableFor: originPropEq('undead', true),
		cooldown: 6, apCost: 0, subtype: ST_STORY }),

	// Equipment skills
	requiringTwoHandedMundane(Free({
		category: ST_EQUIPMENT, name: 'allIn',
		cooldown: 1, range: 2.5, apCost: 3, subtype: ST_EQUIPMENT })),
	requiringDualMelee(Free({
		category: ST_EQUIPMENT, name: 'flurry',
		cooldown: 1, range: 1.8, apCost: 3, subtype: ST_EQUIPMENT })),
	requiringA(ET_SHIELD, Free({
		category: ST_EQUIPMENT, name: 'shieldsUp',
		subtype: ST_EQUIPMENT })),
	requiringA(EH_STAFF, Free({
		category: ST_EQUIPMENT, name: 'staffOfMagus',
		cooldown: 1, range: 13, subtype: ST_EQUIPMENT })),
	requiringFreeHand(Free({
		category: ST_EQUIPMENT, name: 'suckerPunch',
		range: 2, resistedBy: AT_PHYSICAL, apCost: 1, subtype: ST_EQUIPMENT })),

	// Memorizable skills
	Skill({
		category: ST_SOURCE, name: 'bless',
		range: 13, apCost: 1, spCost: 1, subtype: ST_SOURCE }),
	Skill({
		category: ST_STORY, name: 'deafeningShriek',
		cooldown: 4, resistedBy: AT_MAGICAL, subtype: ST_STORY }),
];

function Free({ ability, ...options }) {
	return Skill({ ability, memoryCost: 0, ...options	});
}

function originIs(expected) {
	return ({ origin }) => origin === expected;
}

function isLiveRace(raceName) {
	const originMatches = ({ race, undead }) => !undead && race === raceName;
	return pipe(prop('origin'), toOriginDetails, originMatches);
}

function originPropEq(name, value) {
	return pipe(prop('origin'), toOriginDetails, propEq(name, value));
}

function requiringDualMelee(character) {
	const equipment = ET_MELEE;
	const hasIn = hand => toEquipmentProp('type', hand) === equipment;
	const isDualWielding = ({ mainHand, offHand }) => hasIn(mainHand) && hasIn(offHand);
	return { ...character, requires: { equipment }, availableFor: isDualWielding };
}

function requiringTwoHandedMundane(character) {
	const equipment = ET_MELEE;
	return { ...character, requires: { equipment }, availableFor: ({ mainHand }) => {
		const { name, twoHanded, type } = toEquipmentDetails(mainHand);
		return twoHanded && type === ET_MELEE && name !== EH_STAFF;
	} };
}

function requiringFreeHand(character) {
	return { ...character, availableFor: ({ mainHand, offHand }) => !toEquipmentProp('twoHanded', mainHand) && !offHand };
}
