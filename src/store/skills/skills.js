import { ascend, both, curry, join, keys, pipe, prop, propEq, sortWith, T } from 'ramda';
import { toEquipmentProp } from '../equipment';
import { hasAbility } from '../utils';

export const sortSkills = sortWith([
	ascend(pipe(prop('ability'), String)),
	ascend(pipe(prop('category'), String)),
	ascend(prop('memoryCost')),
	ascend(pipe(prop('requires'), keys, join(''))),
	ascend(mainRequirement),
	ascend(prop('spCost')),
	ascend(prop('name')),
]);

function mainRequirement({ ability, requires }) {
	return ability ? requires[ability] : 0;
}

export const toProp = curry((allSkills, propName, skillName) => {
	return pipe(toDetails(allSkills), prop(propName))(skillName);
});

export const toDetails = curry((allSkills, skillName) => {
	const details = allSkills.find(propEq('name', skillName));
	if (!details) { throw new Error(`Unknown skill: ${skillName}`); }
	return details;
});

export function Basic({ ability, ...options }) {
	return Skill({ ability, requires: { [ability]: 1 }, availableFor: hasAbility(ability, 1), ...options });
}

export function Advanced({ ability, ...options }) {
	return Skill({ ability, requires: { [ability]: 2 }, availableFor: hasAbility(ability, 2), ...options });
}

export function Crafted({ ability, otherAbility, spCost, ...options }) {
	const minSkill = spCost > 0 ? 2 : 1;
	const requires = { [ability]: minSkill, [otherAbility]: minSkill };
	const availableFor = both(hasAbility(ability, minSkill), hasAbility(otherAbility, minSkill));
	const memoryCost = spCost > 0 ? 2 : 1;
	return Skill({ ability, requires, availableFor, spCost, memoryCost, ...options });
}

export function MasterCrafted({ ability, otherAbility, minSkill = 3, spCost = 3, ...options }) {
	const requires = { [ability]: minSkill, [otherAbility]: minSkill };
	const availableFor = both(hasAbility(ability, minSkill), hasAbility(otherAbility, minSkill));
	return Skill({ ability, requires, availableFor, spCost, memoryCost: 3, ...options });
}

export function Master({ ability, spCost, ...options }) {
	const minSkill = spCost >= 3 ? 5 : 3;
	const memoryCost = spCost > 0 ? spCost > 2 ? 3 : 2 : 1;
	return Skill({ ability, requires: { [ability]: minSkill }, availableFor: hasAbility(ability, minSkill), spCost, memoryCost, ...options });
}

export function Effect({ name = 'nothing', duration = 0 } = {}) {
	return { name, duration };
}

export function Skill({
	name = '',
	ability = '',
	category = ability || '',
	subtype = null,
	craftedWith = [],
	resourceKey = `skill.${category}.${name}`,
	requires = {},
	availableFor = T,
	effect = Effect(),
	resistedBy = null,
	apCost = 2,
	spCost = 0,
	memoryCost = 1,
	cooldown = 3,
	duration = null,
	radius = null,
	range = null,
	rangeLocked = !range || range < 4,
}) {
	return {
		name, ability, category, subtype, craftedWith, resourceKey, requires, availableFor,
		effect: Effect(effect), resistedBy, apCost, spCost, memoryCost, cooldown, duration, radius, range, rangeLocked,
	};
}

export function requiringA(equipment, { requires, availableFor, ...rest }) {
	const alsoType = { ...requires, equipment };
	const hasIn = hand => toEquipmentProp('name', hand) === equipment || toEquipmentProp('type', hand) === equipment;
	const hasType = ({ mainHand, offHand }) => hasIn(mainHand) || hasIn(offHand);
	return { requires: alsoType, availableFor: both(availableFor, hasType), ...rest };
}
