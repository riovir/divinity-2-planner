import { AT_MAGICAL, AT_PHYSICAL, CS_HYDROSOPHIST, CS_NECROMANCER, EF_BLEEDING, EF_DEATH_WISH } from '../constants';
import { Basic, Advanced, Master, Crafted, MasterCrafted, Effect } from './skills';

const effect = Effect({ name: EF_BLEEDING, duration: 2 });
const effectDeathWish = Effect({ name: EF_DEATH_WISH, duration: 2 });

export const SKILLS = [
	Basic({ ability: CS_NECROMANCER, name: 'bloodSucker', range: 13, apCost: 1 }),
	Basic({ ability: CS_NECROMANCER, name: 'decayingTouch', range: 2, resistedBy: AT_PHYSICAL }),
	Basic({ ability: CS_NECROMANCER, name: 'raiseBloatedCorpse', range: 13, apCost: 1 }),
	Basic({ ability: CS_NECROMANCER, name: 'mosquitoSwarm', range: 13, resistedBy: AT_PHYSICAL, effect }),
	Advanced({ ability: CS_NECROMANCER, name: 'boneCage', cooldown: 5, apCost: 1 }),
	Advanced({ ability: CS_NECROMANCER, name: 'deathWish', cooldown: 5, range: 13, effect: effectDeathWish }),
	Advanced({ ability: CS_NECROMANCER, name: 'infect', range: 13, resistedBy: AT_PHYSICAL, apCost: 3 }),
	Advanced({ ability: CS_NECROMANCER, name: 'livingOnTheEdge', cooldown: 5, range: 13, apCost: 3 }),
	Advanced({ ability: CS_NECROMANCER, name: 'raiseBoneWidow', cooldown: 5, range: 13, duration: 5 }),
	Advanced({ ability: CS_NECROMANCER, name: 'shacklesOfPain', cooldown: 5, resistedBy: AT_PHYSICAL, apCost: 1 }),
	Master({ ability: CS_NECROMANCER, name: 'blackShroud', cooldown: 5, range: 13, apCost: 1, spCost: 1 }),
	Master({ ability: CS_NECROMANCER, name: 'graspOfTheStarved', cooldown: 5, range: 13, resistedBy: AT_PHYSICAL, spCost: 2 }),
	Master({ ability: CS_NECROMANCER, name: 'lastRites', cooldown: 0, range: 13, apCost: 3 }),
	Master({ ability: CS_NECROMANCER, name: 'silencingStare', range: 13, resistedBy: AT_MAGICAL }),
	Master({ ability: CS_NECROMANCER, name: 'totemsOfTheNecromancer', cooldown: 5, spCost: 3 }),
	Crafted({ ability: CS_NECROMANCER, otherAbility: CS_HYDROSOPHIST, name: 'rainingBlood',
		cooldown: 5, range: 8, resistedBy: AT_PHYSICAL, duration: 1, effect }),
	MasterCrafted({ ability: CS_NECROMANCER, otherAbility: CS_HYDROSOPHIST, name: 'bloodStorm', cooldown: 5, apCost: 4, spCost: 3, duration: 2 }),
];
