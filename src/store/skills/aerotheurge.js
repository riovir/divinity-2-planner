import {
	AT_MAGICAL,
	CS_AEROTHEURGE, CS_HUNTSMAN, CS_NECROMANCER, CS_POLYMORPH, CS_SCOUNDREL, CS_WARFARE,
	SK_BLINDING_RADIANCE, SK_ELECTRIC_DISCHARGE, SK_FAVOURABLE_WIND, SK_SHOCKING_TOUCH, EF_SUFFOCATING,
} from '../constants';
import { Basic, Advanced, Master, Crafted, Effect } from './skills';

const effect = Effect({ name: EF_SUFFOCATING, duration: 2 });

export const SKILLS = [
	Basic({ ability: CS_AEROTHEURGE, name: SK_BLINDING_RADIANCE, cooldown: 4, resistedBy: AT_MAGICAL }),
	Basic({ ability: CS_AEROTHEURGE, name: SK_ELECTRIC_DISCHARGE, range: 13, resistedBy: AT_MAGICAL }),
	Basic({ ability: CS_AEROTHEURGE, name: SK_FAVOURABLE_WIND, cooldown: 6, apCost: 1 }),
	Basic({ ability: CS_AEROTHEURGE, name: SK_SHOCKING_TOUCH, range: 2, resistedBy: AT_MAGICAL }),
	Advanced({ ability: CS_AEROTHEURGE, name: 'apportation', range: 8, apCost: 1 }),
	Advanced({ ability: CS_AEROTHEURGE, name: 'dazingBolt', cooldown: 4, range: 13, resistedBy: AT_MAGICAL, apCost: 3, radius: 3 }),
	Advanced({ ability: CS_AEROTHEURGE, name: 'netherSwap', range: 13, apCost: 1 }),
	Advanced({ ability: CS_AEROTHEURGE, name: 'pressureSpike', range: 13, apCost: 1 }),
	Advanced({ ability: CS_AEROTHEURGE, name: 'teleportation', cooldown: 4, range: 13 }),
	Advanced({ ability: CS_AEROTHEURGE, name: 'uncannyEvasion', cooldown: 4, range: 13, apCost: 1 }),
	Master({ ability: CS_AEROTHEURGE, name: 'chainLightning', cooldown: 5, range: 13, resistedBy: AT_MAGICAL, apCost: 3, spCost: 1 }),
	Master({ ability: CS_AEROTHEURGE, name: 'closedCircuit', cooldown: 4, resistedBy: AT_MAGICAL, spCost: 2 }),
	Master({ ability: CS_AEROTHEURGE, name: 'superconductor', range: 8, resistedBy: AT_MAGICAL, apCost: 3 }),
	Master({ ability: CS_AEROTHEURGE, name: 'thunderstorm', cooldown: 5, range: 17, duration: 2, memoryCost: 3, apCost: 4, spCost: 3 }),
	Master({ ability: CS_AEROTHEURGE, name: 'tornado', cooldown: 5, range: 13 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_HUNTSMAN, name: 'erraticWisp', cooldown: 6, range: 13, apCost: 1 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_HUNTSMAN, name: 'evasiveAura', cooldown: 5, spCost: 1 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_NECROMANCER, name: 'vacuumTouch', cooldown: 5, range: 2, resistedBy: AT_MAGICAL, apCost: 1, effect }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_NECROMANCER, name: 'vacuumAura', cooldown: 5, spCost: 1 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_POLYMORPH, name: 'vaporise', cooldown: 4, apCost: 1 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_SCOUNDREL, name: 'smokeCover', cooldown: 5, apCost: 1 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_SCOUNDREL, name: 'blessedSmokeCloud', cooldown: 5, range: 13, apCost: 1, spCost: 2 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_WARFARE, name: 'breathingBubble', cooldown: 5, apCost: 1 }),
	Crafted({ ability: CS_AEROTHEURGE, otherAbility: CS_WARFARE, name: 'massBreathingBubbles', cooldown: 5, apCost: 1, spCost: 1 }),
];
