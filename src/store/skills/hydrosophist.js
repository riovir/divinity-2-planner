import { AT_MAGICAL, CS_HUNTSMAN, CS_HYDROSOPHIST, CS_POLYMORPH, CS_SCOUNDREL, CS_WARFARE } from '../constants';
import { Basic, Advanced, Master, Crafted } from './skills';

export const SKILLS = [
	Basic({ ability: CS_HYDROSOPHIST, name: 'armourOfFrost', cooldown: 4, range: 13, apCost: 1 }),
	Basic({ ability: CS_HYDROSOPHIST, name: 'hailStrike', range: 13, resistedBy: AT_MAGICAL, apCost: 3, radius: 1 }),
	Basic({ ability: CS_HYDROSOPHIST, name: 'rain', cooldown: 6, range: 8, apCost: 1, duration: 1 }),
	Basic({ ability: CS_HYDROSOPHIST, name: 'restoration', cooldown: 4, range: 13, apCost: 1 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'cryogenicStasis', cooldown: 4, range: 13 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'globalCooling', resistedBy: AT_MAGICAL, apCost: 1 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'healingRitual', cooldown: 5, range: 13 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'iceBreaker', apCost: 1 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'iceFan', cooldown: 4, range: 13, resistedBy: AT_MAGICAL, apCost: 3, radius: 2 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'soothingCold', apCost: 1 }),
	Advanced({ ability: CS_HYDROSOPHIST, name: 'winterBlast', range: 13, resistedBy: AT_MAGICAL }),
	Master({ ability: CS_HYDROSOPHIST, name: 'arcaneStitch', cooldown: 4, range: 13, apCost: 3, spCost: 1 }),
	Master({ ability: CS_HYDROSOPHIST, name: 'deepFreeze', cooldown: 6, range: 8, resistedBy: AT_MAGICAL, apCost: 4 }),
	Master({ ability: CS_HYDROSOPHIST, name: 'hailStorm', cooldown: 4, range: 13, resistedBy: AT_MAGICAL, apCost: 4, spCost: 3, radius: 3 }),
	Master({ ability: CS_HYDROSOPHIST, name: 'steamLance', range: 13, spCost: 2 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_WARFARE, name: 'cleanseWounds', cooldown: 5, range: 2 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_HUNTSMAN, name: 'cryotherapy', apCost: 1 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_POLYMORPH, name: 'healingTears', cooldown: 5, apCost: 1 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_WARFARE, name: 'massCleanseWounds', cooldown: 6, spCost: 1 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_HUNTSMAN, name: 'massCryotherapy', cooldown: 5, spCost: 1 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_SCOUNDREL, name: 'vampiricHunger', cooldown: 5, range: 2, apCost: 1 }),
	Crafted({ ability: CS_HYDROSOPHIST, otherAbility: CS_SCOUNDREL, name: 'vampiricHungerAura', cooldown: 5, apCost: 1, spCost: 1 }),
];
