import {
	AT_MAGICAL, EF_BURNING, EF_NECROFIRE,
	CS_HUNTSMAN, CS_NECROMANCER, CS_POLYMORPH, CS_PYROKINETIC, CS_SCOUNDREL, CS_WARFARE,
} from '../constants';
import { Basic, Advanced, Master, Crafted, Effect } from './skills';

const effect = Effect({ name: [EF_BURNING], duration: 2 });
const effectNecrofire = Effect({ name: [EF_NECROFIRE], duration: 2 });
const effectShort = Effect({ name: [EF_BURNING], duration: 1 });

export const SKILLS = [
	Basic({ ability: CS_PYROKINETIC, name: 'haste', range: 13, apCost: 1 }),
	Basic({ ability: CS_PYROKINETIC, name: 'ignition', cooldown: 2, resistedBy: AT_MAGICAL, apCost: 1, effect }),
	Basic({ ability: CS_PYROKINETIC, name: 'peaceOfMind', range: 13, apCost: 1 }),
	Basic({ ability: CS_PYROKINETIC, name: 'searingDaggers', range: 13, resistedBy: AT_MAGICAL, explode: 1, effect }),
	Advanced({ ability: CS_PYROKINETIC, name: 'fireWhip', range: 8, resistedBy: AT_MAGICAL, apCost: 3, effect: effectShort }),
	Advanced({ ability: CS_PYROKINETIC, name: 'fireball', cooldown: 4, range: 13, resistedBy: AT_MAGICAL, radius: 3, effect }),
	Advanced({ ability: CS_PYROKINETIC, name: 'flamingTongues', cooldown: 5, apCost: 1 }),
	Advanced({ ability: CS_PYROKINETIC, name: 'laserRay', range: 20, resistedBy: AT_MAGICAL, apCost: 3, effect }),
	Advanced({ ability: CS_PYROKINETIC, name: 'spontaneousCombustion', range: 13 }),
	Advanced({ ability: CS_PYROKINETIC, name: 'supernova', cooldown: 6, apCost: 3 }),
	Master({ ability: CS_PYROKINETIC, name: 'epidemicOfFire', cooldown: 6, range: 13, resistedBy: AT_MAGICAL, apCost: 3, spCost: 2, effect: effectNecrofire }),
	Master({ ability: CS_PYROKINETIC, name: 'firebrand', apCost: 1 }),
	Master({ ability: CS_PYROKINETIC, name: 'flamingCrescendo', cooldown: 5, range: 13, apCost: 1 }),
	Master({ ability: CS_PYROKINETIC, name: 'meteorShower', cooldown: 4, range: 13, apCost: 4, spCost: 3, radius: 3 }),
	Master({ ability: CS_PYROKINETIC, name: 'summonFireSlug', cooldown: 6, range: 13, apCost: 1, spCost: 1, duration: 5 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_POLYMORPH, name: 'bleedFire', range: 13, resistedBy: AT_MAGICAL, apCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_NECROMANCER, name: 'corpseExplosion', cooldown: 4, range: 13, apCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_HUNTSMAN, name: 'deployMassTraps', cooldown: 5, range: 13, apCost: 3, spCost: 1, radius: 3 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_NECROMANCER,  name: 'massCorpseExplosion', cooldown: 2, apCost: 1, spCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_SCOUNDREL, name: 'massSabotage', cooldown: 5, range: 13, apCost: 2, spCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_WARFARE, name: 'masterOfSparks', cooldown: 5, range: 13, apCost: 1, spCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_SCOUNDREL, name: 'sabotage', cooldown: 1, range: 13, apCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_WARFARE, name: 'sparkingSwings', cooldown: 5, apCost: 1 }),
	Crafted({ ability: CS_PYROKINETIC, otherAbility: CS_HUNTSMAN, name: 'throwExplosiveTrap', range: 13, apCost: 1, radius: 3 }),
];
