import { CS_WARFARE, EH_STAFF, EH_WAND } from '../constants';
import { toSkillProp } from './index';

test('Melee skills like whirlwind work with a staff', () => {
	expect(toSkillProp('availableFor', 'whirlwind')({ mainHand: EH_WAND, [CS_WARFARE]: 5 })).toBe(false);
	expect(toSkillProp('availableFor', 'whirlwind')({ mainHand: EH_STAFF, [CS_WARFARE]: 5 })).toBe(true);
});
