import { both } from 'ramda';
import { AT_MAGICAL, AT_PHYSICAL, CS_SCOUNDREL, EF_BLEEDING, EF_RUPTURED_TENDONS, ST_SCOUNDREL_BLOOD, ST_SCOUNDREL_LIGHT, EH_DAGGER } from '../constants';
import { Basic, Advanced, Master, Effect } from './skills';

const effect = Effect({ name: EF_BLEEDING, duration: 2 });
const effectRupturedTendons = Effect({ name: EF_RUPTURED_TENDONS, duration: 2 });
const subtype = ST_SCOUNDREL_LIGHT;

export const SKILLS = [
	Basic({ ability: CS_SCOUNDREL, name: 'adrenaline', cooldown: 4, subtype: ST_SCOUNDREL_BLOOD }),
	Basic({ ability: CS_SCOUNDREL, name: 'chloroform', range: 13, resistedBy: AT_MAGICAL, apCost: 1 }),
	Advanced({ ability: CS_SCOUNDREL, name: 'cloakAndDagger', cooldown: 4, range: 13, apCost: 1 }),
	Advanced({ ability: CS_SCOUNDREL, name: 'sawtoothKnife', range: 1.8, resistedBy: AT_PHYSICAL, subtype, effect }),
	Master({ ability: CS_SCOUNDREL, name: 'windUpToy', cooldown: 6, range: 13, radius: 1, subtype }),
	requiringDagger(Basic({ ability: CS_SCOUNDREL, name: 'throwingKnife', range: 8, apCost: 1 })),
	requiringDagger(Basic({ ability: CS_SCOUNDREL, name: 'backlash', range: 8, apCost: 1, subtype })),
	requiringDagger(Advanced({ ability: CS_SCOUNDREL, name: 'corruptedBlade', cooldown: 4, range: 1.8, resistedBy: AT_PHYSICAL, apCost: 3, subtype })),
	requiringDagger(Advanced({ ability: CS_SCOUNDREL, name: 'gagOrder', cooldown: 4, range: 1.8, resistedBy: AT_MAGICAL })),
	requiringDagger(Advanced({ ability: CS_SCOUNDREL, name: 'ruptureTendons', cooldown: 5, range: 2, effect: effectRupturedTendons, subtype })),
	requiringDagger(Advanced({ ability: CS_SCOUNDREL, name: 'sleepingArms', range: 1.8, resistedBy: AT_PHYSICAL })),
	requiringDagger(Master({ ability: CS_SCOUNDREL, name: 'daggersDrawn', cooldown: 6, range: 1.8, apCost: 4, spCost: 2, subtype })),
	requiringDagger(Master({ ability: CS_SCOUNDREL, name: 'fanOfKnives', cooldown: 2, range: 8, apCost: 3, spCost: 1 })),
	requiringDagger(Master({ ability: CS_SCOUNDREL, name: 'mortalBlow', cooldown: 0, range: 1.8, spCost: 3 })),
	requiringDagger(Master({
		ability: CS_SCOUNDREL, name: 'terrifyingCruelty', cooldown: 4, range: 1.8, resistedBy: [AT_MAGICAL, AT_PHYSICAL], apCost: 3, effect, subtype })),
];

function requiringDagger({ requires, availableFor, ...rest }) {
	const alsoDagger = { ...requires, equipment: EH_DAGGER };
	const hasDagger = ({ mainHand, offHand }) => mainHand === EH_DAGGER || offHand === EH_DAGGER;
	return { requires: alsoDagger, availableFor: both(availableFor, hasDagger), ...rest };
}
