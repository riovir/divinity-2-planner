import {
	AT_MAGICAL, AT_PHYSICAL,
	CS_GEOMANCER, CS_HUNTSMAN, CS_NECROMANCER, CS_POLYMORPH, CS_SCOUNDREL, CS_WARFARE, ST_GEOMANCER_POISON, EF_POISONED, EF_ACID, EF_ENTANGLED,
} from '../constants';
import { Basic, Advanced, Master, Crafted, Effect } from './skills';

const subtype = ST_GEOMANCER_POISON;
const effect = Effect({ name: EF_POISONED, duration: 3 });
const effectAcid = Effect({ name: EF_ACID, duration: 2 });
const effectEntangled = Effect({ name: EF_ENTANGLED, duration: 2 });

export const SKILLS = [
	Basic({ ability: CS_GEOMANCER, name: 'contamination', resistedBy: AT_MAGICAL, apCost: 1, subtype }),
	Basic({ ability: CS_GEOMANCER, name: 'fortify', cooldown: 4, range: 13, apCost: 1 }),
	Basic({ ability: CS_GEOMANCER, name: 'fossilStrike', range: 13, radius: 2 }),
	Basic({ ability: CS_GEOMANCER, name: 'poisonDart', cooldown: 4, range: 13, resistedBy: AT_MAGICAL, radius: 1, subtype, effect }),
	Advanced({ ability: CS_GEOMANCER, name: 'earthquake', cooldown: 4, resistedBy: AT_PHYSICAL, apCost: 3 }),
	Advanced({ ability: CS_GEOMANCER, name: 'impalement', cooldown: 4, range: 13, resistedBy: AT_PHYSICAL }),
	Advanced({ ability: CS_GEOMANCER, name: 'mendMetal', apCost: 1 }),
	Advanced({ ability: CS_GEOMANCER, name: 'poisonWave', cooldown: 4, subtype }),
	Advanced({ ability: CS_GEOMANCER, name: 'reactiveArmour', cooldown: 6 }),
	Advanced({ ability: CS_GEOMANCER, name: 'wormTremor', cooldown: 5, range: 13, resistedBy: AT_MAGICAL, apCost: 3, effect: effectEntangled }),
	Master({ ability: CS_GEOMANCER, name: 'acidSpores', cooldown: 5, range: 13, resistedBy: AT_MAGICAL, apCost: 3, spCost: 2, radius: 2, subtype, effect }),
	Master({ ability: CS_GEOMANCER, name: 'livingWall', cooldown: 5, range: 13, duration: 2, subtype }),
	Master({ ability: CS_GEOMANCER, name: 'pyroclasticEruption', cooldown: 5, range: 13, apCost: 3, spCost: 3, radius: 3 }),
	Master({ ability: CS_GEOMANCER, name: 'siphonPoison', cooldown: 5, apCost: 1, subtype }),
	Master({ ability: CS_GEOMANCER, name: 'summonHungryFlower', cooldown: 6, range: 13, spCost: 1, duration: 5, subtype }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_NECROMANCER, name: 'corrosiveSpray',
		range: 7, resistedBy: AT_PHYSICAL, spCost: 1, subtype, effect: effectAcid }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_NECROMANCER, name: 'corrosiveTouch', cooldown: 5, range: 2, apCost: 1, subtype, effect: effectAcid }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_HUNTSMAN, name: 'dustBlast', range: 13, resistedBy: AT_MAGICAL, apCost: 3, spCost: 1, radius: 2 }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_WARFARE, name: 'massOilyCarapace', cooldown: 5, spCost: 1 }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_WARFARE, name: 'oilyCarapace', apCost: 1 }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_HUNTSMAN, name: 'throwDust', range: 13, resistedBy: AT_MAGICAL, radius: 2 }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_POLYMORPH, name: 'turnToOil', cooldown: 4, range: 13, apCost: 1 }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_SCOUNDREL, name: 'venomCoating', cooldown: 5, apCost: 1, subtype }),
	Crafted({ ability: CS_GEOMANCER, otherAbility: CS_SCOUNDREL, name: 'venomousAura', cooldown: 5, spCost: 1, subtype }),
];
