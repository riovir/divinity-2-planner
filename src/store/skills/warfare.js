import { AT_PHYSICAL, CS_WARFARE, ET_SHIELD, ET_MELEE } from '../constants';
import { Basic, Advanced, Master, requiringA } from './skills';

export const SKILLS = [
	Advanced({ ability: CS_WARFARE, name: 'enrage', cooldown: 5, range: 2 }),
	Advanced({ ability: CS_WARFARE, name: 'phoenixDive', cooldown: 4, range: 13, apCost: 1 }),
	Advanced({ ability: CS_WARFARE, name: 'provoke', cooldown: 5 }),
	Master({ ability: CS_WARFARE, name: 'challenge', cooldown: 5, range: 13, apCost: 0 }),
	Master({ ability: CS_WARFARE, name: 'guardianAngel', cooldown: 6 }),
	Master({ ability: CS_WARFARE, name: 'thickOfTheFight', cooldown: 4, spCost: 1 }),
	requiringA(ET_MELEE, Basic({ ability: CS_WARFARE, name: 'batteringRam', cooldown: 5, range: 12, resistedBy: AT_PHYSICAL })),
	requiringA(ET_MELEE, Basic({ ability: CS_WARFARE, name: 'battleStomp', cooldown: 4, range: 10, resistedBy: AT_PHYSICAL })),
	requiringA(ET_MELEE, Basic({ ability: CS_WARFARE, name: 'cripplingBlow', range: 1.8, resistedBy: AT_PHYSICAL })),
	requiringA(ET_MELEE, Advanced({ ability: CS_WARFARE, name: 'blitzAttack', range: 8 })),
	requiringA(ET_MELEE, Advanced({ ability: CS_WARFARE, name: 'whirlwind' })),
	requiringA(ET_MELEE, Master({ ability: CS_WARFARE, name: 'onslaught', cooldown: 6, range: 1.8, apCost: 4, spCost: 2 })),
	requiringA(ET_MELEE, Master({ ability: CS_WARFARE, name: 'overpower', range: 1.8, resistedBy: AT_PHYSICAL, spCost: 3 })),
	requiringA(ET_SHIELD, Basic({ ability: CS_WARFARE, name: 'bouncingShield', cooldown: 2, range: 13 })),
	requiringA(ET_SHIELD, Advanced({ ability: CS_WARFARE, name: 'deflectiveBarrier', cooldown: 5 })),
];
