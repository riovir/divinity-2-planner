import { always, curry, pipe, prop, propEq } from 'ramda';
import {
	A_STRENGTH, A_FINESSE, A_INTELLIGENCE, A_CONSTITUTION, A_MEMORY, A_WITS,
	DS_CARRY_WEIGHT, DS_OBJECT_WEIGHT, DS_DAMAGE_STRENGTH, DS_DAMAGE_FINESSE, DS_DAMAGE_INTELLIGENCE,
	DS_VITALITY, DS_MEMORY_SLOT, DS_INITIATIVE, DS_CRIT_CHANCE,
} from './constants';
import { Bonus } from './bonuses';

const BASE = 10;
const DAMAGE_PER_POINT = 5;

export const ALL_ATTRIBUTES = [
	Attribute({ name: A_STRENGTH, bonusesFor: strengthBonusesFor }),
	Attribute({ name: A_FINESSE, bonusesFor: finesseBonusesFor }),
	Attribute({ name: A_INTELLIGENCE, bonusesFor: intelligenceBonusesFor }),
	Attribute({ name: A_CONSTITUTION, bonusesFor: constitutionBonusesFor }),
	Attribute({ name: A_MEMORY, bonusesFor: memoryBonusesFor }),
	Attribute({ name: A_WITS, bonusesFor: witsBonusesFor }),
];

export const toAttributeProp = curry((propName, talentName) => {
	return pipe(toAttributeDetails, prop(propName))(talentName);
});

function toAttributeDetails(attributeName) {
	const details = ALL_ATTRIBUTES.find(propEq('name', attributeName));
	if (!details) { throw new Error(`Unknown attribute: ${attributeName}`); }
	return details;
}

function strengthBonusesFor(character) {
	const points = pointsOf(character);
	return {
		[DS_DAMAGE_STRENGTH]: AttributeBonus({ name: A_STRENGTH, value: points(A_STRENGTH) * DAMAGE_PER_POINT }),
		[DS_CARRY_WEIGHT]: AttributeBonus({ name: A_STRENGTH, value: points(A_STRENGTH) * 10 }),
		[DS_OBJECT_WEIGHT]: AttributeBonus({ name: A_STRENGTH, value: Math.ceil(points(A_STRENGTH) * 15 / 2) }),
	};
}

function finesseBonusesFor(character) {
	const points = pointsOf(character);
	return { [DS_DAMAGE_FINESSE]: AttributeBonus({ name: A_FINESSE, value: points(A_FINESSE) * DAMAGE_PER_POINT })	};
}

function intelligenceBonusesFor(character) {
	const points = pointsOf(character);
	return { [DS_DAMAGE_INTELLIGENCE]: AttributeBonus({ name: A_INTELLIGENCE, value: points(A_INTELLIGENCE) * DAMAGE_PER_POINT }) };
}

function constitutionBonusesFor(character) {
	const points = pointsOf(character);
	const multiplier = points(A_CONSTITUTION) * 7 / 100;
	const value = Math.floor(character[DS_VITALITY] * multiplier);
	return { [DS_VITALITY]: AttributeBonus({ name: A_CONSTITUTION, value }) };
}

function memoryBonusesFor(character) {
	const points = pointsOf(character);
	return { [DS_MEMORY_SLOT]: AttributeBonus({ name: A_MEMORY, value: points(A_MEMORY) }) };
}

function witsBonusesFor(character) {
	const points = pointsOf(character);
	return {
		[DS_INITIATIVE]: AttributeBonus({ name: A_WITS, value: points(A_WITS) }),
		[DS_CRIT_CHANCE]: AttributeBonus({ name: A_WITS, value: points(A_WITS) }),
	};
}

const pointsOf = curry((character, attributeName) => character[attributeName] - BASE);

function Attribute({ name = '', resourceKey = `attribute.${name}`, bonusesFor = always() }) {
	return { name, resourceKey, bonusesFor };
}

export function AttributeBonus({ name, resourceKey = `attribute.${name}`, ...rest }) {
	return Bonus({ name, resourceKey, ...rest });
}
