const { prop } = require('ramda');
import { FaneRogueBuild } from 'test/utils';
import { ALL_SKILLS } from '../skills';
import { serialize, deserialize } from './index';

const V1_STATE = 'v1-W3tpZDozLGxldmVsOjcsb3JpZ2luOmZhbmUscmFjZTpodW1hbix1bmR' +
	'lYWQ6dHJ1ZSxjb21iYXRQcm9ncmVzc2lvbjpbc2MsaHUscG8sc2Msc2MsaHUsc2MsaHVdLGNp' +
	'dmlsUHJvZ3Jlc3Npb246W3NuLHNuLGx1XSxhdHRyaWJ1dGVQcm9ncmVzc2lvbjpbZmksd2ksb' +
	'WUsbWUsZmksZmksZmksZmksd2ksbWUsd2ksZmksd2ksZmksd2ldLHRhbGVudHM6W0xPLFRIXS' +
	'xmcmVlVGFsZW50czpbc3BpZGVyc0tpc3NdfV0=';

const V2_0_STATE = 'v2-1,21,s,swsiswimmmmmmmmmmmmmmmccccccccccccciisssii,twt__' +
		'_________________,ssss__,wx___,';
const V2_1_STATE = 'v2-1,21,s,swsiswimmmmmmmmmmmmmmmccccccccccccciisssii,twt__' +
		'_________________,ssss__,wx___,T';
const V2_2_STATE = 'v2-1,20,r,ccccmmmmmmmiiiiiiiissffffffffffiiifiiffimffmmi,sooooouuuuusssagypeea,ppthhh,gwJBI,ke,T,d,d-' +
		'2,20,s,ccccciiiffssmmmmmmmwwwwwwwwwwwwwiiii_cccw,ssssseeeppppyyggaaa_h,uuuu_t,JDwr_,ecij,T,w,h';

const V2_3_STATE = 'v2-1,20,r,ccccmmmmmmmwwwwwwiissffffffffffwwwfwwffwwffww_,sooooouuuuusssagypeea,ppthhh,gwJBI,f,T,d,d-' +
		'2,20,s,ccccciiiffssmmmmmmmwwwwwwwwwwwwwiiiiiiii_,ssssseeeppppyyggaaaah,uuuuut,JDwrI,ec,T,w,h-' +
		'3,20,l,sssiiiffffffffffwwwwwwwwwmmmmmmmwwwwwccff,rrrrhhhhsswweygapsssr,bbbbbt,wIrmq,g,F,b,_-' +
		'4,20,f,iiiccciiciimmmmmmmwwwwwwwwwwffsswwiiiiiwi,sellagyepgggaypssssll,sssstl,JIDrw,g,F,w,w';

const V3_0_STATE = 'v3-1,20,r,4c7m6w2i2s10f3wf2w2f2w2f2wc,s5o5u3sagyp2ea,2pt3h,gwJBI,f,T,d,d-' +
		'2,20,s,5c3i2f2s7m13w9i,5s3e4p2y2g4ah,5ut,JDwrI,ec,T,w,h-' +
		'3,20,l,3s3i10f9w7m5w2c2f,4r4h2s2weygap3sr,5bt,wIrmq,g,F,b,_-' +
		'4,20,f,3i3c2ic2i7m10w2f2s2w5iwi,se2lagyep3gayp4s2l,4stl,JIDrw,g,F,w,w';

test('Serializes state to a string', () => {
	expect(typeof serialize({ builds: [FaneRogueBuild()] })).toBe('string');
});

test('Deserializes current format', () => {
	const state = { builds: [FaneRogueBuild()] };
	expect(deserialize(serialize(state))).toEqual(state);
});

test('Deserializes all skills', () => {
	const skillNames = ALL_SKILLS
			.filter(({ memoryCost }) => memoryCost > 0)
			.map(prop('name'));
	skillNames.forEach(skill => {
		const state = { builds: [FaneRogueBuild({ skills: [skill] })] };
		expect(deserialize(serialize(state))).toEqual(state);
	});
});

test('Deserializes V1 format', () => {
	expectValid(deserialize(V1_STATE));
});

test('Deserializes V2 format', () => {
	expectValid(deserialize(V2_0_STATE));
	expectValid(deserialize(V2_1_STATE));
	expectValid(deserialize(V2_2_STATE));
	expectValid(deserialize(V2_3_STATE));
});

test('Deserializes V3 format', () => {
	expectValid(deserialize(V3_0_STATE));
});

test('Throws error on unexpected format', () => {
	expect(() => deserialize('v0-outdated-format')).toThrow('error.unsupportedStateVersion');
});

function expectValid(state) {
	expect(state).toEqual({ builds: expect.arrayContaining([]) });
	expect(state.builds.length).toBeGreaterThan(0);
	state.builds.forEach(build => {
		expect(build).toEqual(expect.objectContaining({
			id: expect.anything(),
			level: expect.anything(),
			origin: expect.anything(),
			active: expect.any(Boolean),
			attributeProgression: expect.anything(),
			civilProgression: expect.anything(),
			combatProgression: expect.anything(),
			talents: expect.anything(),
			freeTalents: expect.anything(),
			skills: expect.anything(),
		}));
		const { mainHand, offHand } = state.builds[0];
		expect(mainHand).toBeDefined();
		expect(offHand).toBeDefined();
	});
}
