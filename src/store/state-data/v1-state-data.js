import { curry, without } from 'ramda';

const NULL_SHORTHAND = 'nu';
const SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	// Attributes
	st: 'strength', fi: 'finesse', in: 'intelligence',
	co: 'constitution', me: 'memory', wi: 'wits',

	// Combat abilities
	wa: 'warfare', sc: 'scoundrel', hu: 'huntsman',
	ae: 'aerotheurge', ge: 'geomancer', hy: 'hydrosophist', py: 'pyrokinetic',
	ne: 'necromancer', po: 'polymorph', su: 'summoning',
	du: 'dualWielding', ra: 'ranged', si: 'singleHanded', tw: 'twoHanded',
	le: 'leadership', pe: 'perserverance', re: 'retribution',

	// Civil abilities
	te: 'telekinesis', lo: 'loremaster', sn: 'sneaking', th: 'thievery',
	ba: 'bartering', pr: 'persuasion', lu: 'luckyCharm',

	// Talents
	AL: 'allSkilledUp', AM: 'ambidextrous', AR: 'arrowRecovery', BI: 'biggerAndBetter', CO: 'comebackKid',
	CE: 'corpseEater', DE: 'demon', DU: 'duckDuckGoose', DW: 'dwarvenGuile', EL: 'elementalAffinity',
	ES: 'escapist', EX: 'executioner', ER: 'elementalRanger', FA: 'farOutMan', FI: 'fiveStarDiner',
	GL: 'glassCannon', GU: 'guerrilla', HO: 'hothead', IC: 'iceKing', IN: 'ingenious',
	LE: 'leech', LI: 'livingArmour', LO: 'lonewolf', MN: 'mnemonic', MO: 'morningPerson',
	OP: 'opportunist', PA: 'parryMaster', PE: 'petPal', PI: 'pictureOfHealth', SA: 'savageSortilege',
	SL: 'slingshot', SO: 'sophisticated', ST: 'stench', SU: 'sturdy', TH: 'thePawn',
	TO: 'torturer', UN: 'undead', US: 'unstable', WA: 'walkItOff', WH: 'whatARush',
};

function restoreQuotes(json) {
	return json
			.replace(/([^\][{},":]+)/g, '"$1"')
			.replace(/"id":"([^"]+)"/g, '"id":$1')
			.replace(/"level":"([^"]+)"/g, '"level":$1')
			.replace(/"undead":"([^"]+)"/g, '"undead":$1');
}

export function deserialize(data) {
	const smallBuilds = JSON.parse(restoreQuotes(atob(data)));
	return { builds: smallBuilds.map(restoreBuild) };
}

function restoreBuild(build) {
	const restore = curry(convertArrayField)(SHORTHANDS, build);
	const clearDeprecatedTalents = build => ({ ...build, freeTalents: without(['spidersKiss'], build.freeTalents) });
	return clearDeprecatedTalents({
		...build,
		...restore('attributeProgression'),
		...restore('combatProgression'),
		...restore('civilProgression'),
		...restore('talents'),
	});
}

function convertArrayField(shorthands, build, arrayField) {
	const fallback = value => value === NULL_SHORTHAND ? null : value;
	const shorten = value => shorthands[value] || fallback(value);
	return { [arrayField]: build[arrayField].map(shorten) };
}

