import { __, defaultTo, identity, invertObj, join, map, merge, objOf, pipe, prop, split, splitEvery } from 'ramda';
import { compress, restore } from './counted-characters';

const BUILD_SEPARATOR = '-';
const PROP_SEPARATOR = ',';
const UNKNOWN_ELEMENT = '*';
const NULL_SHORTHAND = '_';

const BOOLEAN_SHORTHANDS = { T: true,	F: false };

const ORIGIN_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	b: 'beast', f: 'fane', i: 'ifan', l: 'lohse', r: 'redPrince', s: 'sebille',
	E: 'customElfFemale', D: 'customDwarfFemale', H: 'customHumanFemale', I: 'customLizardFemale',
	J: 'customElfMale', K: 'customDwarfMale', M: 'customHumanMale', N: 'customLizardMale',
	L: 'customUndeadElfFemale', W: 'customUndeadDwarfFemale', U: 'customUndeadHumanFemale', Z: 'customUndeadLizardFemale',
	O: 'customUndeadElfMale', P: 'customUndeadDwarfMale', Q: 'customUndeadHumanMale', R: 'customUndeadLizardMale',
};

const ATTRIBUTE_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	s: 'strength', f: 'finesse', i: 'intelligence',
	c: 'constitution', m: 'memory', w: 'wits',
};

const COMBAT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	w: 'warfare', s: 'scoundrel', h: 'huntsman',
	a: 'aerotheurge', g: 'geomancer', y: 'hydrosophist', p: 'pyrokinetic',
	e: 'necromancer', o: 'polymorph', u: 'summoning',
	d: 'dualWielding', r: 'ranged', i: 'singleHanded', t: 'twoHanded',
	l: 'leadership', v: 'perserverance', b: 'retribution',
};

const CIVIL_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	t: 'telekinesis', l: 'loremaster', s: 'sneaking', h: 'thievery',
	b: 'bartering', p: 'persuasion', u: 'luckyCharm',
};

const TALENT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	a: 'allSkilledUp', b: 'ambidextrous', c: 'arrowRecovery', d: 'biggerAndBetter', e: 'comebackKid',
	f: 'corpseEater', g: 'demon', h: 'duckDuckGoose', i: 'dwarvenGuile', j: 'elementalAffinity',
	k: 'escapist', l: 'executioner', m: 'elementalRanger', n: 'farOutMan', o: 'fiveStarDiner',
	p: 'glassCannon', q: 'guerrilla', r: 'hothead', s: 'iceKing', t: 'ingenious',
	u: 'leech', v: 'livingArmour', w: 'lonewolf', x: 'mnemonic', y: 'morningPerson',
	z: 'opportunist', A: 'parryMaster', B: 'petPal', C: 'pictureOfHealth', D: 'savageSortilege',
	E: 'slingshot', F: 'sophisticated', G: 'stench', H: 'sturdy', I: 'thePawn',
	J: 'torturer', K: 'undead', L: 'unstable', M: 'walkItOff', N: 'whatARush',
};

const FREE_TALENT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	a: 'rooted', b: 'spidersKissGC', c: 'tradeSecrets', d: 'spidersKissTO', e: 'spidersKissCQ',
	f: 'spidersKissSD', g: 'spidersKissTC',
	h: 'trainingAuthority', i: 'trainingIntelligence', j: 'trainingWits',
	k: 'trainingFinesse', l: 'trainingConstitution', m: 'trainingStrength',
};

const EQUIPMENT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	b: 'bow', c: 'crossbow', l: 'mace', d: 'dagger', s: 'sword',
	w: 'wand', h: 'shield', a: '2hAxe', m: '2hMace', o: '2hSword',
	p: 'spear', t: 'staff', x: 'axe',
};

const SKILL_SHORTHANDS = {
	[NULL_SHORTHAND.repeat(2)]: null,
	null: UNKNOWN_ELEMENT.repeat(2),

	Aa: 'apportation', As: 'blessedSmokeCloud', Ab: 'blindingRadiance', Ay: 'breathingBubble', Al: 'chainLightning',
	Ac: 'closedCircuit', Az: 'dazingBolt', Ad: 'electricDischarge', Aw: 'erraticWisp', Ai: 'evasiveAura',
	Af: 'favourableWind', Aq: 'massBreathingBubbles', An: 'netherSwap', Ap: 'pressureSpike', At: 'shockingTouch',
	Am: 'smokeCover', Ar: 'superconductor', Ae: 'teleportation', Ah: 'thunderstorm', Ao: 'tornado',
	Au: 'uncannyEvasion', Av: 'vacuumAura', Ak: 'vacuumTouch', Ax: 'vaporise',

	Ga: 'acidSpores', Gc: 'contamination', Go: 'corrosiveSpray', Gr: 'corrosiveTouch', Gd: 'dustBlast',
	Ge: 'earthquake', Gf: 'fortify', Gs: 'fossilStrike', Gi: 'impalement', Gl: 'livingWall',
	Gm: 'massOilyCarapace', Gn: 'mendMetal', Gy: 'oilyCarapace', Gp: 'poisonDart', Gw: 'poisonWave',
	Gu: 'pyroclasticEruption', Gt: 'reactiveArmour', Gh: 'siphonPoison', Gb: 'summonHungryFlower', Gg: 'throwDust',
	Gj: 'turnToOil', Gv: 'venomCoating', Gx: 'venomousAura', Gz: 'wormTremor',

	Ha: 'arrowSpray', Hr: 'arrowStorm', Hs: 'assassinate', Hb: 'ballisticShot', Hg: 'barrage',
	He: 'elementalArrowheads', Hf: 'farsight', Hi: 'firstAid', Hl: 'glitterDust', Hm: 'marksmansFang',
	Hp: 'pinDown', Hc: 'reactiveShot', Ho: 'ricochet', Hk: 'skyShot', Ht: 'tacticalRetreat',

	Ya: 'arcaneStitch', Yr: 'armourOfFrost', Yc: 'cleanseWounds', Yy: 'cryogenicStasis', Yo: 'cryotherapy',
	Yd: 'deepFreeze', Yg: 'globalCooling', Yh: 'hailStorm', Yi: 'hailStrike', Ye: 'healingRitual',
	Yl: 'healingTears', Yb: 'iceBreaker', Yf: 'iceFan', Ym: 'massCleanseWounds', Ys: 'massCryotherapy',
	Yn: 'rain', Yt: 'restoration', Yq: 'soothingCold', Yx: 'steamLance', Yz: 'vampiricHunger',
	Yv: 'vampiricHungerAura', Yw: 'winterBlast',

	Nb: 'blackShroud', Nl: 'bloodStorm', No: 'bloodSucker', Nn: 'boneCage', Nd: 'deathWish',
	Ne: 'decayingTouch', Ng: 'graspOfTheStarved', Ni: 'infect', Na: 'lastRites', Nv: 'livingOnTheEdge',
	Nm: 'mosquitoSwarm', Nr: 'rainingBlood', Ns: 'raiseBloatedCorpse', Nw: 'raiseBoneWidow', Nc: 'shacklesOfPain',
	Nt: 'silencingStare', Nh: 'totemsOfTheNecromancer',

	Pa: 'apotheosis', Pb: 'bullHorns', Pc: 'chameleonCloak', Pi: 'chickenClaw', Pe: 'equalise',
	Pf: 'flamingSkin', Pl: 'flaySkin', Po: 'forcedExchange', Ph: 'heartOfSteel', Py: 'icySkin',
	Pj: 'jellyfishSkin', Pm: 'medusaHead', Pp: 'poisonousSkin', Ps: 'skinGraft', Pd: 'spiderLegs',
	Pr: 'spreadYourWings', Pu: 'summonOilyBlob', Pt: 'tentacleLash', Pn: 'terrainTransmutation',

	Fb: 'bleedFire', Fc: 'corpseExplosion', Fd: 'deployMassTraps', Fe: 'epidemicOfFire', Ff: 'fireWhip',
	Fi: 'fireball', Fr: 'firebrand', Fl: 'flamingCrescendo', Fa: 'flamingTongues', Fh: 'haste',
	Fg: 'ignition', Fs: 'laserRay', Fm: 'massCorpseExplosion', Fo: 'massSabotage', Fp: 'masterOfSparks',
	Fw: 'meteorShower', Fn: 'peaceOfMind', Fj: 'sabotage', Fq: 'searingDaggers', Fy: 'sparkingSwings',
	Fx: 'spontaneousCombustion', Fz: 'summonFireSlug', Fv: 'supernova', Ft: 'throwExplosiveTrap',

	Sa: 'adrenaline', Sb: 'backlash', Sc: 'chloroform', Sl: 'cloakAndDagger', So: 'corruptedBlade',
	Sd: 'daggersDrawn', Sf: 'fanOfKnives', Sg: 'gagOrder', Sm: 'mortalBlow', Sr: 'ruptureTendons',
	Ss: 'sawtoothKnife', Se: 'sleepingArms', St: 'terrifyingCruelty', Sh: 'throwingKnife', Sw: 'windUpToy',

	Ca: 'acidInfusion', Cc: 'cannibalise', Co: 'conjureIncarnate', Ch: 'controlVoidwoken', Cu: 'cursedElectricInfusion',
	Cd: 'dimensionalBolt', Cm: 'dominateMind', Cr: 'doorToEternity', Ce: 'electricInfusion', Cl: 'elementalTotem',
	Ct: 'etherealStorm', Cf: 'farsightInfusion', Ci: 'fireInfusion', Cn: 'iceInfusion', Cg: 'necrofireInfusion',
	Cp: 'planarGateway', Cb: 'poisonInfusion', Cw: 'powerInfusion', Cy: 'rallyingCry', Cs: 'shadowInfusion',
	Cj: 'soulMate', Ck: 'summonCatFamiliar', Cq: 'summonCondor', Cv: 'summonDragonling', Cx: 'summonInnerDemon',
	CS: 'supercharger', CW: 'warpInfusion', CA: 'waterInfusion',

	Wb: 'batteringRam', Wa: 'battleStomp', Wl: 'blitzAttack', Wo: 'bouncingShield', Wc: 'challenge',
	Wr: 'cripplingBlow', Wd: 'deflectiveBarrier', We: 'enrage', Wg: 'guardianAngel', Wn: 'onslaught',
	Wv: 'overpower', Wp: 'phoenixDive', Wk: 'provoke', Wt: 'thickOfTheFight', Ww: 'whirlwind',

	Xb: 'bless', Xc: 'curse', Xd: 'deafeningShriek',
};

const PROPS = [
	NumericProp({ name: 'id', defaults: 1 }),
	NumericProp({ name: 'level', defaults: 1 }),
	StringProp({ name: 'origin', shorthands: ORIGIN_SHORTHANDS, defaults: 'sebille' }),
	ArrayProp({ name: 'attributeProgression', shorthands: ATTRIBUTE_SHORTHANDS }),
	ArrayProp({ name: 'combatProgression', shorthands: COMBAT_SHORTHANDS }),
	ArrayProp({ name: 'civilProgression', shorthands: CIVIL_SHORTHANDS }),
	ArrayProp({ name: 'talents', shorthands: TALENT_SHORTHANDS }),
	ArrayProp({ name: 'freeTalents', shorthands: FREE_TALENT_SHORTHANDS }),
	BooleanProp({ name: 'active', defaults: true }),
	StringProp({ name: 'mainHand', shorthands: EQUIPMENT_SHORTHANDS, defaults: 'sword' }),
	StringProp({ name: 'offHand', shorthands: EQUIPMENT_SHORTHANDS, defaults: null }),
	ArrayProp({ name: 'skills', shorthands: SKILL_SHORTHANDS, shorthandLength: 2 }),
];

export function serialize({ builds }) {
	return pipe(
			map(compressBuild),
			join(BUILD_SEPARATOR),
	)(builds);
}

export function deserialize(data) {
	const restoreBuilds = pipe(split(BUILD_SEPARATOR), map(restoreBuild));
	return { builds: restoreBuilds(data) };
}


function compressBuild(build) {
	return PROPS.reduce((data, prop) => {
		const fragment = prop.compress(build);
		return data ? `${data}${PROP_SEPARATOR}${fragment}` : fragment;
	}, '');
}

function restoreBuild(data) {
	const fragments = data.split(PROP_SEPARATOR);
	return PROPS.reduce((build, prop) => {
		const index = Object.keys(build).length;
		return merge(build, prop.restore(fragments[index]));
	}, {});
}

function NumericProp({ name, defaults }) {
	return {
		compress: prop(name),
		restore: pipe(defaultTo(defaults), Number, objOf(name)),
	};
}

function BooleanProp({ name, defaults = false }) {
	return {
		compress: pipe(prop(name), Boolean, to(BOOLEAN_SHORTHANDS)),
		restore: pipe(from(BOOLEAN_SHORTHANDS), defaultTo(defaults), objOf(name)),
	};
}

function StringProp({ name, shorthands, defaults }) {
	return {
		compress: pipe(prop(name), to(shorthands)),
		restore: pipe(from(shorthands), defaultTo(defaults), objOf(name)),
	};
}

function ArrayProp({ name, shorthands, shorthandLength = 1 }) {
	const doCompress = shorthandLength === 1 ? compress : identity;
	const doRestore = shorthandLength === 1 ? restore : identity;
	return {
		compress: pipe(prop(name), map(to(shorthands)), join(''), doCompress),
		restore: pipe(defaultTo([]), doRestore, splitEvery(shorthandLength), map(from(shorthands)), objOf(name)),
	};
}

function to(shorthands) {
	return pipe(prop(__, invertObj(shorthands)), defaultTo(UNKNOWN_ELEMENT));
}
function from(shorthands) {
	return pipe(prop(__, shorthands), defaultTo(null));
}
