import { contains, complement, flip, filter, join, pipe, reduce } from 'ramda';

export function compress(text) {
	const shortener = reduce(Shortener, {}, sanitize(text));
	return Shortener(shortener).text;
}

function sanitize(text) {
	if (typeof text !== 'string') { return ''; }
	const notNumber = complement(flip(contains)('0123456789'));
	return pipe(String, filter(notNumber), join(''))(text);
}

function Shortener({ text = '', lastChar = '', combo = 0 } = {}, newChar = '') {
	const appendFragment = !newChar || lastChar !== newChar;
	const newFragment = () => combo > 1 ? combo + lastChar : lastChar;
	const valueOf = char => char ? 1 : 0;
	return {
		lastChar: newChar,
		text: appendFragment ? text + newFragment() : text,
		combo: appendFragment ? valueOf(newChar) : combo + 1,
	};
}

export function restore(text) {
	if (!text || typeof text !== 'string') { return ''; }
	return reduce(Restorer, {}, text).text;
}

function Restorer({ text = '', numbers = '' } = {}, newChar = '') {
	const newNumber = parseInt(newChar);
	const newFragment = () => numbers ? newChar.repeat(parseInt(numbers)) : newChar;
	return {
		text: isNaN(newNumber) ? text + newFragment() : text,
		numbers: isNaN(newNumber) ? '' : `${numbers}${newNumber}`,
	};
}
