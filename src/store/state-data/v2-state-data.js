import { __, defaultTo, invertObj, join, map, merge, objOf, pipe, prop, split } from 'ramda';

const BUILD_SEPARATOR = '-';
const PROP_SEPARATOR = ',';
const UNKNOWN_ELEMENT = '*';
const NULL_SHORTHAND = '_';

const BOOLEAN_SHORTHANDS = { T: true,	F: false };

const ORIGIN_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	b: 'beast', f: 'fane', i: 'ifan', l: 'lohse', r: 'redPrince', s: 'sebille',
	E: 'customElfFemale', D: 'customDwarfFemale', H: 'customHumanFemale', I: 'customLizardFemale',
	J: 'customElfMale', K: 'customDwarfMale', M: 'customHumanMale', N: 'customLizardMale',
	L: 'customUndeadElfFemale', W: 'customUndeadDwarfFemale', U: 'customUndeadHumanFemale', Z: 'customUndeadLizardFemale',
	O: 'customUndeadElfMale', P: 'customUndeadDwarfMale', Q: 'customUndeadHumanMale', R: 'customUndeadLizardMale',
};

const ATTRIBUTE_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	s: 'strength', f: 'finesse', i: 'intelligence',
	c: 'constitution', m: 'memory', w: 'wits',
};

const COMBAT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	w: 'warfare', s: 'scoundrel', h: 'huntsman',
	a: 'aerotheurge', g: 'geomancer', y: 'hydrosophist', p: 'pyrokinetic',
	e: 'necromancer', o: 'polymorph', u: 'summoning',
	d: 'dualWielding', r: 'ranged', i: 'singleHanded', t: 'twoHanded',
	l: 'leadership', v: 'perserverance', b: 'retribution',
};

const CIVIL_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	t: 'telekinesis', l: 'loremaster', s: 'sneaking', h: 'thievery',
	b: 'bartering', p: 'persuasion', u: 'luckyCharm',
};

const TALENT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	a: 'allSkilledUp', b: 'ambidextrous', c: 'arrowRecovery', d: 'biggerAndBetter', e: 'comebackKid',
	f: 'corpseEater', g: 'demon', h: 'duckDuckGoose', i: 'dwarvenGuile', j: 'elementalAffinity',
	k: 'escapist', l: 'executioner', m: 'elementalRanger', n: 'farOutMan', o: 'fiveStarDiner',
	p: 'glassCannon', q: 'guerrilla', r: 'hothead', s: 'iceKing', t: 'ingenious',
	u: 'leech', v: 'livingArmour', w: 'lonewolf', x: 'mnemonic', y: 'morningPerson',
	z: 'opportunist', A: 'parryMaster', B: 'petPal', C: 'pictureOfHealth', D: 'savageSortilege',
	E: 'slingshot', F: 'sophisticated', G: 'stench', H: 'sturdy', I: 'thePawn',
	J: 'torturer', K: 'undead', L: 'unstable', M: 'walkItOff', N: 'whatARush',
};

const FREE_TALENT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	a: 'rooted', b: 'spidersKissGC', c: 'tradeSecrets', d: 'spidersKissTO', e: 'spidersKissCQ',
	f: 'spidersKissSD', g: 'spidersKissTC',
	h: 'trainingAuthority', i: 'trainingIntelligence', j: 'trainingWits',
	k: 'trainingFinesse', l: 'trainingConstitution', m: 'trainingStrength',
};

const EQUIPMENT_SHORTHANDS = {
	[NULL_SHORTHAND]: null,
	null: UNKNOWN_ELEMENT,
	b: 'bow', c: 'crossbow', l: 'mace', d: 'dagger', s: 'sword',
	w: 'wand', h: 'shield', a: '2hAxe', m: '2hMace', o: '2hSword',
	p: 'spear', t: 'staff', x: 'axe',
};

const PROPS = [
	NumericProp({ name: 'id', defaults: 1 }),
	NumericProp({ name: 'level', defaults: 1 }),
	StringProp({ name: 'origin', shorthands: ORIGIN_SHORTHANDS, defaults: 'sebille' }),
	ArrayProp({ name: 'attributeProgression', shorthands: ATTRIBUTE_SHORTHANDS }),
	ArrayProp({ name: 'combatProgression', shorthands: COMBAT_SHORTHANDS }),
	ArrayProp({ name: 'civilProgression', shorthands: CIVIL_SHORTHANDS }),
	ArrayProp({ name: 'talents', shorthands: TALENT_SHORTHANDS }),
	ArrayProp({ name: 'freeTalents', shorthands: FREE_TALENT_SHORTHANDS }),
	BooleanProp({ name: 'active', defaults: true }),
	StringProp({ name: 'mainHand', shorthands: EQUIPMENT_SHORTHANDS, defaults: 'sword' }),
	StringProp({ name: 'offHand', shorthands: EQUIPMENT_SHORTHANDS, defaults: null }),
];

export function serialize({ builds }) {
	return pipe(
			map(compressBuild),
			join(BUILD_SEPARATOR),
	)(builds);
}

export function deserialize(data) {
	const restoreBuilds = pipe(split(BUILD_SEPARATOR), map(restoreBuild));
	return { builds: restoreBuilds(data) };
}


function compressBuild(build) {
	return PROPS.reduce((data, prop) => {
		const fragment = prop.compress(build);
		return data ? `${data}${PROP_SEPARATOR}${fragment}` : fragment;
	}, '');
}

function restoreBuild(data) {
	const fragments = data.split(PROP_SEPARATOR);
	return PROPS.reduce((build, prop) => {
		const index = Object.keys(build).length;
		return merge(build, prop.restore(fragments[index]));
	}, {});
}

function NumericProp({ name, defaults }) {
	return {
		compress: prop(name),
		restore: pipe(defaultTo(defaults), Number, objOf(name)),
	};
}

function BooleanProp({ name, defaults = false }) {
	return {
		compress: pipe(prop(name), Boolean, to(BOOLEAN_SHORTHANDS)),
		restore: pipe(from(BOOLEAN_SHORTHANDS), defaultTo(defaults), objOf(name)),
	};
}

function StringProp({ name, shorthands, defaults }) {
	return {
		compress: pipe(prop(name), to(shorthands)),
		restore: pipe(from(shorthands), defaultTo(defaults), objOf(name)),
	};
}

function ArrayProp({ name, shorthands }) {
	return {
		compress: pipe(prop(name), map(to(shorthands)), join('')),
		restore: pipe(defaultTo([]), map(from(shorthands)), objOf(name)),
	};
}

function to(shorthands) {
	return pipe(prop(__, invertObj(shorthands)), defaultTo(UNKNOWN_ELEMENT));
}
function from(shorthands) {
	return pipe(prop(__, shorthands), defaultTo(null));
}
