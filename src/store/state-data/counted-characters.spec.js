import { compress, restore } from './counted-characters';

test('compress returns empty string for unexpected values', () => {
	expect(compress()).toBe('');
	expect(compress(null)).toBe('');
	expect(compress({ a: 2 })).toBe('');
	expect(compress([1, 2, 3])).toBe('');
});

test('compress leaves character sequences shorter then 2 alone', () => {
	expect(compress('')).toBe('');
	expect(compress('a')).toBe('a');
	expect(compress('aba_ab')).toBe('aba_ab');
});

test('compress ignores numbers', () => {
	expect(compress('2a')).toBe('a');
	expect(compress('a2')).toBe('a');
	expect(compress('35a20b34')).toBe('ab');
	expect(compress(123)).toBe('');
});

test('compress shortens conseqative characters to numbered format ', () => {
	expect(compress('aa')).toBe('2a');
	expect(compress('aaa')).toBe('3a');
	expect(compress('aaa')).toBe('3a');
	expect(compress('aaabccdddaaaaaaaaaaaa')).toBe('3ab2c3d12a');
});

test('restore returns empty string for unexpected values', () => {
	expect(restore()).toBe('');
	expect(restore(null)).toBe('');
	expect(restore({ a: 2 })).toBe('');
	expect(restore([1, 2, 3])).toBe('');
});

test('restore returns strings without numbers as is', () => {
	expect(restore('')).toBe('');
	expect(restore('aa')).toBe('aa');
	expect(restore('aaa')).toBe('aaa');
	expect(restore('aada')).toBe('aada');
	expect(restore('aaabcc__daaaaaaaaaaaa')).toBe('aaabcc__daaaaaaaaaaaa');
});

test('restore ignores numbers at the end', () => {
	expect(restore('2')).toBe('');
	expect(restore('a2')).toBe('a');
	expect(restore('aa22')).toBe('aa');
});

test('repeats letters following number just as many times', () => {
	expect(restore('2a')).toBe('aa');
	expect(restore('a2b')).toBe('abb');
	expect(restore('4ab')).toBe('aaaab');
	expect(restore('c14ab')).toBe('caaaaaaaaaaaaaab');
	expect(restore('a0xab')).toBe('aab');
});
