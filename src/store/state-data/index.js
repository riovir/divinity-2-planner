import { concat, cond, defaultTo, drop, head, pipe, split, startsWith, T, take } from 'ramda';
import Build from '../build';
import { deserialize as deserializeV1 } from './v1-state-data';
import { deserialize as deserializeV2 } from './v2-state-data';
import { serialize as serializeV3, deserialize as deserializeV3 } from './v3-state-data';

const V1 = 'v1-';
const V2 = 'v2-';
const V3 = 'v3-';

export const serialize = concatToVersion(V3, serializeV3);

export const deserialize = pipe(cond([
	[startsWith(V1), dropVersionThen(V1, deserializeV1)],
	[startsWith(V2), dropVersionThen(V2, deserializeV2)],
	[startsWith(V3), dropVersionThen(V3, deserializeV3)],
	[T, throwUnsupportedFormatError],
]), sanitize);

function sanitize(state) {
	return { ...state, builds: state.builds.map(Build) };
}

function concatToVersion(version, fn) {
	return pipe(fn, concat(version));
}

function dropVersionThen(version, fn) {
	return pipe(drop(version.length), fn);
}

function throwUnsupportedFormatError(text) {
	const versionOf = pipe(defaultTo(''), String, split('-'), head, take(3));
	throw Object.assign(new Error('error.unsupportedStateVersion'), { parameters: [versionOf(text)] });
}
