import { hasAbility } from './has-ability';
import Character from '../character';
import Build from '../build';
import { CS_AEROTHEURGE } from '../constants';

test('returns false for blank character', () => {
	expect(hasAbility(CS_AEROTHEURGE)(Character())).toBe(false);
});

test('returns false when skill too low', () => {
	expect(hasAbility(CS_AEROTHEURGE, 2)(Character({ [CS_AEROTHEURGE]: 1 }))).toBe(false);
});

test('returns true when skill meets requirement', () => {
	expect(hasAbility(CS_AEROTHEURGE, 2)(Character({ [CS_AEROTHEURGE]: 2 }))).toBe(true);
	expect(hasAbility(CS_AEROTHEURGE, 2)(Character({ [CS_AEROTHEURGE]: 3 }))).toBe(true);
});

test('supports Build as well', () => {
	expect(hasAbility(CS_AEROTHEURGE, 2)(Build({ combatProgression: [CS_AEROTHEURGE] }))).toBe(false);
	expect(hasAbility(CS_AEROTHEURGE, 2)(Build({ combatProgression: [CS_AEROTHEURGE, CS_AEROTHEURGE] }))).toBe(true);
});
