import { converge, defaultTo, equals, filter, length, lte, or, pipe, propSatisfies } from 'ramda';

export function hasAbility(skillName, minimum = 1) {
	const containsTimes = min => pipe(
			defaultTo([]),
			filter(equals(skillName)),
			length,
			lte(min),
	);
	return converge(or, [
		propSatisfies(val => val >= minimum, skillName),
		propSatisfies(containsTimes(minimum), 'combatProgression'),
	]);
}
