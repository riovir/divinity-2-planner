import {
	always, cond, converge, complement, contains, defaultTo, equals, filter,
	find, findIndex, head, isNil, last, pipe, prop, propEq, sum, T, update,
} from 'ramda';
import {
	DS_DAMAGE_INTELLIGENCE, DS_DAMAGE_PHYSICAL, LEVEL_CAP, O_CUSTOM_ELF_FEMALE,
	isAttribute, isCombatAbility, isCivilAbility, statCapFor,
} from './constants';
import { selectableOriginsFor } from './origins';
import Build, {
	sanitizeBuilds, decreaseStat, increaseStat, addTalent, removeTalent, addSkill, removeSkill,
} from './build';
import { toCharacters } from './character';
import { selectableTalentsFor, unselectableTalentsFor, toTalentProp } from './talents';
import { serialize, deserialize } from './state-data';
import { toSkillProp } from './skills';

export { Store };

function Store() {
	const state = {
		builds: [Build({ id: 1 })],
		selectedId: 1,
		busy: false,
		skillbarLocked: false,
		exportActive: false,
		importActive: false,
	};

	const getters = {
		selectedBuild({ builds, selectedId }) {
			return builds.find(propEq('id', selectedId)) || builds[0];
		},
		characters({ builds }) { return toCharacters({ builds }); },
		selectedCharacter({ selectedId }, { characters }) {
			return characters.find(propEq('id', selectedId)) || characters[0];
		},
		stateData: serialize,
		attributePoints: (_, { selectedBuild }) => countNullsIn('attributeProgression', selectedBuild),
		combatPoints: (_, { selectedBuild }) => countNullsIn('combatProgression', selectedBuild),
		civilPoints: (_, { selectedBuild }) => countNullsIn('civilProgression', selectedBuild),
		talentPoints: (_, { selectedBuild }) => countNullsIn('talents', selectedBuild),
		availableTalents: (_, { selectedCharacter }) => selectableTalentsFor(selectedCharacter).map(prop('name')),
		unavailableTalents: (_, { selectedCharacter }) => unselectableTalentsFor(selectedCharacter).map(prop('name')),
		selectableOrigins({ builds }, { selectedBuild }) {
			return selectableOriginsFor({ builds, selectedBuild });
		},
		canDecrease(_, { selectedBuild }) {
			const progression = []
					.concat(selectedBuild.attributeProgression)
					.concat(selectedBuild.combatProgression)
					.concat(selectedBuild.civilProgression);
			return stat =>
				(stat === 'level' && selectedBuild.level > 1) ||
				progression.includes(stat);
		},
		canIncrease(_, { selectedCharacter, attributePoints, combatPoints, civilPoints }) {
			const capOf = statCapFor(selectedCharacter);
			const pointsOf = cond([
				[equals('level'), always(LEVEL_CAP - selectedCharacter.level)],
				[isAttribute, always(attributePoints)],
				[isCombatAbility, always(combatPoints)],
				[isCivilAbility, always(civilPoints)],
				[T, always(0)],
			]);
			return stat => pointsOf(stat) > 0 && selectedCharacter[stat] < capOf(stat);
		},
		canAddTalent(_, { talentPoints }) {
			return talent => talentPoints > 0 || toTalentProp('free', talent);
		},
		canRemoveTalent() {
			return talent => !toTalentProp('autoAdd', talent);
		},
		canAddCharacter: ({ builds }) => builds.length < 4,
		canRemoveCharacter: ({ builds }) => builds.length > 1,
		canDeactivate: ({ builds }) => filter(propEq('active', true), builds).length > 1,
		canToggleActive: (_, { selectedBuild, canDeactivate }) => !selectedBuild.active || canDeactivate,
		canHaveSkill: (_, { selectedCharacter }) => skill => toSkillProp('availableFor', skill)(selectedCharacter),
		getDamageBonus: (_, { selectedCharacter }) => ({ abilityBonus = DS_DAMAGE_INTELLIGENCE, elementalBonus = DS_DAMAGE_PHYSICAL }) => {
			const asMultiplier = bonus => (100 + selectedCharacter[bonus]) / 100;
			return Math.round(asMultiplier(abilityBonus) * asMultiplier(elementalBonus) * 100);
		},
		hasSkill: (_, { selectedBuild }) => skill => contains(skill, selectedBuild.skills),
		memorySlotsUsed: (_, { selectedBuild: { skills } }) => sum(skills.map(toSkillProp('memoryCost'))),
	};

	const mutations = {
		setBusy(state, value = true) { state.busy = value; },
		selectId(state, id) { state.selectedId = id; },
		setBuilds(state, { builds }) {
			const findSelected = find(propEq('id', state.selectedId));
			const selectedIn = pipe(converge(defaultTo, [head, findSelected]), prop('id'));
			Object.assign(state, { builds: sanitizeBuilds(builds), selectedId: selectedIn(builds) });
		},
		updateBuild(state, build) {
			const index = findIndex(propEq('id', build.id), state.builds);
			state.builds = update(index, Build(build), state.builds);
		},
		showExport(state, active = true) { state.exportActive = Boolean(active); },
		showImport(state, active = true) { state.importActive = Boolean(active); },
		setSkillbarLock(state, value = true) { state.skillbarLocked = Boolean(value); },
	};

	const actions = {
		setBusy({ commit }, value = true) { commit('setBusy', value); },
		selectId({ commit, state }, id) {
			const numericId = Number(id);
			if (find(propEq('id', numericId), state.builds)) {
				commit('selectId', numericId);
			}
		},
		setOrigin({ commit, getters }, origin) {
			commit('updateBuild', Build({ ...getters.selectedBuild, origin }));
		},
		setExaminedStat({ commit }, stat) { commit('setExaminedStat', stat); },
		decStat({ commit, getters }, stat) {
			const build = stat === 'level' ?
				Build({ ...getters.selectedBuild, level: getters.selectedBuild.level - 1 }) :
				decreaseStat(stat, getters.selectedBuild);
			commit('updateBuild', build);
		},
		incStat({ commit, getters }, stat) {
			const build = stat === 'level' ?
				Build({ ...getters.selectedBuild, level: getters.selectedBuild.level + 1 }) :
				increaseStat(stat, getters.selectedBuild);
			commit('updateBuild', build);
		},
		addTalent({ commit, getters }, talent) {
			commit('updateBuild', addTalent(talent, getters.selectedBuild));
		},
		removeTalent({ commit, getters }, talent) {
			commit('updateBuild', removeTalent(talent, getters.selectedBuild));
		},
		setEquipment({ commit, getters }, { slot, name }) {
			commit('updateBuild', { ...getters.selectedBuild, [slot]: name });
		},
		setStateData({ commit }, data) {
			if (!data) { return; }
			commit('setBuilds', deserialize(data));
		},
		addCharacter({ commit, state, getters }) {
			if (!getters.canAddCharacter) { return; }
			const build = Build({ origin: O_CUSTOM_ELF_FEMALE });
			commit('setBuilds', { builds: [...state.builds, build] });
			commit('selectId', last(state.builds).id);
		},
		removeCharacter({ commit, state, getters }) {
			if (!getters.canRemoveCharacter) { return; }
			const builds = filter(complement(propEq('id', state.selectedId)), state.builds);
			commit('setBuilds', { builds });
		},
		toggleActive({ commit, getters: { selectedBuild, canToggleActive } }) {
			if (!canToggleActive) { return; }
			commit('updateBuild', { ...selectedBuild, active: !selectedBuild.active });
		},
		addSkill({ commit, getters: { selectedBuild } }, skill) {
			commit('updateBuild', addSkill(skill, selectedBuild));
		},
		removeSkill({ commit, getters: { selectedBuild } }, skill) {
			commit('updateBuild', removeSkill(skill, selectedBuild));
		},
		resetBuilds({ commit }) {
			commit('setBuilds', { builds: [Build({ id: 1 })] });
		},
		showExport({ commit }, active) { commit('showExport', active); },
		showImport({ commit }, active) { commit('showImport', active); },
		setSkillbarLock({ commit }, value) { commit('setSkillbarLock', value); },
	};

	return { state, getters, mutations, actions };
}

function countNullsIn(arrayField, build) {
	return pipe(prop(arrayField), filter(isNil), prop('length'))(build);
}
