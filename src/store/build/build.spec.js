import Build from './build';

test('starts with 3 attribute points on level 1', () => {
	expect(Build({ level: 1 }).attributeProgression).toHaveLength(3);
});

test('gets 2 more attribute point per level after level 1', () => {
	expect(Build({ level: 2 }).attributeProgression).toHaveLength(5);
	expect(Build({ level: 4 }).attributeProgression).toHaveLength(9);
	expect(Build({ level: 20 }).attributeProgression).toHaveLength(41);
});
