import { assoc, has, when } from 'ramda';
import { C_COMPANIONS } from '../constants';
import Build from '../build';
import { sanitizeBuilds } from './sanitize-builds';

test('ensures characters have all expected props', () => {
	expect(sanitizeAsSolo([{ garbage: true }])).toEqual([Build()]);
});

test('ensures characters have ids', () => {
	expect(sanitizeAsSolo([{}, {}])).toEqual([
		Build({ id: 1 }),
		Build({ id: 2 }),
	]);
});

test('ensures characters have unique ids', () => {
	expect(sanitizeAsSolo([{ id: 3 }, { id: 3 }, { id: 3 }, { id: 3 }])).toEqual([
		Build({ id: 3 }),
		Build({ id: 1 }),
		Build({ id: 2 }),
		Build({ id: 4 }),
	]);
});

test('ensures the one of the characters is active', () => {
	expect(sanitizeAsSolo([{ active: false }, { id: 2, active: false }])).toEqual([
		Build({ id: 1, active: true }),
		Build({ id: 2, active: false }),
	]);
	expect(sanitizeAsSolo([{ active: false }, { id: 2, active: true }])).toEqual([
		Build({ id: 1, active: false }),
		Build({ id: 2, active: true }),
	]);
});

function sanitizeAsSolo(builds) {
	const removeAnyCompanions = when(has(C_COMPANIONS), assoc(C_COMPANIONS, []));
	return sanitizeBuilds(builds).map(removeAnyCompanions);
}
