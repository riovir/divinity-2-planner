import { always, append, cond, indexOf, lastIndexOf, T, uniq, update, without } from 'ramda';
import { isAttribute, isCombatAbility, isCivilAbility } from '../constants';
import { toTalentProp } from '../talents';

export function decreaseStat(stat, build) {
	const field = progressionFieldOf(stat);
	const progression = build[field];
	const index = lastIndexOf(stat, build[field]);
	if (index < 0) { throw new Error(`Unable to to decrease ${stat} further`); }
	return { ...build, [field]: update(index, null, progression) };
}

export function increaseStat(stat, build) {
	const field = progressionFieldOf(stat);
	const progression = build[field];
	const index = indexOf(null, build[field]);
	const increase = index < 0 ? append :	update(index);
	return { ...build, [field]: increase(stat, progression) };
}

export function addTalent(talent, build) {
	if (toTalentProp('free', talent)) {
		return { ...build, freeTalents: uniq(append(talent, build.freeTalents)) };
	}
	const talents = build.talents;
	const index = indexOf(null, talents);
	return index < 0 ? build : { ...build, talents: update(index, talent, talents) };
}

export function removeTalent(talent, build) {
	if (toTalentProp('free', talent)) {
		return { ...build, freeTalents: without([talent], build.freeTalents) };
	}
	const talents = build.talents;
	const index = lastIndexOf(talent, talents);
	return index < 0 ? build : { ...build, talents: update(index, null, talents) };
}

export function addSkill(skill, build) {
	return { ...build, skills: uniq(append(skill, build.skills)) };
}

export function removeSkill(skill, build) {
	return { ...build, skills: without(skill, build.skills) };
}

function progressionFieldOf(stat) {
	return cond([
		[isAttribute, always('attributeProgression')],
		[isCombatAbility, always('combatProgression')],
		[isCivilAbility, always('civilProgression')],
		[T, () => { throw new Error(`Unknown stat: ${stat}`); }],
	])(stat);
}
