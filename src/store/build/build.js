import { equals, splitAt, uniq } from 'ramda';
import { toTalentProp } from '../talents';
import { sanitizeHands } from '../equipment';
import { CS_POLYMORPH, T_ALL_SKILLED_UP, T_BIGGER_AND_BETTER, O_SEBILLE } from '../constants';

export default function Build({
	id, level = 1, origin, active,
	attributeProgression = [],
	combatProgression = [],
	civilProgression = [],
	talents = [],
	freeTalents = [],
	skills = [],
	mainHand,
	offHand,
	companions = [],
} = {}) {
	const combat = withExactSize(combatPointsOn({ level, talents }), combatProgression);
	return withoutInvalidTalents({
		...Base({ id, level, origin, active, mainHand, offHand, skills }),
		combatProgression: combat,
		civilProgression: withExactSize(civilPointsOn({ level, talents }), civilProgression),
		attributeProgression: withExactSize(attributePointsOn({ level, talents, combatProgression: combat }), attributeProgression),
		talents: withExactSize(talentPointsOn({ level }), uniq(talents)),
		freeTalents: uniq(freeTalents).filter(Boolean),
		companions,
	});
}

export function Base({ id = 1, level = 1, origin = O_SEBILLE, active = true, mainHand, offHand, skills = [] } = {}) {
	return { id, level, origin, active, ...sanitizeHands({ mainHand, offHand }), skills: uniq(skills).filter(Boolean) };
}

function attributePointsOn({ level, talents, combatProgression }) {
	const skillBonus = combatProgression.filter(equals(CS_POLYMORPH)).length;
	const talentBonus = talents.includes(T_BIGGER_AND_BETTER) ? 2 : 0;
	return level * 2 + 1 + skillBonus + talentBonus;
}

function combatPointsOn({ level, talents }) {
	const bonus = talents.includes(T_ALL_SKILLED_UP) ? 1 : 0;
	return level + 1 + bonus;
}

function civilPointsOn({ level, talents }) {
	const bonus = talents.includes(T_ALL_SKILLED_UP) ? 1 : 0;
	return Math.floor((level + 2) / 4) + 1 + bonus;
}

function talentPointsOn({ level }) {
	return Math.floor((level + 2) / 5) + 1;
}

function withExactSize(size, array) {
	const nulls = new Array(size).fill(null);
	return splitAt(size, array.concat(nulls))[0];
}

function withoutInvalidTalents(build) {
	const shouldKeep = talent => talent === null || toTalentProp('availableFor', talent)(build);
	return {
		...build,
		talents: build.talents.map(name => shouldKeep(name) ? name : null),
		freeTalents: build.freeTalents.filter(shouldKeep),
	};
}
