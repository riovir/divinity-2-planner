import Build from './build';
import { decreaseStat, increaseStat, addTalent, removeTalent, addSkill, removeSkill } from './change-stats';
import { T_LONE_WOLF, T_MNEMONIC, T_SPIDERS_KISS_TO, T_TRAINING_WITS, SK_ELECTRIC_DISCHARGE } from '../constants';

describe('decreaseStat', () => {
	testDecrease({ level: 2, type: 'attribute', stat: 'strength', otherStat: 'wits' });
	testDecrease({ level: 4, type: 'combat', stat: 'geomancer', otherStat: 'leadership' });
	testDecrease({ level: 14, type: 'civil', stat: 'sneaking', otherStat: 'loremaster' });

	test('throws error when stat is not recognized', () => {
		expect(() => decreaseStat('unknown', Build())).toThrowError('Unknown stat: unknown');
	});

	test('throws error when stat can no longer be decreased', () => {
		expect(() => decreaseStat('strength', Build())).toThrowError('Unable to to decrease strength further');
	});
});

function testDecrease({ level = 1, type, progressionField = `${type}Progression`, stat, otherStat }) {
	test(`sets existing ${type} entry to null`, () => {
		const build = Build({ level, [progressionField]: [stat, otherStat] });
		expect(decreaseStat(stat, build)[progressionField]).not.toContain(stat);
	});

	test(`sets last ${type} entry to null`, () => {
		const build = Build({ level, [progressionField]: [stat, otherStat, stat, otherStat, otherStat] });
		expect(decreaseStat(stat, build)[progressionField]).toEqual([stat, otherStat, null, otherStat, otherStat]);
	});
}

describe('increaseStat', () => {
	testIncrease({ type: 'attribute', stat: 'strength', otherStat: 'wits' });
	testIncrease({ type: 'combat', stat: 'geomancer', otherStat: 'leadership' });
	testIncrease({ level: 2, type: 'civil', stat: 'sneaking', otherStat: 'loremaster' });

	test('throws error when stat is not recognized', () => {
		expect(() => increaseStat('unknown', Build())).toThrowError('Unknown stat: unknown');
	});
});

function testIncrease({ level = 1, type, progressionField = `${type}Progression`, stat, otherStat }) {
	test(`sets null ${type} entry`, () => {
		const build = Build({ level });
		expect(increaseStat(stat, build)[progressionField]).toContain(stat);
	});

	test(`sets first null ${type} entry`, () => {
		const build = Build({ level, [progressionField]: [stat, otherStat, null] });
		expect(increaseStat(stat, build)[progressionField]).toEqual([stat, otherStat, stat]);
	});
}

describe('addTalent', () => {
	test('recognizes and adds free talents', () => {
		expect(addTalent(T_TRAINING_WITS, Build())).toEqual(Build({ freeTalents: [T_TRAINING_WITS] }));
	});

	test('recognizes and adds regular talents', () => {
		expect(addTalent(T_LONE_WOLF, Build())).toEqual(Build({ talents: [T_LONE_WOLF] }));
	});

	test('ignores talent already in build', () => {
		const build = Build({ freeTalents: [T_TRAINING_WITS], talents: [T_LONE_WOLF] });
		expect(addTalent(T_TRAINING_WITS, build)).toEqual(build);
		expect(addTalent(T_LONE_WOLF, build)).toEqual(build);
	});

	test('ignores general talent when no more room', () => {
		const build = Build({ talents: [T_MNEMONIC] });
		expect(addTalent(T_LONE_WOLF, build)).toEqual(build);
	});
});

describe('removeTalent', () => {
	test('recognizes and removes free talents', () => {
		expect(removeTalent(T_TRAINING_WITS, Build({ freeTalents: [T_TRAINING_WITS] }))).toEqual(Build());
	});

	test('recognizes and removes regular talents', () => {
		expect(removeTalent(T_LONE_WOLF, Build({ talents: [T_LONE_WOLF] }))).toEqual(Build());
	});

	test('ignores talent not in build', () => {
		const build = Build({ freeTalents: [T_TRAINING_WITS], talents: [T_LONE_WOLF] });
		expect(removeTalent(T_SPIDERS_KISS_TO, build)).toEqual(build);
		expect(removeTalent(T_MNEMONIC, build)).toEqual(build);
	});
});

test('addSkill adds skill', () => {
	expect(addSkill(SK_ELECTRIC_DISCHARGE, Build())).toEqual(Build({ skills: [SK_ELECTRIC_DISCHARGE] }));
});

test('addSkill ignores skill already in build', () => {
	const build = Build({ skills: [SK_ELECTRIC_DISCHARGE] });
	expect(addSkill(SK_ELECTRIC_DISCHARGE, build)).toEqual(build);
});

test('removeSkill removes skill', () => {
	expect(removeSkill(SK_ELECTRIC_DISCHARGE, Build({ skills: [SK_ELECTRIC_DISCHARGE] }))).toEqual(Build());
});

test('removeSkill ignores skill not in build', () => {
	expect(removeSkill(SK_ELECTRIC_DISCHARGE, Build())).toEqual(Build());
});
