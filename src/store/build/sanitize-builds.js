import { any, complement, contains, differenceWith, find, flip, map, range, pipe, prop, propEq } from 'ramda';
import { C_COMPANIONS } from '../constants';
import Build from '../build';

export function sanitizeBuilds(builds) {
	const sanitizedBuilds = pipe(
			map(Build),
			ensureUniqueIds,
			ensureOneActive,
	)(builds);
	return sanitizedBuilds
			.map(addOthersAs(C_COMPANIONS, builds))
			.map(Build);
}

function ensureUniqueIds(builds) {
	return builds.reduce((builds, build) => [...builds, { ...build, id: toUniqueId(builds, build) }], []);
}

function toUniqueId(builds, { id }) {
	const containedBy = pipe(map(prop('id')), flip(contains));
	const isAvailable = complement(containedBy(builds));
	const fallbackIds = range(1, builds.length + 2);
	return isAvailable(id) ? id : find(isAvailable, fallbackIds);
}

function ensureOneActive(builds) {
	if (!builds.length) { return []; }
	if (any(propEq('active', true), builds)) { return builds; }
	const [first, ...rest] = builds;
	return [{ ...first, active: true }, ...rest];
}

function addOthersAs(field, builds) {
	const except = differenceWith(compareProps('id'), builds);
	return build => ({ ...build, [field]: except([build]) });
}

function compareProps(prop) {
	return (left, right) => left[prop] === right[prop];
}
