import { propEq } from 'ramda';
import { FaneRogueBuild } from 'test/utils';
import Character from './character';
import { toCharacter } from './to-characters';
import {
	ATTRIBUTES, COMBAT_ABILITIES, CIVIL_ABILITIES, T_LONE_WOLF, CS_AEROTHEURGE,
	SK_BLINDING_RADIANCE, SK_ELECTRIC_DISCHARGE, SK_FAVOURABLE_WIND, SK_SHOCKING_TOUCH,
} from '../constants';

test('Passing created character back to factory equals with itself', () => {
	const firstCharacter = Character(toCharacter(FaneRogueBuild()));
	expect(Character(firstCharacter)).toEqual(firstCharacter);
});

test('Ignores received bonuses', () => {
	[...ATTRIBUTES, ...COMBAT_ABILITIES, ...CIVIL_ABILITIES].forEach(field => {
		const { bonuses } = Character({ bonuses: {
			[field]: [{ name: 'no-longer-interesting', garbage: true }],
		} });
		expect(bonuses[field].find(propEq('name', 'no-longer-interesting'))).toBeUndefined();
	});
});

test('Applies talent bonuses', () => {
	const { bonuses } = Character({
		strength: 11,
		scoundrel: 1,
		talents: [T_LONE_WOLF],
	});
	expect(bonuses.strength).toHaveLength(1);
	expect(bonuses.strength[0].name).toBe(T_LONE_WOLF);
	expect(bonuses.scoundrel).toHaveLength(1);
	expect(bonuses.scoundrel[0].name).toBe(T_LONE_WOLF);
});

test('Unlearns skills over the memory slot cap', () => {
	const expected = [SK_BLINDING_RADIANCE, SK_ELECTRIC_DISCHARGE, SK_FAVOURABLE_WIND];
	expect(Character({ [CS_AEROTHEURGE]: 1, skills: [...expected, SK_SHOCKING_TOUCH] }).skills)
			.not.toEqual(expect.arrayContaining([SK_SHOCKING_TOUCH]));
});

test('Assigns skills over the memory slot cap to skillsOverLimit', () => {
	const expected = [SK_BLINDING_RADIANCE, SK_ELECTRIC_DISCHARGE, SK_FAVOURABLE_WIND];
	expect(Character({ [CS_AEROTHEURGE]: 1, skills: [...expected, SK_SHOCKING_TOUCH] }).skillsOverLimit).toEqual([SK_SHOCKING_TOUCH]);
});
