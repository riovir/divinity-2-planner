import { merge, objOf } from 'ramda';
import Build from '../build';
import { toCharacter, toCharacters } from './to-characters';
import {
	ATTRIBUTES, COMBAT_ABILITIES, CIVIL_ABILITIES,
	O_FANE, O_BEAST, C_COMPANIONS_IN_PARTY, A_FINESSE, CS_LEADERSHIP, T_LONE_WOLF, RS_AIR,
} from '../constants';

test('toCharacters adds other active characters to active characters', () => {
	const [first, second] = toCharacters({ builds: [{ id: 1 }, { id: 2 }]	});
	expect(first[C_COMPANIONS_IN_PARTY].length).toBe(1);
	expect(first[C_COMPANIONS_IN_PARTY][0]).toEqual(expect.objectContaining({ id: second.id }));
	expect(second[C_COMPANIONS_IN_PARTY].length).toBe(1);
	expect(second[C_COMPANIONS_IN_PARTY][0]).toEqual(expect.objectContaining({ id: first.id }));
});

test('toCharacters ignores inactive characters', () => {
	const [first, inactive, third] = toCharacters({ builds: [{ id: 1 }, { id: 2, active: false }, { id: 3 }] });
	expect(first[C_COMPANIONS_IN_PARTY].length).toBe(1);
	expect(first[C_COMPANIONS_IN_PARTY][0]).toEqual(expect.objectContaining({ id: third.id }));
	expect(third[C_COMPANIONS_IN_PARTY].length).toBe(1);
	expect(third[C_COMPANIONS_IN_PARTY][0]).toEqual(expect.objectContaining({ id: first.id }));
	expect(inactive[C_COMPANIONS_IN_PARTY]).toEqual([]);
});

test('toCharacters applies bonuses from active party members characters', () => {
	const [first, second] = toCharacters({ builds: [
		{ id: 1, origin: O_BEAST, combatProgression: [CS_LEADERSHIP, CS_LEADERSHIP, A_FINESSE], talents: [T_LONE_WOLF] },
		{ id: 2, origin: O_FANE },
	]	});
	expect(first[CS_LEADERSHIP]).toBe(4);
	expect(second[RS_AIR]).toBe(4 * 3);
});

describe('calculating attribute', () => {
	ATTRIBUTES.map(objOf('stat'))
			.map(merge({ statDefault: 10, progressionField: 'attributeProgression' }))
			.forEach(testStat);
});

describe('calculating combat abilities', () => {
	COMBAT_ABILITIES.map(objOf('stat'))
			.map(merge({ progressionField: 'combatProgression' }))
			.forEach(testStat);
});

describe('calculating combat abilities', () => {
	CIVIL_ABILITIES.map(objOf('stat'))
			.map(merge({ progressionField: 'civilProgression' }))
			.forEach(testStat);
});

function testStat({ stat, statDefault = 0, progressionField }) {
	test(`initializes ${stat} as ${statDefault}`, () => {
		expect(toCharacter(Build()).base[stat]).toBe(statDefault);
	});

	test(`computes ${stat} according to progression`, () => {
		const build = Build({ level: 5, [progressionField]: [stat, stat] });
		expect(toCharacter(build).base[stat]).toBe(statDefault + 2);
	});
}
