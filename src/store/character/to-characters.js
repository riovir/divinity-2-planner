import { curry, differenceWith, dissoc, filter, map, pipe, propEq } from 'ramda';
import Build from '../build';
import Character, { BaseCharacter } from './character';
import { C_COMPANIONS_IN_PARTY, C_COMPANIONS } from '../constants';

export function toCharacters({ builds }) {
	const party = builds.map(toCharacter);
	return party
			.map(addOthersAs(C_COMPANIONS, party))
			.map(addActiveOthersAs(C_COMPANIONS_IN_PARTY, party))
			.map(Character);
}

export function toCharacter(build) {
	const sanitizedBuild = Build(build);
	const advance = curry(advanceBuild)(sanitizedBuild);
	return pipe(
			BaseCharacter,
			advance('attributeProgression'),
			advance('combatProgression'),
			advance('civilProgression'),
			Character,
	)(sanitizedBuild);
}

function advanceBuild(build, progressionField, character) {
	return build[progressionField].reduce(improveField, character);
}

function improveField(character, field) {
	const isValid = stat => stat >= 0;
	if (!isValid(character[field])) { return character; }
	return { ...character, [field]: character[field] + 1 };
}

function addActiveOthersAs(field, party) {
	const except = pipe(
			filteredCompanionsIn(propEq('active', true), party),
			map(dissoc([field])),
	);
	return character => {
		const others = character.active ? except(character) : [];
		return { ...character, [field]: others };
	};
}

function addOthersAs(field, party) {
	const except = differenceWith(compareProps('id'), party);
	return character => ({ ...character, [field]: except([character]) });
}

function filteredCompanionsIn(predicate, party) {
	const activeMembers = filter(predicate, party);
	return character => differenceWith(compareProps('id'), activeMembers, [character]);
}

function compareProps(prop) {
	return (left, right) => left[prop] === right[prop];
}
