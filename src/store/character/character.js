import {
	__, add, always, append, concat, defaultTo, difference, filter, flatten,
	isNil, map, mapObjIndexed, mergeWith, min, pipe, prop, sortBy, uniq,
} from 'ramda';
import {
	ATTRIBUTES, COMBAT_ABILITIES, CIVIL_ABILITIES, DERIVED_STATS,
	ATTRIBUTE_CAP, COMBAT_ABILITY_CAP, CIVIL_ABILITY_CAP,
	C_COMPANIONS, C_COMPANIONS_IN_PARTY, C_SKILLS_OVER_LIMIT,
	DS_ACCURACY, DS_AP_MAX, DS_AP_REGEN, DS_AP_START, DS_CRIT_MULTIPLIER, DS_MOVEMENT, DS_EFFECT_DURATION_BONUSES,
	DS_CARRY_WEIGHT, DS_OBJECT_WEIGHT, DS_MEMORY_SLOT, DS_INITIATIVE, DS_VITALITY, DS_VITALITY_MULTIPLIER,
	statCapFor,
} from '../constants';
import Bonuses, { Bonus } from '../bonuses';
import { rankedTalentsOf, racialTalentsOf, toTalentProp } from '../talents';
import { equipmentOf } from '../equipment';
import { Base } from '../build';
import { ALL_ATTRIBUTES } from '../attributes';
import { ALL_ABILITIES } from '../abilities';
import { freeSkillsOf, toSkillDetails } from '../skills';

const VITALITY_PROGRESSION = [30, 40, 60, 75, 95, 120, 140, 170, 205, 245, 295, 350, 420, 505, 610, 740, 900, 1170, 1430, 1755, 2160];
const LEVEL_BONUSES = { bonusesFor: ({ level }) => ({ [DS_VITALITY]: Bonus({
	name: 'level',
	resourceKey: 'attribute.level',
	value: VITALITY_PROGRESSION[level - 1],
}) }) };

export default function Character(overrides = {}) {
	return pipe(
			withoutInvalidTalents,
			assocBase,
			addRacialTalents,
			assocBonuses,
			calculateVitality,
			applyCaps,
			addFreeSkills,
			assocSkillsOverLimit,
	)(BaseCharacter(overrides));
}

export function BaseCharacter(overrides = {}) {
	const freeTalents = overrides.freeTalents || [];
	return withoutRacialTalents({
		...Base(overrides),
		talents: [...freeTalents, ...withoutNulls(overrides.talents)],
		[C_COMPANIONS]: overrides[C_COMPANIONS] || [],
		[C_COMPANIONS_IN_PARTY]: overrides[C_COMPANIONS_IN_PARTY] || [],
		[C_SKILLS_OVER_LIMIT]: overrides[C_SKILLS_OVER_LIMIT] || [],
		...createObject({ fields: ATTRIBUTES, overrides, ensure: between(10, ATTRIBUTE_CAP) }),
		...createObject({ fields: COMBAT_ABILITIES, overrides, ensure: between(0, COMBAT_ABILITY_CAP) }),
		...createObject({ fields: CIVIL_ABILITIES, overrides, ensure: between(0, CIVIL_ABILITY_CAP) }),
		...createObject({ fields: DERIVED_STATS, overrides, ensure: always(0) }),
		...createObject({ fields: [DS_MEMORY_SLOT], overrides, ensure: always(memorySlotsOf(overrides)) }),
		[DS_EFFECT_DURATION_BONUSES]: {},
		[DS_ACCURACY]: 95,
		[DS_MOVEMENT]: 5,
		[DS_CRIT_MULTIPLIER]: 150,
		[DS_INITIATIVE]: 10,
		[DS_AP_MAX]: 6,
		[DS_AP_REGEN]: 4,
		[DS_AP_START]: 4,
		[DS_CARRY_WEIGHT]: 100,
		[DS_OBJECT_WEIGHT]: 75,
	});
}

function memorySlotsOf({ level = 1 }) {
	return Math.floor(level / 2) + 3;
}

function withoutInvalidTalents(character) {
	const shouldKeep = talent => toTalentProp('availableFor', talent)(character);
	return { ...character, talents: character.talents.filter(shouldKeep) };
}

function assocBase(character) {
	return { ...character, base: character };
}

function addRacialTalents(character) {
	const racialTalents = racialTalentsOf(character).map(prop('name'));
	return { ...character, talents: uniq([...racialTalents, ...character.talents]) };
}

function withoutRacialTalents(character) {
	const racialTalents = racialTalentsOf(character).map(prop('name'));
	return { ...character, talents: difference(character.talents, racialTalents) };
}

function assocBonuses(basicCharacter) {
	const bonuses = [
		LEVEL_BONUSES,
		...rankedTalentsOf(basicCharacter),
		...ALL_ATTRIBUTES,
		...ALL_ABILITIES,
		...equipmentOf(basicCharacter),
	].map(prop('bonusesFor'));

	return bonuses.reduce((character, bonusesFor) => {
		const grantedBonuses = ensureArrayValues(bonusesFor(character));
		const bonuses = mergeWith(concat, character.bonuses, grantedBonuses);
		return { ...applyBonuses(bonuses, basicCharacter), bonuses };
	}, { ...basicCharacter, bonuses: Bonuses() });
}

function applyBonuses(allBonuses, character) {
	const mergeBonuses = (initial, statBonuses) => {
		const collect = typeof initial === 'number' ? add : mergeWith(add);
		return statBonuses.map(prop('value')).reduce(collect, initial);
	};
	return mergeWith(mergeBonuses, character, allBonuses);
}

function calculateVitality(character) {
	const bonusMultiplier = (100 + character[DS_VITALITY_MULTIPLIER]) / 100;
	const vitality = Math.floor(character[DS_VITALITY] * bonusMultiplier);
	return { ...character, vitality };
}

function applyCaps(character) {
	const capOf = statCapFor(character);
	const cappedStats = mapObjIndexed((value, stat) => {
		if (typeof value !== 'number') { return value; }
		const cap = capOf(stat);
		return isNil(cap) ? value : Math.min(value, cap);
	});
	return { ...cappedStats(character) };
}

function addFreeSkills(character) {
	const freeSkills = freeSkillsOf(character).map(prop('name'));
	return { ...character, skills: uniq([...freeSkills, ...character.skills]) };
}

function assocSkillsOverLimit(character) {
	const availableFirst = ({ availableFor }) => availableFor(character) ? 1 : 2;
	const skillDetails = [...character.skills, ...character[C_SKILLS_OVER_LIMIT]].map(toSkillDetails);

	const bySlotLimit = (({ slotsLeft, skills, leftover }, { name, memoryCost }) => ({
		slotsLeft: slotsLeft >= memoryCost ? slotsLeft - memoryCost : slotsLeft,
		skills: slotsLeft >= memoryCost ? [...skills, name] : skills,
		leftover: slotsLeft < memoryCost ? [...leftover, name] : leftover,
	}));

	const { skills, leftover } = sortBy(availableFirst, skillDetails)
			.reduce(bySlotLimit, { slotsLeft: character[DS_MEMORY_SLOT], skills: [], leftover: [] });
	return { ...character, skills, skillsOverLimit: leftover };
}

function withoutNulls(array) {
	return filter(v => !!v, array || []);
}

function ensureArrayValues(obj) {
	return map(pipe(append(__, []), flatten), obj || {});
}

function between(minValue, maxValue) {
	return pipe(defaultTo(minValue), min(maxValue));
}

function createObject({ fields = [], overrides = {}, ensure }) {
	const valueOf = field => defaultTo(overrides, overrides.base)[field];
	return fields.reduce((obj, field) => ({
		...obj, [field]: ensure(valueOf(field)),
	}), {});
}
