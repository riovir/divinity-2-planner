import { always, contains, complement, curry, filter, flip, map, pipe, prop, propEq, unless } from 'ramda';
import { camelConcat } from 'src/utils';
import {
	ET_MELEE, ET_MISSILE, ET_RANGED, ET_SHIELD, ES_MAIN_HAND, ES_OFF_HAND,
	EH_BOW, EH_CROSSBOW,
	EH_AXE, EH_MACE, EH_DAGGER, EH_SWORD, EH_WAND, EH_SHIELD,
	EH_2H_AXE, EH_2H_MACE, EH_2H_SWORD, EH_SPEAR, EH_STAFF,
	A_STRENGTH, A_FINESSE, A_INTELLIGENCE, DS_MOVEMENT, A_CONSTITUTION,
} from './constants';
import { Bonus } from './bonuses';

export const ALL_EQUIPMENT = [
	Equipment({ attribute: A_STRENGTH, name: EH_AXE }),
	Equipment({ attribute: A_STRENGTH, name: EH_MACE }),
	Equipment({ attribute: A_STRENGTH, name: EH_SWORD }),
	Equipment({ attribute: A_STRENGTH, name: EH_2H_AXE, twoHanded: true }),
	Equipment({ attribute: A_STRENGTH, name: EH_2H_MACE, twoHanded: true }),
	Equipment({ attribute: A_STRENGTH, name: EH_2H_SWORD, twoHanded: true }),
	Equipment({ attribute: A_FINESSE, name: EH_BOW, type: ET_RANGED, twoHanded: true }),
	Equipment({ attribute: A_FINESSE, name: EH_CROSSBOW, type: ET_RANGED, twoHanded: true, bonusesFor: crossbowMovementPenalty }),
	Equipment({ attribute: A_FINESSE, name: EH_DAGGER }),
	Equipment({ attribute: A_FINESSE, name: EH_SPEAR, twoHanded: true }),
	Equipment({ attribute: A_INTELLIGENCE, name: EH_WAND, type: ET_MISSILE }),
	Equipment({ attribute: A_INTELLIGENCE, name: EH_STAFF, twoHanded: true }),
	Equipment({ attribute: A_CONSTITUTION, name: EH_SHIELD, type: ET_SHIELD }),
];

const SLOTS = {
	[ES_MAIN_HAND]: filter(complement(propEq('type', ET_SHIELD)), ALL_EQUIPMENT),
	[ES_OFF_HAND]: filter(propEq('twoHanded', false), ALL_EQUIPMENT),
};

export function sanitizeHands({ mainHand, offHand }) {
	const namesOf = map(prop('name'));
	const main = eitherIn(namesOf(SLOTS[ES_MAIN_HAND]), EH_SWORD)(mainHand);
	const off = eitherIn(namesOf(SLOTS[ES_OFF_HAND]), null)(offHand);
	return {
		mainHand: main,
		offHand: toEquipmentProp('twoHanded', main) ? null : off,
	};
}

export const toEquipmentProp = curry((propName, talentName) => {
	return pipe(toEquipmentDetails, prop(propName))(talentName);
});

export function equipmentOfSlot(slotName) {
	const selectable = SLOTS[slotName];
	if (!selectable) { throw new Error(`Unknown slot: ${slotName}`); }
	return selectable;
}

export function equipmentOf({ mainHand, offHand }) {
	return pipe(
			map(toEquipmentDetails),
			filter(Boolean),
	)([mainHand, offHand]);
}

export function toEquipmentDetails(equipmentName) {
	if (!equipmentName) { return; }
	const details = ALL_EQUIPMENT.find(propEq('name', equipmentName));
	if (!details) { throw new Error(`Unknown equipment: ${equipmentName}`); }
	return details;
}

function crossbowMovementPenalty() {
	return { [DS_MOVEMENT]: Bonus({ name: EH_CROSSBOW, value: -1, resourceKey: `equipment.${EH_CROSSBOW}` }) };
}

function eitherIn(options, fallback) {
	return unless(flip(contains)(options), always(fallback));
}

function Equipment({
	name = '',
	resourceKey = `equipment.${name}`,
	bonusesFor = always(),
	attribute,
	type = ET_MELEE,
	twoHanded = false,
	iconName = camelConcat('equipment', name),
}) {
	return { name, resourceKey, bonusesFor, attribute, type, twoHanded, iconName };
}
