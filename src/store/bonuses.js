import { defaultTo, map, pipe } from 'ramda';
import { ATTRIBUTES, COMBAT_ABILITIES, CIVIL_ABILITIES } from './constants';

export default function Bonuses({	bonuses } = {}) {
	const ensure = pipe(defaultTo([]), map(Bonus));
	return {
		...createObject({ fields: ATTRIBUTES, overrides: bonuses, ensure }),
		...createObject({ fields: COMBAT_ABILITIES, overrides: bonuses, ensure }),
		...createObject({ fields: CIVIL_ABILITIES, overrides: bonuses, ensure }),
	};
}

export function Bonus({ name = '', resourceKey = `talent.${name}`, value = 1, capRaise = value } = {}) {
	return { name, resourceKey, value, capRaise };
}

function createObject({ fields = [], overrides = {}, ensure }) {
	return fields.reduce((obj, field) => ({
		...obj, [field]: ensure(overrides[field]),
	}), {});
}
