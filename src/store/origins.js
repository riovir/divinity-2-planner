import { contains, complement, curry, difference, filter, flip, map, pipe, prop, propEq } from 'ramda';
import {
	O_BEAST, O_FANE, O_IFAN, O_LOHSE, O_RED_PRINCE, O_SEBILLE, O_CUSTOM_ELF_FEMALE,
	R_DWARF, R_ELF, R_HUMAN, R_LIZARD, G_FEMALE, G_MALE,
} from './constants';

export const toOriginProp = curry((propName, originName) => {
	return pipe(toOriginDetails, prop(propName))(originName);
});

export function toOriginDetails(originName) {
	const details = ORIGINS.find(propEq('name', originName));
	if (!details) { throw new Error(`Unknown origin: ${originName}`); }
	return details;
}

export const ORIGINS = [
	Origin({ name: O_BEAST, unique: true, race: R_DWARF, gender: G_MALE }),
	Origin({ name: O_FANE, unique: true, race: R_HUMAN, gender: G_MALE, undead: true, raceResourceKey: 'race.eternal' }),
	Origin({ name: O_IFAN, unique: true, race: R_HUMAN, gender: G_MALE }),
	Origin({ name: O_LOHSE, unique: true, race: R_HUMAN, gender: G_FEMALE }),
	Origin({ name: O_RED_PRINCE, unique: true, race: R_LIZARD, gender: G_MALE }),
	Origin({ name: O_SEBILLE, unique: true, race: R_ELF, gender: G_FEMALE }),
	Origin({ name: O_CUSTOM_ELF_FEMALE, resourceKey: 'origin.custom', race: R_ELF }),
	Origin({ name: 'customElfMale', resourceKey: 'origin.custom', race: R_ELF, gender: G_MALE }),
	Origin({ name: 'customDwarfFemale', resourceKey: 'origin.custom', race: R_DWARF, gender: G_FEMALE }),
	Origin({ name: 'customDwarfMale', resourceKey: 'origin.custom', race: R_DWARF, gender: G_MALE }),
	Origin({ name: 'customHumanFemale', resourceKey: 'origin.custom', race: R_HUMAN, gender: G_FEMALE }),
	Origin({ name: 'customHumanMale', resourceKey: 'origin.custom', race: R_HUMAN, gender: G_MALE }),
	Origin({ name: 'customLizardFemale', resourceKey: 'origin.custom', race: R_LIZARD, gender: G_FEMALE }),
	Origin({ name: 'customLizardMale', resourceKey: 'origin.custom', race: R_LIZARD, gender: G_MALE }),
	Origin({
		name: 'customUndeadElfFemale',
		resourceKey: 'origin.custom',
		race: R_ELF,
		gender: G_FEMALE,
		undead: true,
		raceResourceKey: 'race.undeadElf',
	}),
	Origin({
		name: 'customUndeadElfMale',
		resourceKey: 'origin.custom',
		race: R_ELF,
		gender: G_MALE,
		undead: true,
		raceResourceKey: 'race.undeadElf',
	}),
	Origin({
		name: 'customUndeadDwarfFemale',
		resourceKey: 'origin.custom',
		race: R_DWARF,
		gender: G_FEMALE,
		undead: true,
		raceResourceKey: 'race.undeadDwarf',
	}),
	Origin({
		name: 'customUndeadDwarfMale',
		resourceKey: 'origin.custom',
		race: R_DWARF,
		gender: G_MALE,
		undead: true,
		raceResourceKey: 'race.undeadDwarf',
	}),
	Origin({
		name: 'customUndeadHumanFemale',
		resourceKey: 'origin.custom',
		race: R_HUMAN,
		gender: G_FEMALE,
		undead: true,
		raceResourceKey: 'race.undeadHuman',
	}),
	Origin({
		name: 'customUndeadHumanMale',
		resourceKey: 'origin.custom',
		race: R_HUMAN,
		gender: G_MALE,
		undead: true,
		raceResourceKey: 'race.undeadHuman',
	}),
	Origin({
		name: 'customUndeadLizardFemale',
		resourceKey: 'origin.custom',
		race: R_LIZARD,
		gender: G_FEMALE,
		undead: true,
		raceResourceKey: 'race.undeadLizard',
	}),
	Origin({
		name: 'customUndeadLizardMale',
		resourceKey: 'origin.custom',
		race: R_LIZARD,
		gender: G_MALE,
		undead: true,
		raceResourceKey: 'race.undeadLizard',
	}),
];

const UNIQUE_ORIGINS = filter(propEq('unique', true), ORIGINS);
export const CUSTOM_ORIGINS = filter(propEq('unique', false), ORIGINS);

export function selectableOriginsFor({ builds, selectedBuild = {} }) {
	const originsInParty = map(prop('origin'), builds);
	const takenOrigins = difference(originsInParty, [selectedBuild.origin]);
	const inParty = pipe(prop('name'), flip(contains)(takenOrigins));
	return [...filter(complement(inParty), UNIQUE_ORIGINS), ...CUSTOM_ORIGINS];
}

function Origin({
	name = '',
	unique = false,
	resourceKey = `origin.${name}`,
	verboseResourceKey = `origin.${name}`,
	race = '',
	gender = 'female',
	undead = false,
	raceResourceKey = undead ? `race.${race}` : `race.${race}`,
}) {
	return { name, unique, resourceKey, verboseResourceKey, race, gender, raceResourceKey, undead };
}
