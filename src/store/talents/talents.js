import { always, both, complement, contains, converge, curry, defaultTo, equals, find, T, or, pipe, prop, propEq, propSatisfies } from 'ramda';
import {
	O_SEBILLE, R_DWARF, R_ELF, R_HUMAN, R_LIZARD,
	A_STRENGTH, A_FINESSE, A_INTELLIGENCE, A_CONSTITUTION, A_MEMORY, A_WITS,
	CS_HUNTSMAN, CS_SCOUNDREL, CS_WARFARE,
	CV_BARTERING, CV_LOREMASTER, CV_PERSUASION, CV_SNEAKING,
	DS_ACCURACY, DS_AP_REGEN, DS_AP_START, DS_CRIT_CHANCE, DS_CRIT_MULTIPLIER,
	DS_DODGING, DS_EFFECT_DURATION_BONUSES, DS_RANGE_BONUS, DS_VITALITY_MULTIPLIER,
	EF_BURNING, EF_POISONED, EF_BLEEDING, EF_NECROFIRE, EF_ACID, EF_SUFFOCATING, EF_ENTANGLED, EF_DEATH_WISH, EF_RUPTURED_TENDONS,
	RS_FIRE, RS_POISON, RS_WATER,
	T_ANCESTRAL_KNOWLEDGE, T_CORPSE_EATER, T_DWARVEN_GUILE, T_STURDY, T_THRIFTY, T_INGENIOUS, T_SPELLSONG, T_SOPHISTICATED, T_UNDEAD,
	T_ALL_SKILLED_UP, T_BIGGER_AND_BETTER, T_DEMON, T_EXECUTIONER, T_FAR_OUT_MAN,
	T_GLASS_CANNON, T_HOTHEAD, T_ICE_KING, T_LONE_WOLF, T_MNEMONIC,
	T_PARRY_MASTER, T_PETPAL, T_PICTURE_OF_HEALTH, T_THE_PAWN, T_TORTURER,
	T_ROOTED,	T_SPIDERS_KISS_TO, T_SPIDERS_KISS_CQ, T_SPIDERS_KISS_GC, T_SPIDERS_KISS_SD,	T_SPIDERS_KISS_TC, T_TRADERS_SECRETS,
	T_TRAINING_AUTHORITY, T_TRAINING_INTELLIGENCE, T_TRAINING_WITS, T_TRAINING_FINESSE, T_TRAINING_CONSTITUTION, T_TRAINING_STRENGTH,
} from '../constants';
import { hasAbility } from '../utils';
import { toOriginProp } from '../origins';
import { Bonus } from '../bonuses';
import { lonewolfBonusesFor } from './lone-wolf-bonuses';

const SPIDERS_KISSES = [T_SPIDERS_KISS_CQ, T_SPIDERS_KISS_GC, T_SPIDERS_KISS_SD, T_SPIDERS_KISS_TC, T_SPIDERS_KISS_TO];

export const ALL_TALENTS = [
	// Racial talents
	RacialTalent({ race: R_ELF, name: T_ANCESTRAL_KNOWLEDGE, aliveOnly: true, bonusesFor: always({
		[CV_LOREMASTER]: Bonus({ name: T_ANCESTRAL_KNOWLEDGE }) }),
	}),
	RacialTalent({ race: R_ELF, name: T_CORPSE_EATER }),
	RacialTalent({ race: R_DWARF, name: T_DWARVEN_GUILE, aliveOnly: true, bonusesFor: always({
		[CV_SNEAKING]: Bonus({ name: T_DWARVEN_GUILE }) }) }),
	RacialTalent({ race: R_DWARF, name: T_STURDY, bonusesFor: always({
		[DS_VITALITY_MULTIPLIER]: Bonus({ name: T_STURDY, value: 10 }),
		[DS_DODGING]: Bonus({ name: T_STURDY, value: 5 }) }) }),
	RacialTalent({ race: R_HUMAN, name: T_THRIFTY, aliveOnly: true, bonusesFor: always({
		[CV_BARTERING]: Bonus({ name: T_THRIFTY }) }) }),
	RacialTalent({ race: R_HUMAN, name: T_INGENIOUS, bonusesFor: always({
		[DS_CRIT_CHANCE]: Bonus({ name: T_INGENIOUS, value: 5 }),
		[DS_CRIT_MULTIPLIER]: Bonus({ name: T_INGENIOUS, value: 10 }) }) }),
	RacialTalent({ race: R_LIZARD, name: T_SPELLSONG, aliveOnly: true, bonusesFor: always({
		[CV_PERSUASION]: Bonus({ name: T_SPELLSONG }) }) }),
	RacialTalent({ race: R_LIZARD, name: T_SOPHISTICATED, bonusesFor: always({
		[RS_FIRE]: Bonus({ name: T_SOPHISTICATED, value: 10 }),
		[RS_POISON]: Bonus({ name: T_SOPHISTICATED, value: 10 }) }) }),
	RacialTalent({ name: T_UNDEAD, availableFor: isUndead(true), bonusesFor: always({
		[RS_POISON]: Bonus({ name: T_UNDEAD, value: 200, capRaise: 100 }) }) }),

	// Selectable talents
	Talent({ name: T_ALL_SKILLED_UP, availableFor: atLeastLevel(2) }),
	Talent({ name: 'ambidextrous' }),
	Talent({ name: 'arrowRecovery' }),
	Talent({ name: T_BIGGER_AND_BETTER, availableFor: atLeastLevel(2) }),
	Talent({ name: 'comebackKid' }),
	Talent({ name: T_DEMON, availableFor: notHasTalent(T_ICE_KING), bonusesFor: always({
		[RS_FIRE]: Bonus({ name: T_DEMON, value: 15, capRaise: 10 }),
		[RS_WATER]: Bonus({ name: T_DEMON, value: -15, capRaise: 0 }) }) }),
	Talent({ name: 'duckDuckGoose', availableFor: hasAbility(CS_HUNTSMAN) }),
	Talent({ name: 'elementalAffinity' }),
	Talent({ name: 'elementalRanger', availableFor: hasAbility(CS_HUNTSMAN) }),
	Talent({ name: 'escapist' }),
	Talent({ name: T_EXECUTIONER, availableFor: both(notHasTalent(T_THE_PAWN), hasAbility(CS_WARFARE)) }),
	Talent({ name: T_FAR_OUT_MAN, bonusesFor: always({
		[DS_RANGE_BONUS]: Bonus({ name: T_FAR_OUT_MAN, value: 2 }) }) }),
	Talent({ name: 'fiveStarDiner' }),
	Talent({ name: T_GLASS_CANNON, availableFor: notHasTalent(T_LONE_WOLF), bonusesFor: always({
		[DS_AP_REGEN]: Bonus({ name: T_GLASS_CANNON, value: 2, capRaise: 0 }),
		[DS_AP_START]: Bonus({ name: T_GLASS_CANNON, value: 2, capRaise: 0 }) }) }),
	Talent({ name: 'guerrilla' }),
	Talent({ name: T_HOTHEAD, bonusesFor: always({
		[DS_ACCURACY]: Bonus({ name: T_HOTHEAD, value: 10 }),
		[DS_CRIT_CHANCE]: Bonus({ name: T_HOTHEAD, value: 10 }) }) }),
	Talent({ name: T_ICE_KING, availableFor: notHasTalent(T_DEMON), bonusesFor: always({
		[RS_WATER]: Bonus({ name: T_ICE_KING, value: 15, capRaise: 10 }),
		[RS_FIRE]: Bonus({ name: T_ICE_KING, value: -15, capRaise: 0 }) }) }),
	Talent({ name: 'leech' }),
	Talent({ name: 'livingArmour' }),
	Talent({ name: T_LONE_WOLF, availableFor: notHasTalent(T_GLASS_CANNON), bonusesFor: lonewolfBonusesFor }),
	Talent({ name: T_MNEMONIC, bonusesFor: always({
		[A_MEMORY]: Bonus({ name: T_MNEMONIC, value: 3 }) }) }),
	Talent({ name: 'morningPerson' }),
	Talent({ name: 'opportunist' }),
	Talent({ name: T_PARRY_MASTER, bonusesFor: always({
		[DS_DODGING]: Bonus({ name: T_PARRY_MASTER, value: 10 }) }) }),
	Talent({ name: T_PETPAL }),
	Talent({ name: T_PICTURE_OF_HEALTH, availableFor: hasAbility(CS_WARFARE), bonusesFor: character => ({
		[DS_VITALITY_MULTIPLIER]: Bonus({ name: T_PICTURE_OF_HEALTH, value: character[CS_WARFARE] * 3 }) }) }),
	Talent({ name: 'savageSortilege' }),
	Talent({ name: 'slingshot' }),
	Talent({ name: 'stench' }),
	Talent({ name: T_THE_PAWN, availableFor: both(notHasTalent(T_EXECUTIONER), hasAbility(CS_SCOUNDREL)) }),
	Talent({ name: T_TORTURER, bonusesFor: always({
		[DS_EFFECT_DURATION_BONUSES]: Bonus({ name: T_TORTURER,  capRaise: 1, value: [
			EF_BURNING, EF_POISONED, EF_BLEEDING, EF_NECROFIRE, EF_ACID,
			EF_SUFFOCATING, EF_ENTANGLED, EF_DEATH_WISH, EF_RUPTURED_TENDONS,
		].reduce((bonuses, effect) => ({ ...bonuses, [effect]: 1 }), {}) }) }) }),
	Talent({ name: 'unstable' }),
	Talent({ name: 'walkItOff' }),
	Talent({ name: 'whatARush' }),

	// Story talents
	Talent({ name: T_ROOTED, free: true, availableFor: originIs(O_SEBILLE), bonusesFor: always({
		[A_MEMORY]: Bonus({ name: T_ROOTED, value: 3 }) }) }),
	Talent({ name: T_SPIDERS_KISS_TO, free: true, availableFor: notKissedSpiderOrHas(T_SPIDERS_KISS_TO), bonusesFor: always({
		[A_STRENGTH]: Bonus({ name: T_SPIDERS_KISS_TO, value: 2 }),
		[A_CONSTITUTION]: Bonus({ name: T_SPIDERS_KISS_TO, value: -2 }) }) }),
	Talent({ name: T_SPIDERS_KISS_CQ, free: true, availableFor: notKissedSpiderOrHas(T_SPIDERS_KISS_CQ), bonusesFor: always({
		[A_INTELLIGENCE]: Bonus({ name: T_SPIDERS_KISS_CQ, value: 2 }),
		[A_CONSTITUTION]: Bonus({ name: T_SPIDERS_KISS_CQ, value: -2 }) }) }),
	Talent({ name: T_SPIDERS_KISS_GC, free: true, availableFor: notKissedSpiderOrHas(T_SPIDERS_KISS_GC), bonusesFor: always({
		[A_CONSTITUTION]: Bonus({ name: T_SPIDERS_KISS_GC, value: -2 }) }) }),
	Talent({ name: T_SPIDERS_KISS_SD, free: true, availableFor: notKissedSpiderOrHas(T_SPIDERS_KISS_SD), bonusesFor: always({
		[A_WITS]: Bonus({ name: T_SPIDERS_KISS_SD, value: 2 }),
		[A_CONSTITUTION]: Bonus({ name: T_SPIDERS_KISS_SD, value: -2 }) }) }),
	Talent({ name: T_SPIDERS_KISS_TC, free: true,
		availableFor: both(
				notKissedSpiderOrHas(T_SPIDERS_KISS_TC),
				talentNotTaken(T_SPIDERS_KISS_TC)),
		bonusesFor: always({ [A_CONSTITUTION]: Bonus({ name: T_SPIDERS_KISS_TC, value: -2 }) }) }),
	Talent({ name: T_TRADERS_SECRETS, free: true, availableFor: both(raceIs(R_ELF), headNotEaten), bonusesFor: always({
		[CV_BARTERING]: Bonus({ name: T_TRADERS_SECRETS }) }) }),

	TrainingTalent({ name: T_TRAINING_AUTHORITY, bonusOn: A_MEMORY, penaltyOn: A_STRENGTH }),
	TrainingTalent({ name: T_TRAINING_CONSTITUTION, bonusOn: A_CONSTITUTION, penaltyOn: A_INTELLIGENCE }),
	TrainingTalent({ name: T_TRAINING_FINESSE, bonusOn: A_FINESSE, penaltyOn: A_MEMORY }),
	TrainingTalent({ name: T_TRAINING_INTELLIGENCE, bonusOn: A_INTELLIGENCE, penaltyOn: A_FINESSE }),
	TrainingTalent({ name: T_TRAINING_STRENGTH, bonusOn: A_STRENGTH, penaltyOn: A_WITS }),
	TrainingTalent({ name: T_TRAINING_WITS, bonusOn: A_WITS, penaltyOn: A_CONSTITUTION }),
];

export function rankedTalentsOf(character) {
	return ALL_TALENTS.filter(({ name, availableFor }) =>
		character.talents.includes(name) &&
		availableFor(character));
}

export function racialTalentsOf(character) {
	return ALL_TALENTS.filter(({ autoAdd, availableFor }) =>
		autoAdd && availableFor(character));
}

export function selectableTalentsFor(character) {
	return ALL_TALENTS.filter(({ name, autoAdd, availableFor }) =>
		!autoAdd &&
		availableFor(character) &&
		!character.talents.includes(name));
}

export function unselectableTalentsFor(character) {
	const isUnpickedSpidersKiss = hasKissedSpider(character) ?
		name => contains(name, SPIDERS_KISSES) :
		always(false);
	return ALL_TALENTS.filter(({ name, autoAdd, availableFor }) =>
		!autoAdd &&
		!availableFor(character) &&
		!isUnpickedSpidersKiss(name) &&
		!character.talents.includes(name));
}

export const toTalentProp = curry((propName, talentName) => {
	return pipe(toTalentDetails, prop(propName))(talentName);
});

export function toTalentDetails(talentName) {
	const details = ALL_TALENTS.find(propEq('name', talentName));
	if (!details) { throw new Error(`Unknown talent: ${talentName}`); }
	return details;
}

function Talent({
	name = '',
	resourceKey = `talent.${name}`,
	autoAdd = false,
	free = false,
	availableFor = T,
	bonusesFor = always(),
}) {
	return { name, resourceKey, autoAdd, free, availableFor, bonusesFor };
}

function RacialTalent(opts) {
	const { race, aliveOnly } = opts;
	const availableFor = both(
			raceIs(race),
			aliveOnly ? isUndead(false) : T);
	return Talent({ autoAdd: true, availableFor, ...opts });
}

function TrainingTalent({ name, bonusOn, penaltyOn, value = 5 }) {
	return Talent({ name, free: true, availableFor: talentNotTaken(name), bonusesFor: always({
		[bonusOn]: Bonus({ name: name, value }),
		[penaltyOn]: Bonus({ name: name, value: -value }) }) });
}

function atLeastLevel(number) {
	return propSatisfies(lv => lv >= number, 'level');
}

function hasTalent(talent) {
	const containsTalent = pipe(defaultTo([]), contains(talent));
	return converge(or, [
		propSatisfies(containsTalent, 'talents'),
		propSatisfies(containsTalent, 'freeTalents'),
	]);
}

function notHasTalent(talent) {
	return complement(hasTalent(talent));
}

function notKissedSpiderOrHas(exception) {
	return character =>
		hasTalent(exception)(character) ||
		!hasKissedSpider(character);
}

function hasKissedSpider(character) {
	return hasTalent(T_SPIDERS_KISS_CQ)(character) ||
		hasTalent(T_SPIDERS_KISS_GC)(character) ||
		hasTalent(T_SPIDERS_KISS_SD)(character) ||
		hasTalent(T_SPIDERS_KISS_TC)(character) ||
		hasTalent(T_SPIDERS_KISS_TO)(character);
}

function headNotEaten(character) {
	return talentNotTaken(T_TRADERS_SECRETS)(character);
}

function talentNotTaken(talent) {
	const alreadyHasBonus = ({ talents = [], freeTalents = [] }) => [...talents, ...freeTalents].includes(talent);
	return ({ companions }) => !find(alreadyHasBonus, companions);
}

function originIs(origin) {
	return propEq('origin', origin);
}

function raceIs(race) {
	return pipe(prop('origin'), toOriginProp('race'), equals(race));
}

function isUndead(undead) {
	return pipe(prop('origin'), toOriginProp('undead'), equals(undead));
}
