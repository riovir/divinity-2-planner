import { assoc, without } from 'ramda';
import {
	T_LONE_WOLF, ATTRIBUTES, COMBAT_ABILITIES, CS_POLYMORPH, DS_AP_MAX, DS_AP_REGEN, DS_AP_START, DS_VITALITY_MULTIPLIER,
} from '../constants';
import { Bonus } from '../bonuses';

export function lonewolfBonusesFor(character) {
	const active = character.companionsInParty.length < 2;
	return bonusesFor({ character, active });
}

function bonusesFor({ character, active }) {
	const ifActive = value => active ? value : 0;
	const pointsSpentOn = field => ATTRIBUTES.includes(field) ? character[field] - 10 : character[field];
	const resourceKey = active ? 'talent.lonewolf' : 'talent.lonewolfInactive';

	const assocBonus = (bonuses, field) => {
		const value = pointsSpentOn(field);
		const bonus = Bonus({ name: T_LONE_WOLF, value: ifActive(value), capRaise: 0, resourceKey });
		return value > 0 ? assoc(field, bonus, bonuses) : bonuses;
	};
	const initialBonuses = {
		[DS_VITALITY_MULTIPLIER]: Bonus({ name: T_LONE_WOLF, value: ifActive(30), resourceKey }),
		[DS_AP_MAX]: Bonus({ name: T_LONE_WOLF, value: ifActive(2), capRaise: 0, resourceKey }),
		[DS_AP_REGEN]: Bonus({ name: T_LONE_WOLF, value: ifActive(2), capRaise: 0, resourceKey }),
		[DS_AP_START]: Bonus({ name: T_LONE_WOLF, value: ifActive(2), capRaise: 0, resourceKey }),
	};
	return without([CS_POLYMORPH], [...ATTRIBUTES, ...COMBAT_ABILITIES]).reduce(assocBonus, initialBonuses);
}
