import { assocPath, difference, pipe, prop, propEq } from 'ramda';
import {
	ATTRIBUTE_CAP, COMBAT_ABILITY_CAP, CIVIL_ABILITY_CAP,
	ATTRIBUTES, COMBAT_ABILITIES, CIVIL_ABILITIES, RESISTS,
	C_COMPANIONS, C_COMPANIONS_IN_PARTY,
	O_BEAST, O_FANE, O_LOHSE, O_RED_PRINCE, O_SEBILLE, O_CUSTOM_ELF_FEMALE,
	A_MEMORY,	A_WITS,
	CS_SCOUNDREL, CS_POLYMORPH, CS_WARFARE, CV_BARTERING, CV_LOREMASTER, CV_PERSUASION, CV_SNEAKING,
	DS_AP_MAX, DS_AP_REGEN, DS_AP_START, DS_ACCURACY, DS_CRIT_CHANCE, DS_CRIT_MULTIPLIER,
	DS_DODGING, DS_EFFECT_DURATION_BONUSES, DS_VITALITY_MULTIPLIER, DS_RANGE_BONUS,
	RS_FIRE, RS_WATER, RS_POISON, EF_ACID,
	T_ANCESTRAL_KNOWLEDGE, T_CORPSE_EATER, T_FAR_OUT_MAN, T_DWARVEN_GUILE,
	T_STURDY, T_THRIFTY, T_INGENIOUS, T_SPELLSONG, T_SOPHISTICATED, T_UNDEAD,
	T_DEMON, T_GLASS_CANNON, T_HOTHEAD, T_ICE_KING, T_LONE_WOLF, T_MNEMONIC, T_PARRY_MASTER,
	T_PICTURE_OF_HEALTH, T_ROOTED, T_TRADERS_SECRETS, T_TORTURER,
	T_TRAINING_STRENGTH, T_TRAINING_INTELLIGENCE, A_STRENGTH, A_INTELLIGENCE, A_FINESSE,
} from '../constants';
import { toTalentProp, ALL_TALENTS } from './talents';
import Character from '../character';

describe('lone wolf', () => {
	test('adds bonus to attribute', () => {
		const character = Character({ [A_WITS]: 13, talents: [T_LONE_WOLF] });
		expect(character[A_WITS]).toBe(16);
	});

	test('adds bonus some combat skills', () => {
		const character = Character({ [CS_SCOUNDREL]: 4, talents: [T_LONE_WOLF] });
		expect(character[CS_SCOUNDREL]).toBe(8);
	});

	test('does not raise cap', () => {
		const character = Character({ [CS_SCOUNDREL]: 7, talents: [T_LONE_WOLF] });
		expect(character[CS_SCOUNDREL]).toBe(COMBAT_ABILITY_CAP);
	});

	test('does not affect polymorph', () => {
		const character = Character({ [CS_POLYMORPH]: 7, talents: [T_LONE_WOLF] });
		expect(character[CS_POLYMORPH]).toBe(7);
	});

	test('does not affect stats without invested points', () => {
		const character = Character({ [A_WITS]: 10, talents: [T_LONE_WOLF] });
		expect(character[A_WITS]).toBe(10);
	});

	test('raises AP stats by 2, vitality multiplier by 30', () => {
		const character = Character({ talents: [T_LONE_WOLF] });
		expect(character[DS_AP_MAX]).toBe(8);
		expect(character[DS_AP_REGEN]).toBe(6);
		expect(character[DS_AP_START]).toBe(6);
		expect(character[DS_VITALITY_MULTIPLIER]).toBe(30);
	});

	test('applies bonuses up to 2 characters', () => {
		const sebille = Character({ origin: O_SEBILLE, [A_WITS]: 13, talents: [T_LONE_WOLF] });
		const beast = Character({ origin: O_BEAST, talents: [T_LONE_WOLF] });
		const lohse = Character({ origin: O_LOHSE, talents: [T_LONE_WOLF] });

		const sebilleInParty = companionsInParty([beast], sebille);
		expect(sebilleInParty[A_WITS]).toBe(16);
		const sebilleInCrowd = companionsInParty([beast, lohse], sebille);
		expect(sebilleInCrowd[A_WITS]).toBe(13);
	});
});

describe("trader's secrets", () => {
	test('raises bartering for corpse eaters', () => {
		const sebille = Character({ origin: O_SEBILLE, [CV_BARTERING]: CIVIL_ABILITY_CAP, talents: [T_TRADERS_SECRETS] });
		expect(sebille[CV_BARTERING]).toBe(6);
	});

	test('is exclusive to a single character', () => {
		const sebilleAlone = Character({ origin: O_SEBILLE, talents: [T_TRADERS_SECRETS] });
		const anotherElf = Character({ origin: O_CUSTOM_ELF_FEMALE, talents: [T_TRADERS_SECRETS] });
		const sebilleInParty = companionsInParty([anotherElf], sebilleAlone);
		expect(sebilleInParty[CV_BARTERING]).toBe(0);
		expect(sebilleInParty.bonuses[CV_BARTERING]).toEqual([]);
	});
});

test('mnemonic raises memory and cap by 3', () => {
	const character = Character({ [A_MEMORY]: ATTRIBUTE_CAP, talents: [T_MNEMONIC] });
	expect(character[A_MEMORY]).toBe(43);
});

test('demon raises fire resistance by 15, reducing water resistance by 15', () => {
	const character = Character({ talents: [T_DEMON] });
	expect(character[RS_FIRE]).toBe(15);
	expect(character[RS_WATER]).toBe(-15);
	const bonusesFor = toTalentProp('bonusesFor', T_DEMON);
	expect(bonusesFor(character)[RS_FIRE].capRaise).toBe(10);
});

test('ice king raises water resistance by 15, cap by 10, reducing fire resistance by 15', () => {
	const character = Character({ talents: [T_ICE_KING] });
	expect(character[RS_WATER]).toBe(15);
	expect(character[RS_FIRE]).toBe(-15);
	const bonusesFor = toTalentProp('bonusesFor', T_ICE_KING);
	expect(bonusesFor(character)[RS_WATER].capRaise).toBe(10);
});

test('picture of health raises vitality multiplier by 3 points per wafare', () => {
	const character = Character({ origin: O_SEBILLE, [CS_WARFARE]: 5, talents: [T_PICTURE_OF_HEALTH] });
	expect(character[DS_VITALITY_MULTIPLIER]).toBe(15);
});

test('picture of health receives bonus from lone wolf', () => {
	const character = Character({ origin: O_SEBILLE, [CS_WARFARE]: 5, talents: [T_PICTURE_OF_HEALTH, T_LONE_WOLF] });
	expect(character[DS_VITALITY_MULTIPLIER]).toBe(60);
});

test('rooted raises Sebille\'s memory and cap by 3', () => {
	const sebille = Character({ origin: O_SEBILLE, [A_MEMORY]: ATTRIBUTE_CAP, talents: [T_ROOTED] });
	expect(sebille[A_MEMORY]).toBe(43);
});

test('glass cannon raises refills AP every turn', () => {
	const sebille = Character({ origin: O_SEBILLE, talents: [T_GLASS_CANNON] });
	expect(sebille[DS_AP_MAX]).toBe(6);
	expect(sebille[DS_AP_REGEN]).toBe(6);
	expect(sebille[DS_AP_START]).toBe(6);
});

test('hothead raises accuracy and crit chance by 10', () => {
	const sebille = Character({ origin: O_SEBILLE, talents: [T_HOTHEAD] });
	expect(sebille[DS_ACCURACY]).toBe(105);
	expect(sebille[DS_CRIT_CHANCE]).toBe(10);
});

test('parry master raises dodge by 10', () => {
	const sebille = Character({ origin: O_SEBILLE, talents: [T_PARRY_MASTER] });
	expect(sebille[DS_DODGING]).toBe(10);
});

test('far out man adds range bonus of 2', () => {
	const sebille = Character({ origin: O_SEBILLE, talents: [T_FAR_OUT_MAN] });
	expect(sebille[DS_RANGE_BONUS]).toBe(2);
});

test('torturer adds duration bonuses to select skill effects', () => {
	const sebille = Character({ origin: O_SEBILLE, talents: [T_TORTURER] });
	expect(sebille[DS_EFFECT_DURATION_BONUSES]).toEqual(expect.objectContaining({ [EF_ACID]: 1 }));
});

describe('racial talents', () => {
	const dwarf = Character({ origin: O_BEAST });
	const eternal = Character({ origin: O_FANE });
	const human = Character({ origin: O_LOHSE });
	const lizard = Character({ origin: O_RED_PRINCE });
	const elf = Character({ origin: O_SEBILLE });

	test('are automatically granted to applicable races', () => {
		expectMatchingTalents({ dwarf, eternal, human, lizard, elf });
	});

	test('get removed from non-matching races', () => {
		const talents = ALL_TALENTS
				.filter(propEq('autoAdd', true))
				.map(prop('name'));
		expectMatchingTalents({
			dwarf: Character({ origin: O_BEAST, talents }),
			eternal: Character({ origin: O_FANE, talents }),
			human: Character({ origin: O_LOHSE, talents }),
			lizard: Character({ origin: O_RED_PRINCE, talents }),
			elf: Character({ origin: O_SEBILLE, talents }),
		});
	});

	function expectMatchingTalents({ dwarf, eternal, human, lizard, elf }) {
		expect(dwarf.talents).toEqual(expect.arrayContaining([T_DWARVEN_GUILE, T_STURDY]));
		expect(eternal.talents).toEqual(expect.arrayContaining([T_INGENIOUS, T_UNDEAD]));
		expect(human.talents).toEqual(expect.arrayContaining([T_INGENIOUS, T_THRIFTY]));
		expect(lizard.talents).toEqual(expect.arrayContaining([T_SPELLSONG, T_SOPHISTICATED]));
		expect(elf.talents).toEqual(expect.arrayContaining([T_ANCESTRAL_KNOWLEDGE, T_CORPSE_EATER]));
	}

	test('apply bonuses to dwarves', () => {
		expect(dwarf[CV_SNEAKING]).toBe(1);
		expect(dwarf[DS_DODGING]).toBe(5);
		expect(dwarf[DS_VITALITY_MULTIPLIER]).toBe(10);
	});

	test('apply bonuses to elves', () => {
		expect(elf[CV_LOREMASTER]).toBe(1);
	});

	test('apply bonuses to humans', () => {
		expect(human[DS_CRIT_CHANCE]).toBe(5);
		expect(human[DS_CRIT_MULTIPLIER]).toBe(160);
		expect(human[CV_BARTERING]).toBe(1);
	});

	test('apply bonuses to lizards', () => {
		expect(lizard[RS_FIRE]).toBe(10);
		expect(lizard[RS_POISON]).toBe(10);
		expect(lizard[CV_PERSUASION]).toBe(1);
	});

	test('apply bonuses to the undead', () => {
		expect(eternal[RS_POISON]).toBe(200);
		expect(eternal[DS_CRIT_CHANCE]).toBe(5);
		expect(eternal[DS_CRIT_MULTIPLIER]).toBe(160);
	});

	test('do not apply bonuses from non-matching races', () => {
		expectDefaultStatsExcept([CV_SNEAKING, DS_DODGING, DS_VITALITY_MULTIPLIER], dwarf);
		expectDefaultStatsExcept([RS_POISON, DS_CRIT_CHANCE, DS_CRIT_MULTIPLIER], eternal);
		expectDefaultStatsExcept([CV_BARTERING, DS_CRIT_CHANCE, DS_CRIT_MULTIPLIER], human);
		expectDefaultStatsExcept([CV_PERSUASION, RS_FIRE, RS_POISON], lizard);
		expectDefaultStatsExcept([CV_LOREMASTER], elf);
	});

	function expectDefaultStatsExcept(exceptions, character) {
		const shouldBe = expected => stat => expect(character)
				.toEqual(expect.objectContaining({ [stat]: expected }));
		difference(ATTRIBUTES, exceptions).forEach(shouldBe(10));
		const remaining = [...COMBAT_ABILITIES, ...CIVIL_ABILITIES, ...RESISTS];
		difference(remaining, exceptions).forEach(shouldBe(0));
	}
});

describe('academy training talents', () => {
	test('boosts one ability', () => expect(Character({ talents: [T_TRAINING_STRENGTH, T_TRAINING_INTELLIGENCE] }))
			.toEqual(expect.objectContaining({ [A_STRENGTH]: 15, [A_INTELLIGENCE]: 15 })));
	test('penalizes another', () => expect(Character({ talents: [T_TRAINING_STRENGTH, T_TRAINING_INTELLIGENCE] }))
			.toEqual(expect.objectContaining({ [A_WITS]: 5, [A_FINESSE]: 5 })));
	test('can only be taken by one character each', () => {
		const dwarf = Character({ origin: O_BEAST, talents: [T_TRAINING_STRENGTH] });
		const eternal = Character({ origin: O_FANE, talents: [T_TRAINING_STRENGTH, T_TRAINING_INTELLIGENCE] });
		const eternalInParty = companionsInParty([dwarf], eternal);
		expect(eternalInParty).toEqual(expect.objectContaining({ [A_INTELLIGENCE]: 15, [A_STRENGTH]: 10 }));
	});
});

function companionsInParty(others, character) {
	return pipe(
			assocPath([C_COMPANIONS], others),
			assocPath([C_COMPANIONS_IN_PARTY], others),
			Character,
	)(character);
}

