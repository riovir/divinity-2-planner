import { always, contains, curry, flip, map, max, pipe, prop, propEq, reduce } from 'ramda';
import {
	CS_AEROTHEURGE, CS_DUAL_WIELDING, CS_GEOMANCER, CS_HYDROSOPHIST, CS_LEADERSHIP,
	CS_PYROKINETIC, CS_SCOUNDREL, CS_RANGED, CS_SINGLE_HANDED, CS_TWO_HANDED, CS_WARFARE,
	DS_DAMAGE_STRENGTH, DS_DAMAGE_FINESSE, DS_DAMAGE_INTELLIGENCE,
	DS_DAMAGE_AIR, DS_DAMAGE_EARTH, DS_DAMAGE_FIRE, DS_DAMAGE_PHYSICAL, DS_DAMAGE_WATER,
	DS_CRIT_CHANCE, DS_DODGING, DS_ACCURACY, DS_CRIT_MULTIPLIER, DS_MOVEMENT,
	ET_MELEE, ET_MISSILE, ET_RANGED, ET_SHIELD, RS_FIRE, RS_WATER, RS_AIR, RS_EARTH, RS_POISON,
} from './constants';
import { Bonus } from './bonuses';
import { toEquipmentProp, toEquipmentDetails } from './equipment';


export const ALL_ABILITIES = [
	CombatAbility({ name: CS_DUAL_WIELDING, bonusesFor: weaponBonusesWhen(isDualWielding,
			{ name: CS_DUAL_WIELDING, customStat: DS_DODGING, customMultiplier: 1 }) }),
	CombatAbility({ name: CS_SINGLE_HANDED, bonusesFor: weaponBonusesWhen(isSingleHanded,
			{ name: CS_SINGLE_HANDED, customStat: DS_ACCURACY }) }),
	CombatAbility({ name: CS_TWO_HANDED, bonusesFor: weaponBonusesWhen(isTwoHanded,
			{ name: CS_TWO_HANDED, customStat: DS_CRIT_MULTIPLIER }) }),
	CombatAbility({ name: CS_RANGED, bonusesFor: weaponBonusesWhen(isRanged,
			{ name: CS_RANGED, customStat: DS_CRIT_CHANCE, customMultiplier: 1 }) }),
	CombatAbility({ name: CS_LEADERSHIP, bonusesFor: leadershipBonusesFor, requiresInvestment: false }),
	CombatAbility({ name: CS_SCOUNDREL, bonusesFor: character => ({
		[DS_CRIT_MULTIPLIER]: CombatBonus({ name: CS_SCOUNDREL, value: character[CS_SCOUNDREL] * 5 }),
		[DS_MOVEMENT]: CombatBonus({ name: CS_SCOUNDREL, value: character[CS_SCOUNDREL] * 0.3 }),
	}) }),

	CombatAbility({ name: CS_AEROTHEURGE, bonusesFor: character => ({
		[DS_DAMAGE_AIR]: CombatBonus({ name: CS_AEROTHEURGE, value: character[CS_AEROTHEURGE] * 5 }),
	}) }),
	CombatAbility({ name: CS_GEOMANCER, bonusesFor: character => ({
		[DS_DAMAGE_EARTH]: CombatBonus({ name: CS_GEOMANCER, value: character[CS_GEOMANCER] * 5 }),
	}) }),
	CombatAbility({ name: CS_HYDROSOPHIST, bonusesFor: character => ({
		[DS_DAMAGE_WATER]: CombatBonus({ name: CS_HYDROSOPHIST, value: character[CS_HYDROSOPHIST] * 5 }),
	}) }),
	CombatAbility({ name: CS_PYROKINETIC, bonusesFor: character => ({
		[DS_DAMAGE_FIRE]: CombatBonus({ name: CS_PYROKINETIC, value: character[CS_PYROKINETIC] * 5 }),
	}) }),
	CombatAbility({ name: CS_WARFARE, bonusesFor: character => ({
		[DS_DAMAGE_PHYSICAL]: CombatBonus({ name: CS_WARFARE, value: character[CS_WARFARE] * 5 }),
	}) }),
];

export const toAbilityProp = curry((propName, talentName) => {
	return pipe(toAbilityDetails, prop(propName))(talentName);
});

function isSingleHanded({ mainHand, offHand }) {
	return mainHand && !toEquipmentProp('twoHanded', mainHand) && !hasWeapon(offHand);
}

function hasWeapon(hand) {
	return hand && toEquipmentProp('type', hand) !== ET_SHIELD;
}

function isDualWielding({ mainHand, offHand }) {
	const isOneOf = flip(contains);
	const isWeapon = pipe(toEquipmentProp('type'), isOneOf([ET_MELEE, ET_MISSILE, ET_RANGED]));
	return isWeapon(mainHand) && isWeapon(offHand);
}

function isTwoHanded({ mainHand, offHand }) {
	const { twoHanded, type } = toEquipmentDetails(mainHand);
	return mainHand && twoHanded && type === ET_MELEE && !offHand;
}

function isRanged({ mainHand }) {
	return mainHand && toEquipmentProp('type', mainHand) === ET_RANGED;
}

function weaponBonusesWhen(condition, { name, customStat, customMultiplier = 5 }) {
	return character => condition(character) ? {
		...WeaponDamageBonuses({ name, value: character[name] * 5 }),
		[customStat]: CombatBonus({ name, value: character[name] * customMultiplier }),
	} : undefined;
}

function WeaponDamageBonuses({ name, value }) {
	const bonus = CombatBonus({ name, value });
	return { [DS_DAMAGE_STRENGTH]: bonus, [DS_DAMAGE_FINESSE]: bonus, [DS_DAMAGE_INTELLIGENCE]: bonus };
}

function leadershipBonusesFor({ companionsInParty }) {
	const bestLeadershipIn = pipe(map(prop(CS_LEADERSHIP)), reduce(max, 0));
	const skill = bestLeadershipIn(companionsInParty);
	const resist = CombatBonus({ name: CS_LEADERSHIP, value: skill * 3 });
	return !skill ? null : {
		[DS_DODGING]: CombatBonus({ name: CS_LEADERSHIP, value: skill * 2 }),
		[RS_FIRE]: resist, [RS_WATER]: resist, [RS_AIR]: resist, [RS_EARTH]: resist, [RS_POISON]: resist,
	};
}

function toAbilityDetails(abilityName) {
	const details = ALL_ABILITIES.find(propEq('name', abilityName));
	if (!details) { throw new Error(`Unknown ability: ${abilityName}`); }
	return details;
}

function CombatAbility({ name = '', resourceKey = `combatAbility.${name}`, bonusesFor = always(), requiresInvestment = true }) {
	const validBonuses = character => !requiresInvestment || character[name] ? bonusesFor(character) : undefined;
	return { name, resourceKey, bonusesFor: validBonuses };
}

function CombatBonus({ name, resourceKey = `combatAbility.${name}`, ...rest }) {
	return Bonus({ name, resourceKey, ...rest });
}
