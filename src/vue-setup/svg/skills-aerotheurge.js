import skillAerotheurgeApportation from 'src/assets/skill-aerotheurge/apportation.svg';
import skillAerotheurgeBlessedSmokeCloud from 'src/assets/skill-aerotheurge/blessed-smoke-cloud.svg';
import skillAerotheurgeBlindingRadiance from 'src/assets/skill-aerotheurge/blinding-radiance.svg';
import skillAerotheurgeBreathingBubble from 'src/assets/skill-aerotheurge/breathing-bubble.svg';
import skillAerotheurgeChainLightning from 'src/assets/skill-aerotheurge/chain-lightning.svg';
import skillAerotheurgeClosedCircuit from 'src/assets/skill-aerotheurge/closed-circuit.svg';
import skillAerotheurgeDazingBolt from 'src/assets/skill-aerotheurge/dazing-bolt.svg';
import skillAerotheurgeElectricDischarge from 'src/assets/skill-aerotheurge/electric-discharge.svg';
import skillAerotheurgeErraticWisp from 'src/assets/skill-aerotheurge/erratic-wisp.svg';
import skillAerotheurgeEvasiveAura from 'src/assets/skill-aerotheurge/evasive-aura.svg';
import skillAerotheurgeFavourableWind from 'src/assets/skill-aerotheurge/favourable-wind.svg';
import skillAerotheurgeMassBreathingBubbles from 'src/assets/skill-aerotheurge/mass-breathing-bubbles.svg';
import skillAerotheurgeNetherSwap from 'src/assets/skill-aerotheurge/nether-swap.svg';
import skillAerotheurgePressureSpike from 'src/assets/skill-aerotheurge/pressure-spike.svg';
import skillAerotheurgeShockingTouch from 'src/assets/skill-aerotheurge/shocking-touch.svg';
import skillAerotheurgeSmokeCover from 'src/assets/skill-aerotheurge/smoke-cover.svg';
import skillAerotheurgeSuperconductor from 'src/assets/skill-aerotheurge/superconductor.svg';
import skillAerotheurgeTeleportation from 'src/assets/skill-aerotheurge/teleportation.svg';
import skillAerotheurgeThunderstorm from 'src/assets/skill-aerotheurge/thunderstorm.svg';
import skillAerotheurgeTornado from 'src/assets/skill-aerotheurge/tornado.svg';
import skillAerotheurgeUncannyEvasion from 'src/assets/skill-aerotheurge/uncanny-evasion.svg';
import skillAerotheurgeVacuumAura from 'src/assets/skill-aerotheurge/vacuum-aura.svg';
import skillAerotheurgeVacuumTouch from 'src/assets/skill-aerotheurge/vacuum-touch.svg';
import skillAerotheurgeVaporise from 'src/assets/skill-aerotheurge/vaporise.svg';

export default {
	skillAerotheurgeApportation,
	skillAerotheurgeBlessedSmokeCloud,
	skillAerotheurgeBlindingRadiance,
	skillAerotheurgeBreathingBubble,
	skillAerotheurgeChainLightning,
	skillAerotheurgeClosedCircuit,
	skillAerotheurgeDazingBolt,
	skillAerotheurgeElectricDischarge,
	skillAerotheurgeErraticWisp,
	skillAerotheurgeEvasiveAura,
	skillAerotheurgeFavourableWind,
	skillAerotheurgeMassBreathingBubbles,
	skillAerotheurgeNetherSwap,
	skillAerotheurgePressureSpike,
	skillAerotheurgeShockingTouch,
	skillAerotheurgeSmokeCover,
	skillAerotheurgeSuperconductor,
	skillAerotheurgeTeleportation,
	skillAerotheurgeThunderstorm,
	skillAerotheurgeTornado,
	skillAerotheurgeUncannyEvasion,
	skillAerotheurgeVacuumAura,
	skillAerotheurgeVacuumTouch,
	skillAerotheurgeVaporise,
};
