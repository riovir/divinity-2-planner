import skillPolymorphApotheosis from 'src/assets/skill-polymorph/apotheosis.svg';
import skillPolymorphBullHorns from 'src/assets/skill-polymorph/bull-horns.svg';
import skillPolymorphChameleonCloak from 'src/assets/skill-polymorph/chameleon-cloak.svg';
import skillPolymorphChickenClaw from 'src/assets/skill-polymorph/chicken-claw.svg';
import skillPolymorphEqualise from 'src/assets/skill-polymorph/equalise.svg';
import skillPolymorphFlamingSkin from 'src/assets/skill-polymorph/flaming-skin.svg';
import skillPolymorphFlaySkin from 'src/assets/skill-polymorph/flay-skin.svg';
import skillPolymorphForcedExchange from 'src/assets/skill-polymorph/forced-exchange.svg';
import skillPolymorphHeartOfSteel from 'src/assets/skill-polymorph/heart-of-steel.svg';
import skillPolymorphIcySkin from 'src/assets/skill-polymorph/icy-skin.svg';
import skillPolymorphJellyfishSkin from 'src/assets/skill-polymorph/jellyfish-skin.svg';
import skillPolymorphMedusaHead from 'src/assets/skill-polymorph/medusa-head.svg';
import skillPolymorphPoisonousSkin from 'src/assets/skill-polymorph/poisonous-skin.svg';
import skillPolymorphSkinGraft from 'src/assets/skill-polymorph/skin-graft.svg';
import skillPolymorphSpiderLegs from 'src/assets/skill-polymorph/spider-legs.svg';
import skillPolymorphSpreadYourWings from 'src/assets/skill-polymorph/spread-your-wings.svg';
import skillPolymorphSummonOilyBlob from 'src/assets/skill-polymorph/summon-oily-blob.svg';
import skillPolymorphTentacleLash from 'src/assets/skill-polymorph/tentacle-lash.svg';
import skillPolymorphTerrainTransmutation from 'src/assets/skill-polymorph/terrain-transmutation.svg';

export default {
	skillPolymorphApotheosis,
	skillPolymorphBullHorns,
	skillPolymorphChameleonCloak,
	skillPolymorphChickenClaw,
	skillPolymorphEqualise,
	skillPolymorphFlamingSkin,
	skillPolymorphFlaySkin,
	skillPolymorphForcedExchange,
	skillPolymorphHeartOfSteel,
	skillPolymorphIcySkin,
	skillPolymorphJellyfishSkin,
	skillPolymorphMedusaHead,
	skillPolymorphPoisonousSkin,
	skillPolymorphSkinGraft,
	skillPolymorphSpiderLegs,
	skillPolymorphSpreadYourWings,
	skillPolymorphSummonOilyBlob,
	skillPolymorphTentacleLash,
	skillPolymorphTerrainTransmutation,
};
