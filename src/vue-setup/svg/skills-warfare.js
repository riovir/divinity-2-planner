import skillWarfareBatteringRam from 'src/assets/skill-warfare/battering-ram.svg';
import skillWarfareBattleStomp from 'src/assets/skill-warfare/battle-stomp.svg';
import skillWarfareBlitzAttack from 'src/assets/skill-warfare/blitz-attack.svg';
import skillWarfareBouncingShield from 'src/assets/skill-warfare/bouncing-shield.svg';
import skillWarfareChallenge from 'src/assets/skill-warfare/challenge.svg';
import skillWarfareCripplingBlow from 'src/assets/skill-warfare/crippling-blow.svg';
import skillWarfareDeflectiveBarrier from 'src/assets/skill-warfare/deflective-barrier.svg';
import skillWarfareEnrage from 'src/assets/skill-warfare/enrage.svg';
import skillWarfareGuardianAngel from 'src/assets/skill-warfare/guardian-angel.svg';
import skillWarfareOnslaught from 'src/assets/skill-warfare/onslaught.svg';
import skillWarfareOverpower from 'src/assets/skill-warfare/overpower.svg';
import skillWarfarePhoenixDive from 'src/assets/skill-warfare/phoenix-dive.svg';
import skillWarfareProvoke from 'src/assets/skill-warfare/provoke.svg';
import skillWarfareThickOfTheFight from 'src/assets/skill-warfare/thick-of-the-fight.svg';
import skillWarfareWhirlwind from 'src/assets/skill-warfare/whirlwind.svg';

export default {
	skillWarfareBatteringRam,
	skillWarfareBattleStomp,
	skillWarfareBlitzAttack,
	skillWarfareBouncingShield,
	skillWarfareChallenge,
	skillWarfareCripplingBlow,
	skillWarfareDeflectiveBarrier,
	skillWarfareEnrage,
	skillWarfareGuardianAngel,
	skillWarfareOnslaught,
	skillWarfareOverpower,
	skillWarfarePhoenixDive,
	skillWarfareProvoke,
	skillWarfareThickOfTheFight,
	skillWarfareWhirlwind,
};
