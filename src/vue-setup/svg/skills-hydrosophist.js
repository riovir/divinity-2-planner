import skillHydrosophistArcaneStitch from 'src/assets/skill-hydrosophist/arcane-stitch.svg';
import skillHydrosophistArmourOfFrost from 'src/assets/skill-hydrosophist/armour-of-frost.svg';
import skillHydrosophistCleanseWounds from 'src/assets/skill-hydrosophist/cleanse-wounds.svg';
import skillHydrosophistCryogenicStasis from 'src/assets/skill-hydrosophist/cryogenic-stasis.svg';
import skillHydrosophistCryotherapy from 'src/assets/skill-hydrosophist/cryotherapy.svg';
import skillHydrosophistDeepFreeze from 'src/assets/skill-hydrosophist/deep-freeze.svg';
import skillHydrosophistGlobalCooling from 'src/assets/skill-hydrosophist/global-cooling.svg';
import skillHydrosophistHailStorm from 'src/assets/skill-hydrosophist/hail-storm.svg';
import skillHydrosophistHailStrike from 'src/assets/skill-hydrosophist/hail-strike.svg';
import skillHydrosophistHealingRitual from 'src/assets/skill-hydrosophist/healing-ritual.svg';
import skillHydrosophistHealingTears from 'src/assets/skill-hydrosophist/healing-tears.svg';
import skillHydrosophistIceBreaker from 'src/assets/skill-hydrosophist/ice-breaker.svg';
import skillHydrosophistIceFan from 'src/assets/skill-hydrosophist/ice-fan.svg';
import skillHydrosophistMassCleanseWounds from 'src/assets/skill-hydrosophist/mass-cleanse-wounds.svg';
import skillHydrosophistMassCryotherapy from 'src/assets/skill-hydrosophist/mass-cryotherapy.svg';
import skillHydrosophistRain from 'src/assets/skill-hydrosophist/rain.svg';
import skillHydrosophistRestoration from 'src/assets/skill-hydrosophist/restoration.svg';
import skillHydrosophistSoothingCold from 'src/assets/skill-hydrosophist/soothing-cold.svg';
import skillHydrosophistSteamLance from 'src/assets/skill-hydrosophist/steam-lance.svg';
import skillHydrosophistVampiricHunger from 'src/assets/skill-hydrosophist/vampiric-hunger.svg';
import skillHydrosophistVampiricHungerAura from 'src/assets/skill-hydrosophist/vampiric-hunger-aura.svg';
import skillHydrosophistWinterBlast from 'src/assets/skill-hydrosophist/winter-blast.svg';

export default {
	skillHydrosophistArcaneStitch,
	skillHydrosophistArmourOfFrost,
	skillHydrosophistCleanseWounds,
	skillHydrosophistCryogenicStasis,
	skillHydrosophistCryotherapy,
	skillHydrosophistDeepFreeze,
	skillHydrosophistGlobalCooling,
	skillHydrosophistHailStorm,
	skillHydrosophistHailStrike,
	skillHydrosophistHealingRitual,
	skillHydrosophistHealingTears,
	skillHydrosophistIceBreaker,
	skillHydrosophistIceFan,
	skillHydrosophistMassCleanseWounds,
	skillHydrosophistMassCryotherapy,
	skillHydrosophistRain,
	skillHydrosophistRestoration,
	skillHydrosophistSoothingCold,
	skillHydrosophistSteamLance,
	skillHydrosophistVampiricHunger,
	skillHydrosophistVampiricHungerAura,
	skillHydrosophistWinterBlast,
};
