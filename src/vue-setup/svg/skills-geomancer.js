import skillGeomancerAcidSpores from 'src/assets/skill-geomancer/acid-spores.svg';
import skillGeomancerContamination from 'src/assets/skill-geomancer/contamination.svg';
import skillGeomancerCorrosiveSpray from 'src/assets/skill-geomancer/corrosive-spray.svg';
import skillGeomancerCorrosiveTouch from 'src/assets/skill-geomancer/corrosive-touch.svg';
import skillGeomancerDustBlast from 'src/assets/skill-geomancer/dust-blast.svg';
import skillGeomancerEarthquake from 'src/assets/skill-geomancer/earthquake.svg';
import skillGeomancerFortify from 'src/assets/skill-geomancer/fortify.svg';
import skillGeomancerFossilStrike from 'src/assets/skill-geomancer/fossil-strike.svg';
import skillGeomancerImpalement from 'src/assets/skill-geomancer/impalement.svg';
import skillGeomancerLivingWall from 'src/assets/skill-geomancer/living-wall.svg';
import skillGeomancerMassOilyCarapace from 'src/assets/skill-geomancer/mass-oily-carapace.svg';
import skillGeomancerMendMetal from 'src/assets/skill-geomancer/mend-metal.svg';
import skillGeomancerOilyCarapace from 'src/assets/skill-geomancer/oily-carapace.svg';
import skillGeomancerPoisonDart from 'src/assets/skill-geomancer/poison-dart.svg';
import skillGeomancerPoisonWave from 'src/assets/skill-geomancer/poison-wave.svg';
import skillGeomancerPyroclasticEruption from 'src/assets/skill-geomancer/pyroclastic-eruption.svg';
import skillGeomancerReactiveArmour from 'src/assets/skill-geomancer/reactive-armour.svg';
import skillGeomancerSiphonPoison from 'src/assets/skill-geomancer/siphon-poison.svg';
import skillGeomancerSummonHungryFlower from 'src/assets/skill-geomancer/summon-hungry-flower.svg';
import skillGeomancerThrowDust from 'src/assets/skill-geomancer/throw-dust.svg';
import skillGeomancerTurnToOil from 'src/assets/skill-geomancer/turn-to-oil.svg';
import skillGeomancerVenomCoating from 'src/assets/skill-geomancer/venom-coating.svg';
import skillGeomancerVenomousAura from 'src/assets/skill-geomancer/venomous-aura.svg';
import skillGeomancerWormTremor from 'src/assets/skill-geomancer/worm-tremor.svg';

export default {
	skillGeomancerAcidSpores,
	skillGeomancerContamination,
	skillGeomancerCorrosiveSpray,
	skillGeomancerCorrosiveTouch,
	skillGeomancerDustBlast,
	skillGeomancerEarthquake,
	skillGeomancerFortify,
	skillGeomancerFossilStrike,
	skillGeomancerImpalement,
	skillGeomancerLivingWall,
	skillGeomancerMassOilyCarapace,
	skillGeomancerMendMetal,
	skillGeomancerOilyCarapace,
	skillGeomancerPoisonDart,
	skillGeomancerPoisonWave,
	skillGeomancerPyroclasticEruption,
	skillGeomancerReactiveArmour,
	skillGeomancerSiphonPoison,
	skillGeomancerSummonHungryFlower,
	skillGeomancerThrowDust,
	skillGeomancerTurnToOil,
	skillGeomancerVenomCoating,
	skillGeomancerVenomousAura,
	skillGeomancerWormTremor,
};
