import originBeast from 'src/assets/origin/beast.svg';
import originFane from 'src/assets/origin/fane.svg';
import originIfan from 'src/assets/origin/ifan.svg';
import originLohse from 'src/assets/origin/lohse.svg';
import originRedPrince from 'src/assets/origin/red-prince.svg';
import originSebille from 'src/assets/origin/sebille.svg';
import originCustomElfFemale from 'src/assets/origin/custom-elf-female.svg';
import originCustomElfMale from 'src/assets/origin/custom-elf-male.svg';
import originCustomDwarfFemale from 'src/assets/origin/custom-dwarf-female.svg';
import originCustomDwarfMale from 'src/assets/origin/custom-dwarf-male.svg';
import originCustomHumanFemale from 'src/assets/origin/custom-human-female.svg';
import originCustomHumanMale from 'src/assets/origin/custom-human-male.svg';
import originCustomLizardFemale from 'src/assets/origin/custom-lizard-female.svg';
import originCustomLizardMale from 'src/assets/origin/custom-lizard-male.svg';
import originCustomUndeadElfFemale from 'src/assets/origin/custom-undead-elf-female.svg';
import originCustomUndeadElfMale from 'src/assets/origin/custom-undead-elf-male.svg';
import originCustomUndeadDwarfFemale from 'src/assets/origin/custom-undead-dwarf-female.svg';
import originCustomUndeadDwarfMale from 'src/assets/origin/custom-undead-dwarf-male.svg';
import originCustomUndeadHumanFemale from 'src/assets/origin/custom-undead-human-female.svg';
import originCustomUndeadHumanMale from 'src/assets/origin/custom-undead-human-male.svg';
import originCustomUndeadLizardFemale from 'src/assets/origin/custom-undead-lizard-female.svg';
import originCustomUndeadLizardMale from 'src/assets/origin/custom-undead-lizard-male.svg';

export default {
	originBeast, originFane, originIfan, originLohse, originRedPrince, originSebille,
	originCustomElfFemale, originCustomElfMale,
	originCustomDwarfFemale, originCustomDwarfMale,
	originCustomHumanFemale, originCustomHumanMale,
	originCustomLizardFemale, originCustomLizardMale,
	originCustomUndeadElfFemale, originCustomUndeadElfMale,
	originCustomUndeadDwarfFemale, originCustomUndeadDwarfMale,
	originCustomUndeadHumanFemale, originCustomUndeadHumanMale,
	originCustomUndeadLizardFemale, originCustomUndeadLizardMale,
};
