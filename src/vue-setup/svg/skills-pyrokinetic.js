import skillPyrokineticBleedFire from 'src/assets/skill-pyrokinetic/bleed-fire.svg';
import skillPyrokineticCorpseExplosion from 'src/assets/skill-pyrokinetic/corpse-explosion.svg';
import skillPyrokineticDeployMassTraps from 'src/assets/skill-pyrokinetic/deploy-mass-traps.svg';
import skillPyrokineticEpidemicOfFire from 'src/assets/skill-pyrokinetic/epidemic-of-fire.svg';
import skillPyrokineticFireWhip from 'src/assets/skill-pyrokinetic/fire-whip.svg';
import skillPyrokineticFireball from 'src/assets/skill-pyrokinetic/fireball.svg';
import skillPyrokineticFirebrand from 'src/assets/skill-pyrokinetic/firebrand.svg';
import skillPyrokineticFlamingCrescendo from 'src/assets/skill-pyrokinetic/flaming-crescendo.svg';
import skillPyrokineticFlamingTongues from 'src/assets/skill-pyrokinetic/flaming-tongues.svg';
import skillPyrokineticHaste from 'src/assets/skill-pyrokinetic/haste.svg';
import skillPyrokineticIgnition from 'src/assets/skill-pyrokinetic/ignition.svg';
import skillPyrokineticLaserRay from 'src/assets/skill-pyrokinetic/laser-ray.svg';
import skillPyrokineticMassCorpseExplosion from 'src/assets/skill-pyrokinetic/mass-corpse-explosion.svg';
import skillPyrokineticMassSabotage from 'src/assets/skill-pyrokinetic/mass-sabotage.svg';
import skillPyrokineticMasterOfSparks from 'src/assets/skill-pyrokinetic/master-of-sparks.svg';
import skillPyrokineticMeteorShower from 'src/assets/skill-pyrokinetic/meteor-shower.svg';
import skillPyrokineticPeaceOfMind from 'src/assets/skill-pyrokinetic/peace-of-mind.svg';
import skillPyrokineticSabotage from 'src/assets/skill-pyrokinetic/sabotage.svg';
import skillPyrokineticSearingDaggers from 'src/assets/skill-pyrokinetic/searing-daggers.svg';
import skillPyrokineticSparkingSwings from 'src/assets/skill-pyrokinetic/sparking-swings.svg';
import skillPyrokineticSpontaneousCombustion from 'src/assets/skill-pyrokinetic/spontaneous-combustion.svg';
import skillPyrokineticSummonFireSlug from 'src/assets/skill-pyrokinetic/summon-fire-slug.svg';
import skillPyrokineticSupernova from 'src/assets/skill-pyrokinetic/supernova.svg';
import skillPyrokineticThrowExplosiveTrap from 'src/assets/skill-pyrokinetic/throw-explosive-trap.svg';

export default {
	skillPyrokineticBleedFire,
	skillPyrokineticCorpseExplosion,
	skillPyrokineticDeployMassTraps,
	skillPyrokineticEpidemicOfFire,
	skillPyrokineticFireWhip,
	skillPyrokineticFireball,
	skillPyrokineticFirebrand,
	skillPyrokineticFlamingCrescendo,
	skillPyrokineticFlamingTongues,
	skillPyrokineticHaste,
	skillPyrokineticIgnition,
	skillPyrokineticLaserRay,
	skillPyrokineticMassCorpseExplosion,
	skillPyrokineticMassSabotage,
	skillPyrokineticMasterOfSparks,
	skillPyrokineticMeteorShower,
	skillPyrokineticPeaceOfMind,
	skillPyrokineticSabotage,
	skillPyrokineticSearingDaggers,
	skillPyrokineticSparkingSwings,
	skillPyrokineticSpontaneousCombustion,
	skillPyrokineticSummonFireSlug,
	skillPyrokineticSupernova,
	skillPyrokineticThrowExplosiveTrap,
};
