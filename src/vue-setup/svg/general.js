import cornerFancy from 'src/assets/general/corner-fancy.svg';
import cornerSimple from 'src/assets/general/corner-simple.svg';
import cornerStandalone from 'src/assets/general/corner-standalone.svg';
import decoratorLineLeft from 'src/assets/general/decorator-line-left.svg';
import attributeStrength from 'src/assets/general/attribute-strength.svg';
import attributeFinesse from 'src/assets/general/attribute-finesse.svg';
import attributeIntelligence from 'src/assets/general/attribute-intelligence.svg';
import attributeConstitution from 'src/assets/general/attribute-constitution.svg';
import attributeMemory from 'src/assets/general/attribute-memory.svg';
import attributeWits from 'src/assets/general/attribute-wits.svg';
import civilAbilityBartering from 'src/assets/general/civil-ability-bartering.svg';
import civilAbilityLoremaster from 'src/assets/general/civil-ability-loremaster.svg';
import civilAbilityLuckyCharm from 'src/assets/general/civil-ability-lucky-charm.svg';
import civilAbilityPersuasion from 'src/assets/general/civil-ability-persuasion.svg';
import civilAbilitySneaking from 'src/assets/general/civil-ability-sneaking.svg';
import civilAbilityTelekinesis from 'src/assets/general/civil-ability-telekinesis.svg';
import civilAbilityThievery from 'src/assets/general/civil-ability-thievery.svg';
import combatAbilityAerotheurge from 'src/assets/general/combat-ability-aerotheurge.svg';
import combatAbilityDualWielding from 'src/assets/general/combat-ability-dual-wielding.svg';
import combatAbilityGeomancer from 'src/assets/general/combat-ability-geomancer.svg';
import combatAbilityHuntsman from 'src/assets/general/combat-ability-huntsman.svg';
import combatAbilityHydrosophist from 'src/assets/general/combat-ability-hydrosophist.svg';
import combatAbilityLeadership from 'src/assets/general/combat-ability-leadership.svg';
import combatAbilityNecromancer from 'src/assets/general/combat-ability-necromancer.svg';
import combatAbilityPerserverance from 'src/assets/general/combat-ability-perserverance.svg';
import combatAbilityPolymorph from 'src/assets/general/combat-ability-polymorph.svg';
import combatAbilityPyrokinetic from 'src/assets/general/combat-ability-pyrokinetic.svg';
import combatAbilityRanged from 'src/assets/general/combat-ability-ranged.svg';
import combatAbilityRetribution from 'src/assets/general/combat-ability-retribution.svg';
import combatAbilityScoundrel from 'src/assets/general/combat-ability-scoundrel.svg';
import combatAbilitySingleHanded from 'src/assets/general/combat-ability-single-handed.svg';
import combatAbilitySummoning from 'src/assets/general/combat-ability-summoning.svg';
import combatAbilityTwoHanded from 'src/assets/general/combat-ability-two-handed.svg';
import combatAbilityWarfare from 'src/assets/general/combat-ability-warfare.svg';
import derivedStatAccuracy from 'src/assets/general/derived-stat-accuracy.svg';
import derivedStatApStart from 'src/assets/general/derived-stat-ap-start.svg';
import derivedStatCritMultiplier from 'src/assets/general/derived-stat-crit-multiplier.svg';
import derivedStatDodging from 'src/assets/general/derived-stat-dodging.svg';
import derivedStatInitiative from 'src/assets/general/derived-stat-initiative.svg';
import derivedStatMovement from 'src/assets/general/derived-stat-movement.svg';
import derivedStatResistAir from 'src/assets/general/derived-stat-resist-air.svg';
import derivedStatResistEarth from 'src/assets/general/derived-stat-resist-earth.svg';
import derivedStatResistFire from 'src/assets/general/derived-stat-resist-fire.svg';
import derivedStatResistPoison from 'src/assets/general/derived-stat-resist-poison.svg';
import derivedStatResistWater from 'src/assets/general/derived-stat-resist-water.svg';
import derivedStatSp from 'src/assets/general/derived-stat-sp.svg';
import derivedStatVitality from 'src/assets/general/derived-stat-vitality.svg';
import equipment2hMace from 'src/assets/general/equipment-2h-mace.svg';
import equipment2hAxe from 'src/assets/general/equipment-2h-axe.svg';
import equipment2hSword from 'src/assets/general/equipment-2h-sword.svg';
import equipmentAxe from 'src/assets/general/equipment-axe.svg';
import equipmentBow from 'src/assets/general/equipment-bow.svg';
import equipmentCrossbow from 'src/assets/general/equipment-crossbow.svg';
import equipmentDagger from 'src/assets/general/equipment-dagger.svg';
import equipmentMace from 'src/assets/general/equipment-mace.svg';
import equipmentShield from 'src/assets/general/equipment-shield.svg';
import equipmentSpear from 'src/assets/general/equipment-spear.svg';
import equipmentStaff from 'src/assets/general/equipment-staff.svg';
import equipmentSword from 'src/assets/general/equipment-sword.svg';
import equipmentWand from 'src/assets/general/equipment-wand.svg';
import skillCooldown from 'src/assets/general/skill-cooldown.svg';
import skillTypeStory from 'src/assets/general/skill-type-story.svg';
import statusResting from 'src/assets/general/status-resting.svg';

const skillTypeSource = combatAbilityNecromancer;
const skillTypeEquipment = skillTypeStory;
const skillTypeInnate = skillTypeStory;

export default {
	cornerFancy, cornerSimple, cornerStandalone, decoratorLineLeft,
	attributeStrength, attributeFinesse, attributeIntelligence,
	attributeConstitution, attributeMemory, attributeWits,
	civilAbilityBartering, civilAbilityLoremaster, civilAbilityLuckyCharm, civilAbilityPersuasion,
	civilAbilitySneaking, civilAbilityTelekinesis, civilAbilityThievery,
	combatAbilityAerotheurge, combatAbilityDualWielding, combatAbilityGeomancer, combatAbilityHuntsman,
	combatAbilityHydrosophist, combatAbilityLeadership, combatAbilityNecromancer, combatAbilityPerserverance,
	combatAbilityPolymorph, combatAbilityPyrokinetic, combatAbilityRanged, combatAbilityRetribution,
	combatAbilityScoundrel, combatAbilitySingleHanded, combatAbilitySummoning, combatAbilityTwoHanded,
	combatAbilityWarfare, derivedStatAccuracy, derivedStatApStart, derivedStatCritMultiplier, derivedStatDodging,
	derivedStatInitiative, derivedStatMovement,
	derivedStatResistAir, derivedStatResistEarth, derivedStatResistFire, derivedStatResistPoison, derivedStatResistWater,
	derivedStatSp, derivedStatVitality,
	equipment2hMace, equipment2hAxe, equipment2hSword, equipmentAxe, equipmentBow, equipmentCrossbow, equipmentDagger,
	equipmentMace, equipmentShield, equipmentSpear, equipmentStaff, equipmentSword, equipmentWand,
	skillTypeEquipment, skillTypeInnate, skillTypeSource, skillTypeStory, skillCooldown, statusResting,
};
