import skillSummoningAcidInfusion from 'src/assets/skill-summoning/acid-infusion.svg';
import skillSummoningCannibalise from 'src/assets/skill-summoning/cannibalise.svg';
import skillSummoningConjureIncarnate from 'src/assets/skill-summoning/conjure-incarnate.svg';
import skillSummoningControlVoidwoken from 'src/assets/skill-summoning/control-voidwoken.svg';
import skillSummoningCursedElectricInfusion from 'src/assets/skill-summoning/cursed-electric-infusion.svg';
import skillSummoningDimensionalBolt from 'src/assets/skill-summoning/dimensional-bolt.svg';
import skillSummoningDominateMind from 'src/assets/skill-summoning/dominate-mind.svg';
import skillSummoningDoorToEternity from 'src/assets/skill-summoning/door-to-eternity.svg';
import skillSummoningElectricInfusion from 'src/assets/skill-summoning/electric-infusion.svg';
import skillSummoningElementalTotem from 'src/assets/skill-summoning/elemental-totem.svg';
import skillSummoningEtherealStorm from 'src/assets/skill-summoning/ethereal-storm.svg';
import skillSummoningFarsightInfusion from 'src/assets/skill-summoning/farsight-infusion.svg';
import skillSummoningFireInfusion from 'src/assets/skill-summoning/fire-infusion.svg';
import skillSummoningIceInfusion from 'src/assets/skill-summoning/ice-infusion.svg';
import skillSummoningNecrofireInfusion from 'src/assets/skill-summoning/necrofire-infusion.svg';
import skillSummoningPlanarGateway from 'src/assets/skill-summoning/planar-gateway.svg';
import skillSummoningPoisonInfusion from 'src/assets/skill-summoning/poison-infusion.svg';
import skillSummoningPowerInfusion from 'src/assets/skill-summoning/power-infusion.svg';
import skillSummoningRallyingCry from 'src/assets/skill-summoning/rallying-cry.svg';
import skillSummoningShadowInfusion from 'src/assets/skill-summoning/shadow-infusion.svg';
import skillSummoningSoulMate from 'src/assets/skill-summoning/soul-mate.svg';
import skillSummoningSummonCatFamiliar from 'src/assets/skill-summoning/summon-cat-familiar.svg';
import skillSummoningSummonCondor from 'src/assets/skill-summoning/summon-condor.svg';
import skillSummoningSummonDragonling from 'src/assets/skill-summoning/summon-dragonling.svg';
import skillSummoningSummonInnerDemon from 'src/assets/skill-summoning/summon-inner-demon.svg';
import skillSummoningSupercharger from 'src/assets/skill-summoning/supercharger.svg';
import skillSummoningWarpInfusion from 'src/assets/skill-summoning/warp-infusion.svg';
import skillSummoningWaterInfusion from 'src/assets/skill-summoning/water-infusion.svg';

export default {
	skillSummoningAcidInfusion,
	skillSummoningCannibalise,
	skillSummoningConjureIncarnate,
	skillSummoningControlVoidwoken,
	skillSummoningCursedElectricInfusion,
	skillSummoningDimensionalBolt,
	skillSummoningDominateMind,
	skillSummoningDoorToEternity,
	skillSummoningElectricInfusion,
	skillSummoningElementalTotem,
	skillSummoningEtherealStorm,
	skillSummoningFarsightInfusion,
	skillSummoningFireInfusion,
	skillSummoningIceInfusion,
	skillSummoningNecrofireInfusion,
	skillSummoningPlanarGateway,
	skillSummoningPoisonInfusion,
	skillSummoningPowerInfusion,
	skillSummoningRallyingCry,
	skillSummoningShadowInfusion,
	skillSummoningSoulMate,
	skillSummoningSummonCatFamiliar,
	skillSummoningSummonCondor,
	skillSummoningSummonDragonling,
	skillSummoningSummonInnerDemon,
	skillSummoningSupercharger,
	skillSummoningWarpInfusion,
	skillSummoningWaterInfusion,
};
