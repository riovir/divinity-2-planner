import skillEquipmentAllIn from 'src/assets/skill-special/equipment-all-in.svg';
import skillEquipmentFlurry from 'src/assets/skill-special/equipment-flurry.svg';
import skillEquipmentShieldsUp from 'src/assets/skill-special/equipment-shields-up.svg';
import skillEquipmentStaffOfMagus from 'src/assets/skill-special/equipment-staff-of-magus.svg';
import skillEquipmentSuckerPunch from 'src/assets/skill-special/equipment-sucker-punch.svg';
import skillInnateBlindingSquall from 'src/assets/skill-special/innate-blinding-squall.svg';
import skillInnateBreakTheShackles from 'src/assets/skill-special/innate-break-the-shackles.svg';
import skillInnateDemonicStare from 'src/assets/skill-special/innate-demonic-stare.svg';
import skillInnateDomeOfProtection from 'src/assets/skill-special/innate-dome-of-protection.svg';
import skillInnateDragonsBlaze from 'src/assets/skill-special/innate-dragons-blaze.svg';
import skillInnateEncourage from 'src/assets/skill-special/innate-encourage.svg';
import skillInnateFleshSacrifice from 'src/assets/skill-special/innate-flesh-sacrifice.svg';
import skillInnateMaddeningSong from 'src/assets/skill-special/innate-maddening-song.svg';
import skillInnatePetrifyingTouch from 'src/assets/skill-special/innate-petrifying-touch.svg';
import skillInnatePlayDead from 'src/assets/skill-special/innate-play-dead.svg';
import skillInnateSummonIfansSoulWolf from 'src/assets/skill-special/innate-summon-ifans-soul-wolf.svg';
import skillInnateTimeWarp from 'src/assets/skill-special/innate-time-warp.svg';
import skillSourceBless from 'src/assets/skill-special/source-bless.svg';
import skillStoryDeafeningShriek from 'src/assets/skill-special/story-deafening-shriek.svg';

export default {
	skillEquipmentAllIn,
	skillEquipmentFlurry,
	skillEquipmentShieldsUp,
	skillEquipmentStaffOfMagus,
	skillEquipmentSuckerPunch,
	skillInnateBlindingSquall,
	skillInnateBreakTheShackles,
	skillInnateDemonicStare,
	skillInnateDomeOfProtection,
	skillInnateDragonsBlaze,
	skillInnateEncourage,
	skillInnateFleshSacrifice,
	skillInnateMaddeningSong,
	skillInnatePetrifyingTouch,
	skillInnatePlayDead,
	skillInnateSummonIfansSoulWolf,
	skillInnateTimeWarp,
	skillSourceBless,
	skillStoryDeafeningShriek,
};
