import skillHuntsmanArrowSpray from 'src/assets/skill-huntsman/arrow-spray.svg';
import skillHuntsmanArrowStorm from 'src/assets/skill-huntsman/arrow-storm.svg';
import skillHuntsmanAssassinate from 'src/assets/skill-huntsman/assassinate.svg';
import skillHuntsmanBallisticShot from 'src/assets/skill-huntsman/ballistic-shot.svg';
import skillHuntsmanBarrage from 'src/assets/skill-huntsman/barrage.svg';
import skillHuntsmanElementalArrowheads from 'src/assets/skill-huntsman/elemental-arrowheads.svg';
import skillHuntsmanFarsight from 'src/assets/skill-huntsman/farsight.svg';
import skillHuntsmanFirstAid from 'src/assets/skill-huntsman/first-aid.svg';
import skillHuntsmanGlitterDust from 'src/assets/skill-huntsman/glitter-dust.svg';
import skillHuntsmanMarksmansFang from 'src/assets/skill-huntsman/marksmans-fang.svg';
import skillHuntsmanPinDown from 'src/assets/skill-huntsman/pin-down.svg';
import skillHuntsmanReactiveShot from 'src/assets/skill-huntsman/reactive-shot.svg';
import skillHuntsmanRicochet from 'src/assets/skill-huntsman/ricochet.svg';
import skillHuntsmanSkyShot from 'src/assets/skill-huntsman/sky-shot.svg';
import skillHuntsmanTacticalRetreat from 'src/assets/skill-huntsman/tactical-retreat.svg';

export default {
	skillHuntsmanArrowSpray,
	skillHuntsmanArrowStorm,
	skillHuntsmanAssassinate,
	skillHuntsmanBallisticShot,
	skillHuntsmanBarrage,
	skillHuntsmanElementalArrowheads,
	skillHuntsmanFarsight,
	skillHuntsmanFirstAid,
	skillHuntsmanGlitterDust,
	skillHuntsmanMarksmansFang,
	skillHuntsmanPinDown,
	skillHuntsmanReactiveShot,
	skillHuntsmanRicochet,
	skillHuntsmanSkyShot,
	skillHuntsmanTacticalRetreat,
};
