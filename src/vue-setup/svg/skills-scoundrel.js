import skillScoundrelAdrenaline from 'src/assets/skill-scoundrel/adrenaline.svg';
import skillScoundrelBacklash from 'src/assets/skill-scoundrel/backlash.svg';
import skillScoundrelChloroform from 'src/assets/skill-scoundrel/chloroform.svg';
import skillScoundrelCloakAndDagger from 'src/assets/skill-scoundrel/cloak-and-dagger.svg';
import skillScoundrelCorruptedBlade from 'src/assets/skill-scoundrel/corrupted-blade.svg';
import skillScoundrelDaggersDrawn from 'src/assets/skill-scoundrel/daggers-drawn.svg';
import skillScoundrelFanOfKnives from 'src/assets/skill-scoundrel/fan-of-knives.svg';
import skillScoundrelGagOrder from 'src/assets/skill-scoundrel/gag-order.svg';
import skillScoundrelMortalBlow from 'src/assets/skill-scoundrel/mortal-blow.svg';
import skillScoundrelRuptureTendons from 'src/assets/skill-scoundrel/rupture-tendons.svg';
import skillScoundrelSawtoothKnife from 'src/assets/skill-scoundrel/sawtooth-knife.svg';
import skillScoundrelSleepingArms from 'src/assets/skill-scoundrel/sleeping-arms.svg';
import skillScoundrelTerrifyingCruelty from 'src/assets/skill-scoundrel/terrifying-cruelty.svg';
import skillScoundrelThrowingKnife from 'src/assets/skill-scoundrel/throwing-knife.svg';
import skillScoundrelWindUpToy from 'src/assets/skill-scoundrel/wind-up-toy.svg';

export default {
	skillScoundrelAdrenaline,
	skillScoundrelBacklash,
	skillScoundrelChloroform,
	skillScoundrelCloakAndDagger,
	skillScoundrelCorruptedBlade,
	skillScoundrelDaggersDrawn,
	skillScoundrelFanOfKnives,
	skillScoundrelGagOrder,
	skillScoundrelMortalBlow,
	skillScoundrelRuptureTendons,
	skillScoundrelSawtoothKnife,
	skillScoundrelSleepingArms,
	skillScoundrelTerrifyingCruelty,
	skillScoundrelThrowingKnife,
	skillScoundrelWindUpToy,
};
