import skillNecromancerBlackShroud from 'src/assets/skill-necromancer/black-shroud.svg';
import skillNecromancerBloodStorm from 'src/assets/skill-necromancer/blood-storm.svg';
import skillNecromancerBloodSucker from 'src/assets/skill-necromancer/blood-sucker.svg';
import skillNecromancerBoneCage from 'src/assets/skill-necromancer/bone-cage.svg';
import skillNecromancerDeathWish from 'src/assets/skill-necromancer/death-wish.svg';
import skillNecromancerDecayingTouch from 'src/assets/skill-necromancer/decaying-touch.svg';
import skillNecromancerGraspOfTheStarved from 'src/assets/skill-necromancer/grasp-of-the-starved.svg';
import skillNecromancerInfect from 'src/assets/skill-necromancer/infect.svg';
import skillNecromancerLastRites from 'src/assets/skill-necromancer/last-rites.svg';
import skillNecromancerLivingOnTheEdge from 'src/assets/skill-necromancer/living-on-the-edge.svg';
import skillNecromancerMosquitoSwarm from 'src/assets/skill-necromancer/mosquito-swarm.svg';
import skillNecromancerRainingBlood from 'src/assets/skill-necromancer/raining-blood.svg';
import skillNecromancerRaiseBloatedCorpse from 'src/assets/skill-necromancer/raise-bloated-corpse.svg';
import skillNecromancerRaiseBoneWidow from 'src/assets/skill-necromancer/raise-bone-widow.svg';
import skillNecromancerShacklesOfPain from 'src/assets/skill-necromancer/shackles-of-pain.svg';
import skillNecromancerSilencingStare from 'src/assets/skill-necromancer/silencing-stare.svg';
import skillNecromancerTotemsOfTheNecromancer from 'src/assets/skill-necromancer/totems-of-the-necromancer.svg';

export default {
	skillNecromancerBlackShroud,
	skillNecromancerBloodStorm,
	skillNecromancerBloodSucker,
	skillNecromancerBoneCage,
	skillNecromancerDeathWish,
	skillNecromancerDecayingTouch,
	skillNecromancerGraspOfTheStarved,
	skillNecromancerInfect,
	skillNecromancerLastRites,
	skillNecromancerLivingOnTheEdge,
	skillNecromancerMosquitoSwarm,
	skillNecromancerRainingBlood,
	skillNecromancerRaiseBloatedCorpse,
	skillNecromancerRaiseBoneWidow,
	skillNecromancerShacklesOfPain,
	skillNecromancerSilencingStare,
	skillNecromancerTotemsOfTheNecromancer,
};
