import Vue from 'vue';
import VueRouter from 'vue-router';
import { CS_AEROTHEURGE } from 'src/store';
import TheFrame from '../the-frame';
import store from './vuex';

export const FLAG_SW_UPDATE = 'swUpdate';

Vue.use(VueRouter);

const TheCharacter = () => import(/* webpackChunkName: 'the-character' */ '../components/the-character');
const TheSkillbook = () => import(/* webpackChunkName: 'the-skillbook' */ '../components/the-skillbook');

const router = new VueRouter({ mode: 'history', routes: [
	{	path: '/:locale', component: TheFrame, props: frameProps, children: [
		{ name: 'character', path: 'character/:id/edit', component: TheCharacter },
		{ name: 'skillbook', path: 'character/:id/skillbook', redirect: redirectToSkillbookPage },
		{ name: 'skillbook-page', path: 'character/:id/skillbook/:category', props: true, component: TheSkillbook },
	] },
	{ path: '*', redirect: redirectToCharacterPage },
] });

router.beforeEach((to, _, next) => {
	return window[FLAG_SW_UPDATE] && process.env.NODE_ENV === 'production' ? (window.location = to) : next();
});
router.beforeResolve(({ name: fromName }, { name: toName }, next) => {
	if (fromName === toName) { return next(); }
	if (fromName) { store.dispatch('setBusy'); }
	setTimeout(next, 20);
});
router.afterEach(() => store.dispatch('setBusy', false));

export default router;

function frameProps({ params, query }) {
	return { locale: params.locale, data: query.data, id: Number(params.id) || 1 };
}

function redirectToCharacterPage({ params: { locale = 'en', id = 1 } = {}, query }) {
	return { name: 'character', params: { locale, id }, query };
}

function redirectToSkillbookPage({ params, query }) {
	return { name: 'skillbook-page', params: { ...params, category: CS_AEROTHEURGE }, query };
}
