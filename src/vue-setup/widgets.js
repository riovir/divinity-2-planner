import Vue from 'vue';
import Widgets, { installIcons, setIconsReady } from '../widgets';
import { mixin as clickaway } from 'vue-clickaway';
import actionEdit from 'src/assets/general/action-edit.svg';
import actionExamine from 'src/assets/general/civil-ability-loremaster.svg';
import actionExpand from 'src/assets/general/action-expand.svg';
import actionGoBack from 'src/assets/general/action-go-back.svg';
import statusLockOff from 'src/assets/general/status-lock-off.svg';
import statusLockOn from 'src/assets/general/status-lock-on.svg';
import toggleSkillbook from 'src/assets/general/toggle-skillbook.svg';
import logo from 'src/assets/logo.svg';

Vue.mixin(clickaway);

Vue.use(Widgets);

installIcons({ actionEdit, actionExamine, actionExpand, actionGoBack, logo, statusLockOff, statusLockOn, toggleSkillbook });
fetchIconsThen(registerWith(installIcons));

async function fetchIconsThen(register) {
	await Promise.all([
		import(/* webpackChunkName: 'svgs-general' */ './svg/general'),
		import(/* webpackChunkName: 'svgs-origin' */ './svg/origins'),
		import(/* webpackChunkName: 'svgs-skills-aerotheurge' */ './svg/skills-aerotheurge'),
		import(/* webpackChunkName: 'svgs-skills-geomancer' */ './svg/skills-geomancer'),
		import(/* webpackChunkName: 'svgs-skills-huntsman' */ './svg/skills-huntsman'),
		import(/* webpackChunkName: 'svgs-skills-hydrosophist' */ './svg/skills-hydrosophist'),
		import(/* webpackChunkName: 'svgs-skills-necromancer' */ './svg/skills-necromancer'),
		import(/* webpackChunkName: 'svgs-skills-polymorph' */ './svg/skills-polymorph'),
		import(/* webpackChunkName: 'svgs-skills-pyrokinetic' */ './svg/skills-pyrokinetic'),
		import(/* webpackChunkName: 'svgs-skills-scoundrel' */ './svg/skills-scoundrel'),
		import(/* webpackChunkName: 'svgs-skills-special' */ './svg/skills-special'),
		import(/* webpackChunkName: 'svgs-skills-summoning' */ './svg/skills-summoning'),
		import(/* webpackChunkName: 'svgs-skills-warfare' */ './svg/skills-warfare'),
	].map(promise => promise.then(register)));
	setIconsReady();
}

function registerWith(install) {
	return async iconPack => {
		const iconModule = await iconPack;
		install(iconModule.default);
	};
}
