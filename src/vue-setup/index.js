import './widgets';
import i18n from './i18n';
import router from './router';
import store from './vuex';

export default { router, i18n, store };
export { router, i18n, store };
export { FLAG_SW_UPDATE } from './router';
