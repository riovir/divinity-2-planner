export const svgs = { data: {}, ready: false };

export function install(newSvgs) {
	return svgs.data = { ...svgs.data, ...newSvgs };
}

export function setReady(value = true) {
	svgs.ready = value;
}

export function isKnown(name) {
	return !!svgs.data[name];
}

export function isReady() {
	return svgs.ready;
}
