import DpBox from './dp-box';
import DpDecorTitle from './dp-decor-title';
import DpDetailField from './dp-detail-field';
import DpDropdown from './dp-dropdown';
import DpField from './dp-field';
import DpLink from './dp-link';
import DpLock from './dp-lock';
import DpModal from './dp-modal';
import DpSelect from './dp-select';
import DpScreenCover from './dp-screen-cover';
import DpSvg from './dp-svg';
import { install as installSvgs } from './svg-library';

export default function install(Vue, { svgs } = {}) {
	installSvgs(svgs);
	Vue.component(DpBox.name, DpBox);
	Vue.component(DpDecorTitle.name, DpDecorTitle);
	Vue.component(DpDetailField.name, DpDetailField);
	Vue.component(DpDropdown.name, DpDropdown);
	Vue.component(DpField.name, DpField);
	Vue.component(DpLink.name, DpLink);
	Vue.component(DpLock.name, DpLock);
	Vue.component(DpModal.name, DpModal);
	Vue.component(DpSelect.name, DpSelect);
	Vue.component(DpScreenCover.name, DpScreenCover);
	Vue.component(DpSvg.name, DpSvg);
}

export {
	isKnown as isKnownIcon,
	install as installIcons,
	setReady as setIconsReady,
} from './svg-library';
