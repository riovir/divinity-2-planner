import { action, error, title, html, workingOn } from './application';
import { derivedStat, equipment, equipmentSlot, gender, origin, race, status } from './overview';
import { attribute } from './attribute';
import { civilAbility, combatAbility } from './ability';
import { talent, talentType } from './talent';
import { skill, skillTitle } from './skill';

export default {
	action, error, title, html, workingOn,
	derivedStat, equipment, equipmentSlot, gender, origin, race, status,
	attribute,
	civilAbility, combatAbility,
	talent, talentType,
	skill, skillTitle,
};
