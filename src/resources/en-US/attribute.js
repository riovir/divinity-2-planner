export const attribute = {
	level: 'Level',
	strength: 'Strength',
	strengthDescription:
			'Strength increases your damage with strength-based weapons and skills, ' +
			'and allows you to lift and carry heavier items.',
	strengthDetails:
			'Can move items with weight up to {0}kg. ' +
			'Can carry items with total weight up to {1}kg before becoming encumbered.',
	finesse: 'Finesse',
	finesseDescription: 'Finesse increases your damage with finesse-based weapons and skills.',
	intelligence: 'Intelligence',
	intelligenceDescription: 'Determines intelligence-based damage you deal.',
	constitution: 'Constitution',
	constitutionDescription: 'Constitution determines how much Vitality you have.',
	memory: 'Memory',
	memoryDescription:
			'Memory affects your amount of Memory Slots, which are required to learn skills. ' +
			'More powerful skills cost more Memory Slots.',
	wits: 'Wits',
	witsDescription:
			'Wits affect your Critical Chance, Initiative, and your ability to ' +
			'detect traps and find hidden treasures.',
};
