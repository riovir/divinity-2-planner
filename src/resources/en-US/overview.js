export const derivedStat = {
	accuracy: 'Accuracy',
	accuracyDescription: 'Your chance to hit an enemy.',
	apGeneral: 'Action Points',
	apMax: 'Maximum Action Points',
	apMaxDescription:
		'The number of Action Points you can have in total. You save up unused Action Points from previous turns, but never more than this.',
	apRegen: 'Action Points per turn',
	apRegenDescription: 'How many Action Points you gain in subsequent turns.',
	apStart: 'Starting Action Points',
	apStartDescription: 'How many Action Points you start combat with.',
	critChance: 'Critical chance',
	critChanceDescription:
			'How much chance you have to critically hit ' +
			'when attacking with a weapon or when using a skill that is weapon-based.',
	critGeneral: 'Critical bonus',
	critMultiplier: 'Critical damage bonus',
	critMultiplierDescription: 'The extra damage you deal on a successful critical strike.',
	critValues: '+{0} / {1}',
	damageBonusGeneral: 'General bonus',
	damageBonusElemental: 'Elemental bonus',
	damageBonusExplanation: 'General and Elemental bonuses are multiplicative.',
	damageAir: 'Air bonus',
	damageEarth: 'Earth and Poison bonus',
	damageFire: 'Fire bonus',
	damageWater: 'Water bonus',
	damageStrength: 'Strength damage',
	damageStrengthDescription:
			'Your Damage bonus when successfully hitting an enemy with strength-based weapons and skills.',
	damageStrengthDetails: 'Currently: Strength based attacks and skills do {0}% extra damage.',
	damageFinesse: 'Finesse damage',
	damageFinesseDescription:
			'Your Damage bonus when successfully hitting an enemy with finesse-based weapons and skills.',
	damageFinesseDetails: 'Currently: Finesse based attacks and skills do {0}% extra damage.',
	damageIntelligence: 'Intelligence damage',
	damageIntelligenceDescription:
			'Your Damage bonus when successfully hitting an enemy with intelligence-based weapons and skills.',
	damageIntelligenceDetails: 'Currently: Intelligence based attacks and skills do {0}% extra damage.',
	dodging: 'Dodging',
	dodgingDescription: 'Your ability to evade attacks.',
	initiative: 'Initiative',
	initiativeDescription:
			'Initiative determines turn order. During combat, teams take turns attacking. ' +
			'Individuals with higher initiative will move up in tum order.',
	memorySlot: 'Memory slots',
	memorySlotDetails: 'Base slots: {0}. Extra slots from Memory: +{1}',
	movement: 'Movement',
	movementDescription: 'The distance you can run with one action point.',
	resistAir: 'Air resistance',
	resistAirDescription: 'Reduces the damage from air-based attacks.',
	resistEarth: 'Earth resistance',
	resistEarthDescription: 'Reduces the damage from earth-based attacks.',
	resistFire: 'Fire resistance',
	resistFireDescription: 'Reduces the damage from fire-based attacks.',
	resistPoison: 'Poison resistance',
	resistPoisonDescription: 'Reduces the damage from poison-based attacks.',
	resistWater: 'Water resistance',
	resistWaterDescription: 'Reduces the damage from water-based attacks.',
	vitality: 'Vitality',
	vitalityDescription: 'How much damage you can take before you die.',
	vitalityMultiplier: 'Vitality bonus',
	vitalityMultiplierDescription: 'Applies on top of total Vitality',
};

export const equipment = {
	'2hAxe': 'Two-handed axe',
	'2hMace': 'Two-handed mace',
	'2hSword': 'Two-handed sword',
	axe: 'Axe',
	bow: 'Bow',
	crossbow: 'Crossbow',
	dagger: 'Dagger',
	mace: 'Mace',
	melee: 'Melee weapon',
	ranged: 'Ranged weapon',
	shield: 'Shield',
	spear: 'Spear',
	staff: 'Staff',
	sword: 'Sword',
	wand: 'Wand',
};

export const equipmentSlot = {
	mainHand: 'Main hand',
	offHand: 'Off hand',
};

export const gender = {
	female: 'Female',
	male: 'Male',
};

export const origin = {
	beast: 'Beast',
	fane: 'Fane',
	ifan: 'Ifan Ben-Mezd',
	lohse: 'Lohse',
	redPrince: 'The Red Prince',
	sebille: 'Sebille',
	custom: 'Custom',
	customElfFemale: 'Custom elf female',
	customElfMale: 'Custom elf male',
	customDwarfFemale: 'Custom dwarf female',
	customDwarfMale: 'Custom dwarf male',
	customHumanFemale: 'Custom human female',
	customHumanMale: 'Custom human male',
	customLizardFemale: 'Custom lizard female',
	customLizardMale: 'Custom lizard male',
	customUndeadElfFemale: 'Custom undead elf female',
	customUndeadElfMale: 'Custom undead elf male',
	customUndeadDwarfFemale: 'Custom undead dwarf female',
	customUndeadDwarfMale: 'Custom undead dwarf male',
	customUndeadHumanFemale: 'Custom undead human female',
	customUndeadHumanMale: 'Custom undead human male',
	customUndeadLizardFemale: 'Custom undead lizard female',
	customUndeadLizardMale: 'Custom undead lizard male',
};

export const race = {
	dwarf: 'Dwarf',
	elf: 'Elf',
	eternal: 'Eternal',
	human: 'Human',
	lizard: 'Lizard',
	undeadDwarf: 'Undead dwarf',
	undeadElf: 'Undead elf',
	undeadHuman: 'Undead human',
	undeadLizard: 'Undead lizard',
};

export const status = {
	notInParty: 'Not in the Party',
};
