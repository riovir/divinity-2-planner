export const civilAbility = {
	bartering: 'Bartering',
	barteringDescription:
		'Bartering improves your haggling skills. With each point invested, ' +
		'traders items become cheaper and your items become more expensive.',
	loremaster: 'Loremaster',
	loremasterDescription:
		'Loremaster identifies enemies and allows you to identify items. ' +
		'Increasing Loremaster allows you to identify more, faster.',
	luckyCharm: 'Lucky Charm',
	luckyCharmDescription: 'Lucky Charm increases your likelihood of finding extra treasure wherever loot is stashed.',
	persuasion: 'Persuasion',
	persuasionDescription:
		'Persuasion helps you convince characters to do your bidding in dialogues, ' +
		'and increases how much characters like you.',
	sneaking: 'Sneaking',
	sneakingDescription:
		'Sneaking determines how well you can sneak without getting caught. ' +
		'Increasing this ability shrinks NPC sight cones and improves your movement speed while sneaking.',
	telekinesis: 'Telekinesis',
	telekinesisDescription: 'Telekinesis allows you to move items telepathically regardless of weight.',
	thievery: 'Thievery',
	thieveryDescription: 'Thievery improves your lockpicking and pickpocketing skills.',
};

export const combatAbility = {
	aerotheurge: 'Aerotheurge',
	aerotheurgeDescription: 'Aerotheurge increases all Air damage you deal.',
	dualWielding: 'Dual wielding',
	dualWieldingDescription: 'Dual Wielding increases damage and Dodging when dual-wielding two single-handed weapons.',
	geomancer: 'Geomancer',
	geomancerDescription: 'Geomancer increases all Poison and Earth damage you deal, and any Physical Armour restoration you cause.',
	huntsman: 'Huntsman',
	huntsmanDescription:
		'Huntsman increases the damage bonus when attacking from high ground. ' +
		"It's regarded as a Critical bonus and multiplies any other damage bonuses.",
	hydrosophist: 'Hydrosophist',
	hydrosophistDescription:
		'Hydrosophist increases all Water damage you deal, and any Vitality healing ' +
		'or Magic Armour restoration that you cause.',
	leadership: 'Leadership',
	leadershipDescription: 'Leadership grants Dodging and Resistance bonuses to all allies in a 5m radius.',
	necromancer: 'Necromancer',
	necromancerDescription: 'Necromancer heals you whenever you deal damage directly to Vitality.',
	perserverance: 'Perserverance',
	perserveranceDescription:
		'Perseverance restores Magic Armor after you recover from Frozen or Stunned, ' +
		'and restores Physical Armor after Knocked Down or Petrified.',
	polymorph: 'Polymorph',
	polymorphDescription: 'Polymorph provides 1 free attribute point per point invested.',
	pyrokinetic: 'Pyrokinetic',
	pyrokineticDescription: 'Pyrokinetic increases all Fire damage you deal.',
	ranged: 'Ranged',
	rangedDescription: 'Ranged increase damage and Critical Chance when using bows and crossbows.',
	retribution: 'Retribution',
	retributionDescription: 'Retribution reflects received damage to your attacker.',
	scoundrel: 'Scoundrel',
	scoundrelDescription: 'Scoundrel increases movement speed and boosts your Critical Modifier.',
	singleHanded: 'Single-handed',
	singleHandedDescription:
		'Single-handed increase damage and Accuracy when using a ' +
		'one-handed weapon (dagger, sword, axe, mace or wand) with a shield or empty off-hand.',
	summoning: 'Summoning',
	summoningDescription: 'Summoning increases Vitality, Damage, Physical Armour and Magical Armour of your summons and totems.',
	twoHanded: 'Two-handed',
	twoHandedDescription:
		'Two-Handed increases damage and the Critical Multiplier when using ' +
		'two-handed melee weapons (sword, axe, mace, spear or staff).',
	warfare: 'Warfare',
	warfareDescription: 'Warfare increases all Physical damage you deal.',
};
