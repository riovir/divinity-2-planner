export const workingOn = {
	currently: 'Latest fix: Single-handed bonus and Backlash',
	link: 'https://gitlab.com/riovir/divinity-2-planner/-/boards',
};

export const action = {
	addCharacter: 'Add',
	buildCopy: 'Copy',
	buildCopyExplanation: 'Your build is packed into this URL. You can save it for later, import it back, or share it with others.',
	buildExport: 'Export',
	buildImport: 'Import',
	buildImportExplanation: 'Paste build URL to import',
	buildShare: 'Share',
	removeCharacter: 'Delete',
	giveFeedback: 'Give feedback',
	resetBuilds: 'Wipe party',
	showMore: 'More',
	skillbookClose: 'Close skillbook',
	skillbookOpen: 'Open skillbook',
	sneakPeek: 'Take a Peek',
	statIncrease: 'Increase {0} of {1}',
	statDecrease: 'Decrease {0} of {1}',
	talentAdd: 'Add talent: {0}',
	talentRemove: 'Remove talent: {0}',
	toggleDetails: 'Toggle details: {0}',
};

export const error = {
	invalidBuildUrl: "This doesn't seem to be a valid build URL",
	unsupportedStateVersion: 'Unable to load a saved party of version {0}. Sorry for the inconvenience.',
};

export const title = {
	appMain: 'Divinity Original Sin 2',
	appSubtitle: 'Party planner',
	attribute: 'Attributes',
	buildExport: 'Export build',
	buildImport: 'Import build',
	buildCopy: 'Export build',
	busy: 'Just a sec...',
	edit: 'Edit',
	civilAbilities: 'Civil abilities',
	combatAbilities: 'Combat abilities',
	combatAbilitiesDefence: 'Defence',
	combatAbilitiesSkills: 'Skills',
	combatAbilitiesWeapons: 'Weapons',
	damageBonuses: 'Damage bonus',
	equipment: 'Equipment',
	gender: 'Gender',
	level: 'Level',
	origin: 'Origin',
	overview: 'Overview',
	race: 'Race',
	resists: 'Resistances',
	sharedBuild: 'My build',
	skillbook: 'Skillbook',
	skills: 'Memorized Skills',
	statBase: 'Base',
	stats: 'Stats',
	talents: 'Talents',
};

const footer =
`<p><strong>Divinity: Original Sin 2 - Party planner</strong> by
<a href="https://gitlab.com/users/riovir/projects" target="_blank">Riovir</a>. The source code and image assets are
licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">CC BY NC SA 4.0</a> as they are
derived from the work of <a href="http://larian.com/" target="_blank">Larian Studios</a>.</p>
<p><strong>Big thanks</strong> to the <a href="https://www.reddit.com/r/DivinityOriginalSin" target="_blank">DivinityOriginalSin</a>
community for testing, brainstorming, and bug-spotting!</p>`;

export const html = { footer };
